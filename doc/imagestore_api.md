# Yleistä

Kaikki virhevastaukset palautetaan JSON-objektina, joka sisältää ainakin
koodikentän code ja mahdollisia lisäparametreja.

Jollei erikseen mainittu, kaikkien pyyntöjen ja vastausten sisältö on JSON-muodossa.

Varsinainen kuvatiedostojen säilytystapa tiedostojärjestelmässä tai tietokannassa on rajapinnan sisäisestä toteutuksesta riippuvaa, eikä sen tulisi näkyä itse rajapinnassa.

## Käsitteitä

* *Asiakasjärjestelmä* - Kuvapalvelua hyödyntävä järjestelmä, esimerkiksi kokoelmanhallintajärjestelmä, jossa on mahdollista liittää kuvia näytteisiin tai oppimisympäristö, joka näyttää tiettyyn lajiin liittyviä kuvia.
* *Loppukäyttäjä* - Jonkin asiakasjärjestelmän loppukäyttäjä, tyypillisesti ihminen.
* *Kuvaresurssi* - Yhdestä kuvatiedostosta ja siihen liittyvistä metatiedoista luotu kokonaisuus.
* *Kuvapalvelu* - Luomuksen ylläpitämä rajapinta, johon asiakasjärjestelmät voivat ladata kuvia ja hakea niitä. Huolehtii mm. kuvien metatiedoista ja muunnoksista. Asiakasjärjestelmät tyypillisesti välittävät kuvapalvelun tarjoamia tietoja loppukäyttäjille.
* *Kuvavarasto* - Digitariumin ylläpitämä rajapinta, johon *kuvapalvelu* voi ladata kuvatiedostoja ja myös poistaa niitä. Loppukäyttäjät voivat suoraan ladata kuvia kuvapalvelusta saaduilla osoitteilla.

# Autentikaatio ja suojaus

Kaikki pyynnöt - poislukien itse kuviin kohdistuvat GET-pyynnöt - tulee autentikoida. Yhteyden salaus vaaditaan kaikissa autentikointia vaativissa pyynnöissä.

Jotkin kuvaresurssit ovat suojattuja, tällöin kaikkiin kuvaresurssin kuvatiedostoihin kohdistuvissa GET-pyynnöissä tulee olla kuvapalvelun loppukäyttäjälle välitetty avain. Avain luodaan ja tarkistetaan yhdistämällä kuvaresurssin tunniste ja kuvapalvelun sekä kuvarajapinnan yhteinen salasana.

## Esimerkki suojatusta resurssista

Kuvapalvelu ja kuvavarasto ovat sopineet, että salausalgoritmina käytetään bcryptia ja yhteinen salasana on "salainen." Suojattujen kuvaresurssien yhteydessä kuvapalvelu välittää asiakasjärjestelmille avaimen, jota voidaan käyttää salatun kuvaresurssin tiedostojen katseluun.

Käyttäjä hakee salatun kuvan avaimen kera:
```
GET /MS.313/salattu_full.jpg?secret=%242a%2410%24LWcymPo%2FWn3v6JVyLjm82ez01Y0hl7zKsAHCdH%2F8Jsly6qIgmyZ6m
```

Kuvavarasto dekoodaa pyynnön kohdeosoitteen avainparametrin ja saa tulokseksi "$2a$10$LWcymPo/Wn3v6JVyLjm82ez01Y0hl7zKsAHCdH/8Jsly6qIgmyZ6m." Tämän jälkeen kokeillaan, vastaako salattu avain alkuperäistä salasanaa "MS.313;salainen." Jos avain vastaa salasanaa, näytetään kuva loppukäyttäjälle.


# Esimerkki

Kuvarajapinta luo uuden suojaamattoman kuvaresurssin MS.1, ja sitä varten lataa kuvavarastoon neljä kuvaresurssiin liittyvää kuvatiedostoa. Latauksen jälkeen loppukäyttäjä haluaa katsoa yhtä kuvista.

### Kuvaresurssin kuvien lataaminen

```
Pyyntö:

POST /MS.1 HTTP/1.1
Content-Type: multipart/form-data; boundary=---123
Content-length: 456
---123
Content-Disposition: form-data; name="original"; filename="kuva.tiff"
Content-Type:image/tiff
(paljon kuvadataa)
---123
Content-Disposition: form-data; name="full"; filename="kuva_full.jpg"
Content-Type:image/jpg
(paljohkosti kuvadataa)
---123
Content-Disposition: form-data; name="square"; filename="kuva_square.jpg"
Content-Type:image/jpg
(kuvadataa)
---123
Content-Disposition: form-data; name="thumb"; filename="kuva_thumb.jpg"
Content-Type:image/jpg
(kuvadataa)
---123

Vastaus:
201 CREATED
Content-Type: application/json


{
    "original": "kuva.tiff",
    "full": "kuva_full.jpg",
    "square": "kuva_square.jpg",
    "thumb": "kuva_thumb.jpg",
}

```

On huomattava, että rajapinta voi muuttaa tiedostojen lopullisia nimiä tarpeen mukaan, lopullisen nimen on tulisi kuitenkin olla alkuperäisen nimen kaltainen (esim "huono/nimi.jpg" -> "huono_nimi.jpg" on kelvollinen muunnos, mutta "huono/nimi.jpg" -> "848a424a-aeb1-486d-b1c4-3171a52083e1.jpg" ei ole).

Kuvan latauspyynnössä käytettävät name-parametrit voivat vaihdella vapaasti, tällä mahdollistetaan uusien kuvatyyppien lisääminen ja poistaminen tulevaisuudessa.

## Kuvan katsominen

Kuvarajapinta on välittänyt loppukäyttäjälle kuvatiedoston osoitteen http://kuvavarasto.digitarium.fi/MS.1/kuva_full.jpg, loppukäyttäjän selain koittaa ladata kuvaa:

```
Pyyntö:

GET /MS.1/kuva_full.jpg HTTP/1.1

Vastaus:
200 OK
Content-Type: image/jpg
(paljohkosti kuvadataa)
```

## /:resourceID

### POST

Uuteen kuvaresurssiin liittyvien kuvien lataaminen.

#### Parametrit

Pakollinen kuvadata multipart-pyyntönä, joka voi sisältää useita kuvia. 

Multipart-pyynnön yhteydessä välitetty tiedostonnimi on käytettävä tiedoston nimi, joka tulee jossain muodossa säilyttää lopullisessa tiedostonnimessä.  Kuvatiedostojen nimet ovat vain resurssin sisällä uniikkeja. On siis mahdollista, että resursseilla MS.1 ja MS.2 on molemmilla kuva.jpg-niminen kuvatiedosto, mutta ei ole mahdollista, että MS.1-resurssilla olisi kaksi kuvaa joiden nimi olisi kuva.jpg.

Kuvatiedostoissa välitetty name-parametri vastaa kuvan tyyppiä (esim. full, medium, thumb, jne). Name-parametri on uniikki kuvaresurssin sisällä, ja voi vaihdella vapaasti.

Jos kuvaresurssi on salattu, tulee latauspyynnön yhteydessä toimittaa lomakeparametri "protected" arvolla "true."

#### Vastaus

Kuvavarasto-rajapinnan tulee palauttaa objekti, joka sisältää lopulliset tiedostonnimet kaikille ladatuille tiedostoille. Objektin kenttien nimien tulee vastata ladattujen tiedostojen name-parametria.

```
201 CREATED

{
    // jokaista name-parametria kohden tiedostonnimi
    name1 (string): lopullinen kuvatiedoston nimi
    name2 (string): lopullinen kuvatiedoston nimi
    ...
    nameN (string): lopullinen kuvatiedoston nimi
}
```

#### Muut vastaukset

* 400 INVALID_FILE
    * Pyynnön sisältö ei ole kelpo kuvatiedosto tai sitä ei ole.  
    * { names (array[string]): lista name-kentistä, jotka vastaavat epäkelpoja tiedostoja, tyhjä jos pyyntöä ei ole voitu jäsentää ollenkaan }  
* 400 TOO_LARGE
    * Kuvatiedosto on liian suuri.  
    * { maximumSizeKB (integer): enimmäiskoko kilotavuina }  
* 409 RESOURCE_ALREADY_EXISTS
    * resourceID-parametri-nimiselle resurssille on jo ladattu kuvatiedostoja
    * {}
* 503 INSUFFICIENT_SPACE
    * Kuvapalvelin ei pysty käsittelemään latauspyyntöä riittämättömän levytilan vuoksi.  
    * {}

#### Suojaus

Asiakkaan tulee olla autentikoitu palvelu.

### DELETE

Poista kuvaresurssiin liittyvät tiedostot.

#### Paramaterit

Ei muita parametreja.

#### Vastaus
```
200 OK

(ei sisältöä)
```

#### Muut vastaukset
* 404 NOT_FOUND
    * Pyydetylle kuvaresurssille ei ole ladattu tiedostoja.
    * {}

#### Suojaus

Asiakkaan tulee olla autentikoitu palvelu.

### PUT

Kuvatiedostoihin liittyvien tietojen päivitys.

#### Parametrit

Uudet tiedot JSON-objektina:
```
{
    protected (boolean): onko kuvaresurssi salattu 
}
```

#### Vastaus

```
204 No Content

(ei sisältöä)
```

## /:resourceID/:filename

### GET

Palauttaa suoraan kuvatiedoston, joka vastaa pyynnön kohdeosoitteessa olevaa resurssitunnistetta ja tiedostonnimeä.

#### Parametrit

Vapaavalintainen secret-osoiteparametri joka sisältää salausavaimen.

#### Vastaus

Kuvavaraston palauttama Content-Typen tulisi vastata multipart-latauspyynnössä käytettyä Content-Typeä.

```
200 OK

Content-Type: (kuvatiedoston alkuperäinen content-type)
(kuvadata)

```

#### Muut vastaukset

* 401 KEY_REQUIRED
    * Kuvaresurssi on suojattu ja vaatii avaimen
    * {}
* 403 INVALID_KEY
    * Käyttäjän ilmoittama avain ei kelpaa
    * {}

#### Suojaus

Suojattujen kuvaresurssien kohdalla vaaditaan validi avain.
