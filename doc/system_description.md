# Media API System Description

## Kuvan lataaminen

Jokaiselle kuvapalveluun onnistuneesti ladatulle kuvatiedostolle luodaan kuvaresurssi, jolla on uniikki tunniste. Kuvaresurssi sisältää kuvaan liittyviä metatietoja sekä osoitteet, mistä kuva ja siitä mahdollisesti luodut versiot voidaan hakea suoraan. Nämä osoitteet voidaan välittää suoraan loppukäyttäjille esimerkiksi upotettuna web-sivuun upotettuna.

Kuvatiedoston lataaminen tapahtuu kahdessa vaiheessa - varsinainen kuvatiedosto ladataan kuvapalveluun multipart-pyyntönä, mikä palauttaa väliaikaisen kuvatiedoston tunnisteen. Tämän jälkeen kuvapalveluun lähetetään luotavan kuvaresurssin metatiedot JSON-muodossa jonka ohessa on myös kuvatiedoston latauksen yhteydessä saatu väliaikainen tunniste. Kuvapalvelu luo kuvasta tarvittavat muunnokset ja lataa alkuperäisen kuvatiedostot ja muunnokset kuvavarastoon. Lopulta kuvapalvelu luo kuvaresurssin metatiedot Luomuksen ontologiakirjasto LuOntoon.

Kuvaresurssin metatietojen rakenne ja tarkka kuvaus löytyy rajapintakuvauksesta.

## Kuvamuodot

Monet alkuperäiset kuvat eivät sellaisenaan sovellu käytettäväksi esimerkiksi web-sivujen sisällöksi suuren koon tai erikoisen tiedostoformaattinsa vuoksi. Toisaalta, on myös toivottavaa, että alkuperäinen kuva säilyisi vaikka siitä tehtäisiin web-käyttöön sopivia versioita. Kuvapalvelu automaattisesti luo kuvasta erilaisia versioita tyypillisiä web-käyttöskenaarioita varten. Jos kuvasta ei tarvitse tehdä muunnosta (esim. kuva on jo tarpeeksi pieni), rajapinta palauttaa alkuperäiseen kuvatiedostoon viittaavan osoitteen erillisen kuvatiedoston sijaan.

Muunnokset ovat seuraavat:

* original - Alkuperäinen kuva sellaisenaan.
* full - Täysikokoinen kuva, joka on muunnettu jpeg-muotoon saman kokoisena.
* large - Kuva muunnettuna jpeg-kuvaksi, jonka suurin ulottuvuus on enintään 1280 pikseliä.
* square - Neliön muotoiseksi rajattu jpeg-muotoinen kuva, jonka suurin ulottuvuus on enintään 400 pikseliä.
* thumb - Kuvagallerioihin sopiva pieni jpeg-kuva, jonka suurin ulottuvuus on 150 pikseliä.

Kuvapalvelu on suunniteltu niin, että tulevaisuudessa uusien muunnosten lisääminen ja olemassa olevien muuttaminen ja poistaminen onnistuu suhteellisen helposti.

## Kuvaresurssien hakeminen

Kun asiakasjärjestelmä hakee kuvia kuvapalvelusta, se tekee kyselyn ontologiakantaan saatujen parametrien perusteella. Ontologiakannasta saadut kuvaresurssit muunnetaan helpommin tulkittaviksi olioiksi ja lähetetään JSON-vastauksena 


## Protokolla

Kuvapalvelua käytetään HTTP/JSON-rajapinnan kautta. Toistaiseksi kuvapalvelu ei tarjoa CORSin tai JSONP:n kaltaisia ratkaisua cross-origin-pyyntöjä varten ja kaikki pyynnöt vaativat HTTP basic auth-tunnistautumisen.

Tarkat kuvaukset rajapinnan käyttämisestä löytyvät rajapintakuvauksesta.

## Suojatut kuvaresurssit

Kuvapalveluun ladattavat kuvatiedostot voidaan määritellä salaisiksi, jolloin vain ylläpitäjät sekä kuvan järjestelmään alun perin ladannut asiakasjärjestelmä voi tarkastella kuvan tietoja. Kuvavarasto ei anna hakea suojattujen kuvaresurssien kuvatiedostoja ilman käyttöavainta. Jos asiakasjärjestelmä haluaa esimerkiksi upottaa suojatun kuvaresurssin kuvatiedostoja web-sivuihin, tulee kuvatiedoston osoitteeseen liittää parametriksi kuvapalvelun antama käyttöavain.

### Esimerkki

Asiakasjärjestelmä hakee lataamansa salaiseksi määritellyn kuvaresurssin tiedot kuvapalvelusta:

Kuvarajapinta palauttaa suojatun kuvaresurssin:

```
Pyyntö (autentikoitu):
GET /images/MS.313

Vastaus:
200 OK

Media {
        "urls": {
        ...
        "full": "http://kuvavarasto.digitarium.fi/MS.123/kuva_full.jpg"
        }
        ...
        "secretKey": "ew0KICAgImlkIjogIk1TLjEyMyIsDQogICAic2VjcmV0IjogIiQyYSQxMCRHVU9JbDh3WWxSSGY5c0JDakRzYWMuZG54Ym5ub2gzeXptTTlQR1oxbVFWN216WWN0bk1wbSINCn0="
}
```

Asiakasjärjestelmä välittää avaimen loppukäyttäjälle parametrina:

```
...
<img src="http://imagestore-test.example.com/MS.123/kuva_full.jpg?secret=ew0KICAgImlkIjogIk1TLjEyMyIsDQogICAic2VjcmV0IjogIiQyYSQxMCRHVU9JbDh3WWxSSGY5c0JDakRzYWMuZG54Ym5ub2gzeXptTTlQR1oxbVFWN216WWN0bk1wbSINCn0%3D" alt="Huippusalainen kuva" />
...
```
