See also
* [MM.image](http://tun.fi/MM.image)
* [MM.audio](http://tun.fi/MM.audio)
* [MM.video](http://tun.fi/MM.video)
* [MM.model](http://tun.fi/MM.media)

```
MM.123
 rdf:type [MM.multimediaObject > MM.image | MM.audio | MM.video | MM.model]	1
 
 MM.originalURL [literal]					1
 MM.fullURL [literal]							1
 MM.largeURL [literal]						1
 MM.squareThumbnailURL [literal]			1
 MM.thumbnailURL [literal]					1
 MM.mp3URL [literal]							0/1..1
 MM.wavURL [literal]							0..1
 MM.flacURL [literal]							0..1
 MM.videoURL [literal]						0/1..1
 MM.lowDetailModelURL [literal]				0..1
 MM.highDetailModelURL [literal]			0..1
 
 MM.sourceSystem [KE.informationSystem]	1
 MM.originalFilename [literal]				1
 MM.uploadedBy [literal]						1
 MM.modifiedBy [literal]						1
 
 MM.capturerVerbatim [literal]				0..n
 MM.captureDateTime [xsd:dateTime]			0..1   (2013-10-23T08:15:59+0300)

 MZ.intellectualOwner [literal]										1
 MZ.intellectualRights [MZ.intellectualRightsEnum]			1
 
 MM.taxonURI [MX.taxon]						0..n
 MM.taxonVerbatim [literal]					0..n 
 MM.documentURI [literal]					0..1 
 
 MM.caption								0..1
 MM.taxonDescriptionCaption			0..1 (multilang)
 
 MM.type [MM.typeEnum]							0..1
 MM.side [MM.sideEnum]							0..1
 MY.lifeStage [MY.lifeStages]					0..n
 MY.plantLifeStage [MY.plantLifeStageEnum]	0..n
 MY.sex [MY.sexes]									0..n
 MM.primaryForTaxon [MX.taxon]					0..n
 MM.keyword [literal]								0..n
 
 MZ.publicityRestrictions [literal:PUBLIC|PRIVATE]

 sortOrder [xsd:integer]		0..1 
 
```
 
 
 