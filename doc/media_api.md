# General

All errors contain - in addition to the HTTP staus code - a JSON-object, which contains "code" and possible additional information. Check the responses for details.

All responses are in JSON-format, unless otherwise mentioned.

# Authentication and authorization

All methods require HTTP Basic authentication. When accessing without proper authorization, response is HTTP 401 (error code "UNAUTHORIZED").

When accessing secured media object without proper authorization, response is HTTP 403 (error code "ACCESS_DENIED").

Each client will has their own identifier (stored to MM.sourceSystem of media object metadata). These identifiers are KE.informationSystem identifiers (KE.123). 

Client can only modify and delete media resources creatd by the client. If the client has been granted admin role (kuvapalvelu-admin), then that client can modify and delete all media objects.

A client is an API user: if the system using Media-API wants to implement higher granularity resource permissions (for example for real-life persons), the using system must implement these in the system's own logic.

## Common responses

* 400 INVALID_PARAMETERS
    * One or more of parameters do not pass validations
    * { fields: { name(String), cause(String) }
* 401 UNAUTHORIZED
    * Requires proper authorization  
    * {}
* 403 ACCESS_DENIED
    * No permissions to modify the media object  
    * {}

## /fileUpload/

Creating a new media resource is a two-step process. First step is for uploading the media files. This returns a temporary id. 
(Second step is to upload metadata for the temporary media resource which will actually create the media resource)
 
### POST

Consumes multipart/form-data.

Each body part is a media resource. 

You can give a name for the body part. If you use any of the following as the name of the bodypart, this helps Media-API determining type of the media item for validation checks: 
WAV, MP3, FLAC, VIDEO, LOW_DETAIL_MODEL, HIGH_DETAIL_MODEL

Each body part has content-disposition which contains original name of the file.

#### Parameters

* mediaClass - If you provide one of the following, this helps Media-API determining type of the media item for validation checks:
IMAGE, AUDIO, VIDEO, MODEL 

#### Response

```
201 CREATED

array[FileUploadResponse]

FileUploadResponse { 
	name (string): multipart body part name,
	fileName (string): multipart body part content-disposition file name,
	id(string): temporary id for the media resource, 
	expires(integer): timestamp, expiry from temporary file repository 
} 
```

#### Other responses

* 400 INVALID_FILE
    * File does not pass checks (invalid media format or other error)  
    * { error(string), filename(string), name(string) }

#### Authorization

Client must have kuvapalvelu-client or kuvapalvelu-admin role.

## /images/ /audio/ 

### POST

Second step: Provide metadata for a temporary resource to create the resource.

#### Parameters

No parameters.

Body must have JSON array, containing the following:

```
array[NewMedia]

NewMedia {
    tempFileId (string): temporary id
    meta (Meta): media metadata
}

Meta {
    license (string, REQUIRED): License - must be one of MZ.intellectualRightsEnum (see /licenses endpoint)
    rightsOwner(string, REQUIRED): Name of copyright owner (license)

    secret(boolean): Is media resource public or secured? true - secured, false - public

    capturers (array[string]): author/photographer names
    captureDateTime (Datetime): ISO 8601
    uploadedBy(string): User id of user uploading the image (MA.123 or other id)
    modifiedBy(string): User id of user modifying image metadata (MA.123 or other id)
    
    originalFilename (string): original file name

    documentId (string): To which MY.document this media is related to
    tags (array[string]): free-form keywords

    identifications (Identifications): to which taxa this media is related to 
    primaryForTaxon (array[string]): for which taxon identifiers should this media be the primary media 
    
    caption (string): free-form caption
    taxonDescriptionCaption (LocalizedString): localized description for taxon pages

    sex (array[string]): sex(es) present in the media - values MY.sexes (see /sexes endpoint)
    lifeStage (array[string]): lifestage(s) present in the media - values MY.lifeStages (see /lifeStages endpoint)
    plantLifeStage (array[string]): plant lifestages() present in the media - values MY.plantLifeStageEnum (see /plantLifeStages endpoint)
    side (string): upside/downside - values MM.sideEnum (see /sides endpoint)
    type (string): type of the taxon image - values MM.typeEnum (see /types endpoint)
    
    sortOrder (int): for taxon media, order of the media
}

Note - the Meta object also contains the following but they are set by the API (if provided by the client, will be overriden)
    uploadedDateTime (DateTime): milloin kuva on ladattu,
    modifiedDateTime (DateTime): milloin metadataa on viimeksi muokattu
    sourceSystem (string): järjestelmä mistä kuva on ladattu (ontologiakannan tunniste),
    
Identifications {
    taxonIds (array[string]): taxon identifiers (MX.123)
    verbatim (array[string]): verbatim taxon names
}

LocalizedString {
	fi (string)
	en (string)
	sv (string)
}

```

#### Response

Created media resources, which contain identifiers

```
201 CREATED

array[Media]

Media {
	id (string),
	secretKey (string),
	urls (Urls),
	meta (Meta)
}

Urls {
    original (Uri): image in original format - but with stripped exif-data,
    full (Uri): full resolution image converted to jpeg,
    large (Uri): image, max 1280px width or height,
    square (Uri): square image thumbnail, max 400px,
    thumbnail (Uri): image thumbnail, max 200px width or height,
    mp3 (Uri): audio in MP3 format,
    wav (Uri): audio in WAV format (not present if mp3 or flac uploaded),
    flac (Uri): audio in FLAC format (not present if mp3 or wav uploaded),
    video (Uri): video (untouched),
    lowDetailModel (Uri): low detail model (untouched),
    highDetailModel (Uri): high detail model (untouched) 
}
```

#### Muut vastaukset

* 404 TEMP_FILE_NOT_FOUND
    * Temporary id was not found  
    * { missingTempFiles(array[string]): missing identifiers }



## /video/multi/ /model/multi/ 

### POST

Second step: Provide metadata for a temporary resource to create the resource. Used to join together multiple temporary file uploads as a single media object. For example video with video file and thumbnail image.

#### Parameters

No parameters.

Body must have JSON array, containing the following:

```
array[NewMultiSourceMedia]

NewMultiSourceMedia {
	entries (array(MediaFile)),
	meta (Meta)
}

MediaFile {
	tempFileId (string),
	type (MediaFileType),
	filename (String, optional)
}

MediaFileType: 
ORIGINAL_IMAGE|FULL_IMAGE|LARGE_IMAGE|THUMBNAIL_IMAGE|SQUARE_THUMBNAIL_IMAGE|WAV|MP3|FLAC|VIDEO|LOW_DETAIL_MODEL|HIGH_DETAIL_MODEL

```

#### Response

Created media resources, which contain identifiers

```
201 CREATED

array[Media]

Media {
	id (string),
	secretKey (string),
	urls (Urls),
	meta (Meta)
}

```

#### Other responses

* 404 TEMP_FILE_NOT_FOUND
    * Temporary id was not found  
    * { missingTempFiles(array[string]): missing identifiers }
    
## /images/:id /audio/:id /video/:id /model/:id

### GET 

Fetch media object identified by the id.

#### Parameters
 
No additional parameters to the identifier (in path)

#### Response

Media item if it exists

```
200 OK

Media {
    ...
}
```

#### Other responses

* 404 NOT_FOUND
    * Nothing found with the identifier
    * {}
* 403 PROTECTED
    * Api user does not have access to the media object (must be owner of the object or admin)
    * {}

### PUT

Update media object metadata identified by the id.

#### Parameters

Body contains new metadata in JSON

```
Meta {
    ...
}
```

#### Response

```
204 No Content

```

#### Authorization

User must be owner of the media object or an admin.


### DELETE

Delete media object identifier by the id. Deletes metadata and the physical files from image-store.

#### Parameters

No additional parameters to the identifier (in path)

#### Response

```
204 No content
```

#### Other responses
* 404 NOT_FOUND
    * Nothing found with the identifier
    * {}
* 403 PROTECTED
    * Api user does not have access to the media object (must be owner of the object or admin)
    * {}
    
#### Authorization

User must be owner of the media object or an admin.


## Seach API

### GET /images /audio /video /model

Query media objects using the given criteria.

#### Parameters

Query  must have at least one parameter that matches a field in the Meta datamodel. Each query parameter can have several values.

When using parameter identifications.taxonIds it is possible to also provide searchForSubTaxons=true parameter, which will also find subtaxa of the given search taxon id(s).

Limit and offset: By default provides first 1000 results. 

Will only return media objects that are of the queried type (image, audio, ..)

Examples:

``` 
/images?documentId=foo&documentId=bar
/images?identifications.taxonIds=MX.12345 
```

#### Response

```
200 OK

{
    result (array[Media])
}

```

## Enumeration endpoints

The following endpoints provide allowed values of their respective Meta object fields:

 * /licenses
 * /lifeStages
 * /plantLifeStages
 * /sexes
 * /sides
 * /types
 
### Parameters

No additional parameters

### Response

```
200 OK

{
    result (array[NamedResource])
}

NamedResource {
	id (string): resource identifier (like "MZ.intellectualRightsCC-BY-NC-4.0" or "MY.sexF"),
	label (LocalizedString): label in fi, sv, en 
}

LocalizedString {
	fi (string)
	en (string)
	sv (string)
}

```
