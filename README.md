# Media-API (aka "kuvapalvelu")

Media-API is a Luomus/FiNBIF service that allows storing and querying for media objects using a REST-API interface.

All endpoints require authentication (either media-admin or media-client). This is configured in [web.xml](src/main/webapp/WEB-INF/web.xml).

Credentials (usernames and passwords are stored to server configuration - for example tomcat-users.xml) 

In addition to the media files, metadata is given for each media object. See [schema](doc/ontology_terms.md).

Transformations (such as thumbnails) are done while uploading a media object. There is support currently for the following:
* Images: Common image formats such as JPG, PNG, GIF and HEIC/HEIF (usd by iPhones) - Makes transformations (thumbnails)
* Audio: Wav and MP3 - converts Wav files also to mp3 (but doesn't convert mp3 into wav) - Spectogram image + thumbnails are generated
* Video (multi-type only): MP4 only allowed at this point - Upload video and provide a thumbnail image: can't generate thumbnails from video files currently
* Model (multi-type only): STL only allowed at this point - Upload low or high-detail model and provide thumbnail image; also a video can be provided of the model; can't generate thumbnails or video convertions currently

Multi type only* = See API description for more details.

Media files are stored to "image store".

Media metadata is stored to Triplestore (LuOnto) ontology/rdf database.

## Additional documentation

* [System description](doc/system_description.md)
* [Media-API API](doc/media_api.md)
* [Image store API](doc/imagestore_api.md)

Usage example

```
curl -X POST -v -F file=@filename.wav http://user:pass@localhost:9091/api/fileUpload
```

## Media-API Client

There is a separate Java Client for using Media API: [media-api-client](https://bitbucket.org/luomus/kuvapalvelu-client).

## Dev environment 

Media-API is a Java web app, which needs Java 8 server. For test and production Apache Tomcat 8 server is reccomendend.
Unit test and manual local testing use Jetty server.

### Dependencies (Centos9)

```
# yum install java-1.8.0-openjdk-devel maven ImageMagick sox perl-Image-ExifTool
# note: if perl-Image-ExifTool does not exist, try exiftool or libimage-exiftool-perl
```

The following must be manually cloned from git and uploaded to local maven repository:
 * [utils](https://bitbucket.org/luomus/utils)
 * [kuvapalvelu-client](https://bitbucket.org/luomus/kuvapalvelu-client)
 * [triplestore-client](https://bitbucket.org/luomus/triplestore-client)

```
$ git clone git@bitbucket.org:luomus/utils.git
$ cd utils && mvn install && cd ..

$ git clone git@bitbucket.org:luomus/triplestore-client.git
$ cd triplestore-client && mvn install && cd ..

$ git clone git@bitbucket.org:luomus/kuvapalvelu-client.git
$ cd kuvapalvelu-client && mvn install && cd ..
```

Unit tests should now pass

```
$ mvn integration-test
```

Start up test server for configuration:

```
$ cd kuvapalvelu-server
$ mvn jetty:run
```

* Go to http://localhost:9091/admin/preferences 
* Username/password is defined in [src/test/resources/testrealm.properties](src/test/resources/testrealm.properties] (admin/password)
* Set settings (see bellow).
* Close test server 

Tests should now pass

```
$ mvn integration-test
```

### Deploy

Option 1: Use mvn deploy scriots
Option 2 (perhaps preferred): Generate war and manually use Tomcat manager to undeploy (+restart) + deploy

To use option 1 you must add test and prod server information to Maven configuration

```
$ vi ~/.m2/settings.xml
<settings>
...
<servers>
    ...
	<server>
		<id>fmnh-ws-test</id>
		<username>admin-deploy</username>
		<password>********</password>
	</server>
	<server>
		<id>fmnh-imageapi</id>
		<username>deploy</username>
		<password>**********</password>
	</server>
</servers>
...
</settings>
```

After that you can use

```
$ mvn -P staging tomcat:undeploy tomcat:deploy
```

For option 2, to generate a war you can run

```
$ mvn clean package
```

### Users and settings

All request require authentication. Users and credentials are managed on the server. There are two user roles: client and admin. 

API-users with client role (kuvapalvelu-client) may create media resources and modify and delete media resources created by the client. Clients may query all non-secret media resources and secret media resources created by them.

API-users with admin role (kuvapalvelu-admin) may modify, delete and query all media resources.

Preferences are updated using a web HTML UI. This requires user with admin role. Preferences is located in /admin/preferences.

* *DEV_MODE* - true, if local dev or test server environment, false for production
* *IMAGE_QUALITY* - Number between 0-100 for jpeg compression. Currently 75 is used in production.
* *IMAGE_REPOSITORY_URL* - Image store address (include trailing /)
* *IMAGE_REPOSITORY_USERNAME, IMAGE_REPOSITORY_PASSWORD* - Image store username and password.
* *IMAGE_REPOSITORY_SECRET* - Shared secred between Media API and image store; used in create secret media keys. Should be a random string configured to be the same in media-api and image store.
* *MAIL_RECIPIENTS* - Error message reporting (separate multiple with ;)
* *MAIL_SERVER* - SMTP-server for error reporting
* *TEMPORARY_FILE_LOCATION* - Temporary folder. If empty, uses OS default temporary storage.
* *TRIPLESTORE_URL* - Triplestore API address (include trailing /)
* *TRIPLESTORE_USERNAME, TRIPLESTORE_PASSWORD* - Triplestoren API username and password
