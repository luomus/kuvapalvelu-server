package fi.luomus.kuvapalvelu.service;

import static fi.luomus.utils.model.LocalizedString.localizedString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.google.common.base.Optional;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.Urls;
import fi.luomus.kuvapalvelu.repository.MapMockSource;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepositoryImpl;
import fi.luomus.kuvapalvelu.repository.ResourceMockSource;
import fi.luomus.triplestore.client.AbstractSearchQuery;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.TriplestoreClient.TooManyResultsException;
import fi.luomus.utils.exceptions.ApiException;

public class MediaMetaDataRepositoryImplTest {

	@Test
	public void testInvalid() throws Exception {
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(new ResourceMockSource());
		try {
			mediaMetaDataRepository.get("MX.123");
			fail("exception not raised");
		} catch (MediaMetaDataRepositoryImpl.NotMediaMetadataException e) {
			//nop
		}
	}

	@Test
	public void testGet() throws Exception {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		when(mock.getResource("MM.1")).thenReturn(Optional.of(getModel("MM.1")));
		MediaMetaDataRepository.MetaAndUrls metaAndUrls = mediaMetaDataRepository.get("MM.1").get();
		Meta meta = metaAndUrls.meta;

		assertEquals(1, meta.getCapturers().size());
		assertEquals("Kalle Kuvaaja", meta.getCapturers().get(0));

		assertEquals(2, meta.getTags().size());
		assertTrue(meta.getTags().contains("söpö"));
		assertTrue(meta.getTags().contains("hurja"));

		assertTrue(Math.abs(DateTime.parse("2014-01-01T12:00:13").getMillis() - meta.getCaptureDateTime().getMillis()) < 1000);
		assertEquals(new Boolean(false), meta.isSecret());
		assertEquals("kalle420", meta.getUploadedBy());
		assertEquals("kissakuvat oyj", meta.getRightsOwner());
		assertEquals("kuva.tiff", meta.getOriginalFilename());
		assertEquals("KE.176", meta.getSourceSystem());
		assertEquals("Kuvassa on kissani Söpö", meta.getCaption());
		assertEquals("Kissoilla voi olla raitoja", meta.getTaxonDescriptionCaption().get("fi"));
		assertEquals("Cats may have stripes", meta.getTaxonDescriptionCaption().get("en"));
		assertEquals(null, meta.getTaxonDescriptionCaption().get("sv"));
		Meta.Identifications identifications = meta.getIdentifications();
		assertEquals(1, identifications.getTaxonIds().size());
		assertEquals("MX.46639", identifications.getTaxonIds().get(0));
		assertEquals(1, identifications.getVerbatim().size());
		assertEquals("kissa", identifications.getVerbatim().get(0));

		Urls urls = metaAndUrls.urls;
		assertEquals(new URI("http://kuva.digitarium.fi/MM.1/kuva.tiff"), urls.getOriginal());
		assertEquals(new URI("http://kuva.digitarium.fi/MM.1/kuva_full.jpg"), urls.getFull());
		assertEquals(new URI("http://kuva.digitarium.fi/MM.1/kuva_large.jpg"), urls.getLarge());
		assertEquals(new URI("http://kuva.digitarium.fi/MM.1/kuva_square.jpg"), urls.getSquare());
		assertEquals(new URI("http://kuva.digitarium.fi/MM.1/kuva_thumb.jpg"), urls.getThumbnail());
	}

	@Test
	public void testSaveGet() throws Exception {
		Meta meta = new Meta()
				.setSourceSystem("KE.123")
				.setSecret(true)
				.setCaptureDateTime(new DateTime())
				.addTag("bar")
				.setLicense("MY.stallman")
				.addDocumentId("http://id.luomus.fi/KE.123/abc")
				.setUploadedBy("uploader")
				.addCapturer("kuvaaja")
				.addTaxonId("MX.124")
				.setSecret(true)
				.setCaption("Kuvateksti")
				.addTaxonDescriptionCaption("Suomi", "fi")
				.addTaxonDescriptionCaption("På svenska", "sv")
				.setFullResolutionMediaAvailable(true);

		MediaMetaDataRepository.MetaAndUrls source = new MediaMetaDataRepository.MetaAndUrls(meta, new Urls());

		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(new MapMockSource());

		String id = mediaMetaDataRepository.newId();
		assertNotNull(id);
		mediaMetaDataRepository.save(id, source.meta, source.urls, MediaClass.IMAGE);

		Meta gotMeta = mediaMetaDataRepository.get(id).get().meta;
		assertTrue(Math.abs(source.meta.getCaptureDateTime().getMillis() - gotMeta.getCaptureDateTime().getMillis()) < 1000);
		source.meta.setCaptureDateTime(gotMeta.getCaptureDateTime());
		assertEquals(source.meta, gotMeta);
	}

	@Test
	public void testValidSearch() throws Exception {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		TriplestoreClient.SearchQuery mockQuery = spy(new AbstractSearchQuery("literal") {
			@Override
			protected Model actualExecute() throws ApiException {
				return ModelFactory.createDefaultModel();
			}
		});
		when(mock.searchLiteral()).thenReturn(mockQuery);
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		mediaMetaDataRepository.search("originalFilename", Arrays.asList("foo"), 1000, 0, MediaClass.IMAGE);
		verify(mockQuery, times(1)).execute();
		verify(mockQuery).limit(1000);
		verify(mockQuery).offset(0);
		verify(mockQuery).object(Arrays.asList("foo"));
		verify(mockQuery).predicate("MM.originalFilename");
	}

	@Test
	public void testValidNestedObjectSearch() throws Exception {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		TriplestoreClient.SearchQuery mockQuery = spy(new AbstractSearchQuery("literal") {
			@Override
			protected Model actualExecute() throws ApiException {
				return ModelFactory.createDefaultModel();
			}
		});
		when(mock.searchResource()).thenReturn(mockQuery);
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		mediaMetaDataRepository.search("identifications.taxonIds", Arrays.asList("foo"), 1000, 0, MediaClass.IMAGE);
		verify(mockQuery, times(1)).execute();
		verify(mockQuery).limit(1000);
		verify(mockQuery).offset(0);
		verify(mockQuery).object(Arrays.asList("foo"));
		verify(mockQuery).predicate("MM.taxonURI");
	}

	@Test
	public void testNotMappedFieldSeach() throws Exception {
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock(TriplestoreClient.class));
		try {
			mediaMetaDataRepository.search("secret", Arrays.asList("foo"), 1000, 0, MediaClass.IMAGE);
			fail("exception not raised");
		} catch (MediaMetaDataRepository.InvalidParameterException e) {
			assertEquals(1, e.getInvalidParams().size());
			assertEquals("secret", e.getInvalidParams().get(0));
		}
	}

	@Test
	public void testNonExistentFieldSearch() throws Exception {
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock(TriplestoreClient.class));
		try {
			mediaMetaDataRepository.search("doesnotexist", Arrays.asList("foo"), 1000, 0, MediaClass.IMAGE);
			fail("exception not raised");
		} catch (MediaMetaDataRepository.InvalidParameterException e) {
			assertEquals(1, e.getInvalidParams().size());
			assertEquals("doesnotexist", e.getInvalidParams().get(0));
		}
	}

	@Test
	public void testSearchModel() throws Exception {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		TriplestoreClient.SearchQuery mockQuery = queryWithResult("searchresult");
		when(mock.searchLiteral()).thenReturn(mockQuery);
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		List<MediaMetaDataRepository.WithId> search =
				mediaMetaDataRepository.search("originalFilename", Arrays.asList("bar"), 1000, 0, MediaClass.IMAGE);
		assertEquals(2, search.size());
		Set<String> ids = new HashSet<>();
		for (MediaMetaDataRepository.WithId withId : search) {
			ids.add(withId.id);
		}
		assertTrue(ids.contains("MM.69"));
		assertTrue(ids.contains("MM.70"));
	}

	@Test
	public void testSearchSortOrder() throws Exception {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		TriplestoreClient.SearchQuery mockQuery = queryWithResult("searchresult_with_sort_order");

		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		when(mock.searchLiteral()).thenReturn(mockQuery);
		List<MediaMetaDataRepository.WithId> search =
				mediaMetaDataRepository.search("originalFilename", Arrays.asList("bar"), 1000, 0, MediaClass.IMAGE);
		assertEquals(4, search.size());
		assertEquals("MM.72", search.get(0).id);
		assertEquals("MM.70", search.get(1).id);
		assertEquals("MM.69", search.get(2).id);
		assertEquals("MM.71", search.get(3).id);
	}

	@Test
	public void getLicenses() throws ApiException, TooManyResultsException {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		MediaMetaDataRepositoryImpl mediaMetaDataRepository = new MediaMetaDataRepositoryImpl(mock);
		when(mock.getResource(anyString(), anyMapOf(String.class, String.class))).thenAnswer(new Answer<Optional<Model>>() {
			@Override
			public Optional<Model> answer(InvocationOnMock invocation) throws Throwable {
				String resourceName = (String) invocation.getArguments()[0];
				return Optional.of(getModel(resourceName));
			}
		});
		when(mock.getResource(anyString())).thenAnswer(new Answer<Optional<Model>>() {
			@Override
			public Optional<Model> answer(InvocationOnMock invocation) throws Throwable {
				String resourceName = (String) invocation.getArguments()[0];
				return Optional.of(getModel(resourceName));
			}
		});
		List<License> licenses = mediaMetaDataRepository.getLicenses();

		assertEquals(2, licenses.size());
		License license = licenses.iterator().next();
		assertEquals("MZ.intellectualRightsCC-BY-SA-4.0", license.getId());
		assertEquals(localizedString().with("en", "c").with("fi", "d").get(), license.getLabel());
	}


	private Model getModel(String name) throws IOException {
		return ModelFactory.createDefaultModel().read(this.getClass().getResource("/triplestore/" + name + ".xml").openStream(), "");
	}

	private TriplestoreClient.SearchQuery queryWithResult(final String name) {
		return spy(new AbstractSearchQuery("literal") {
			@Override
			protected Model actualExecute() throws ApiException {
				try {
					return getModel(name);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		});
	}

}
