package fi.luomus.kuvapalvelu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepositoryMock;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.preferences.MapBackedLuomusPreferences;

public class PdfTransformerTest {

	private PdfTransformer pdfTransformer;
	private TransformedMedia transformedMedia;

	@Before
	public void setup() {
		pdfTransformer = new PdfTransformer(new MapBackedLuomusPreferences(
				ImmutableMap.<String, Object>builder().put(MediaApiPreferences.IMAGE_QUALITY.key, "75").build()),
				new TempFileRepositoryMock());
		transformedMedia = null;
	}

	@After
	public void after() throws Exception {
		if (transformedMedia != null) {
			transformedMedia.close();
		}
	}

	@Test
	public void testNonPdf() throws Exception {
		File pdf = new File(this.getClass().getClassLoader().getResource("test.pdf").toURI());
		File notPdf = new File(this.getClass().getClassLoader().getResource("bird.jpeg").toURI());
		assertTrue(pdfTransformer.check(pdf).isTransformable());
		assertFalse(pdfTransformer.check(notPdf).isTransformable());
		assertEquals("Not a PDF", pdfTransformer.check(notPdf).getError());
	}

	@Test
	public void testConverting() throws Exception {
		File pdf = new File(this.getClass().getClassLoader().getResource("test.pdf").toURI());
		transformedMedia = pdfTransformer.transform(pdf, pdf.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();
		ImageTransformerTest.assertJpegWithSize(transformed.get(MediaFileType.FULL_IMAGE), 612, 792);
		ImageTransformerTest.assertJpegWithSize(transformed.get(MediaFileType.LARGE_IMAGE), 612, 792);
		ImageTransformerTest.assertJpegWithSize(transformed.get(MediaFileType.THUMBNAIL_IMAGE), 154, 199);
		ImageTransformerTest.assertJpegWithSize(transformed.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE), 400, 400);
	}

}
