package fi.luomus.kuvapalvelu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepositoryMock;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.preferences.MapBackedLuomusPreferences;

public class AudioTransformerTest {

	private AudioTransformer transformer;
	private TransformedMedia transformedMedia;

	@Before
	public void setup() {
		transformer = new AudioTransformer(new MapBackedLuomusPreferences(
				ImmutableMap.<String, Object>builder().put(MediaApiPreferences.IMAGE_QUALITY.key, "75").build()),
				new TempFileRepositoryMock()
				);
	}

	@After
	public void after() throws Exception {
		if (transformedMedia != null) {
			transformedMedia.close();
		}
	}

	@Test
	public void testNonAudio() throws Exception {
		File wav = new File(this.getClass().getClassLoader().getResource("bat.wav").toURI());
		File mp3 = new File(this.getClass().getClassLoader().getResource("bird.mp3").toURI());
		File flac = new File(this.getClass().getClassLoader().getResource("bat.flac").toURI());
		File notAudio = new File(this.getClass().getClassLoader().getResource("logback-test.xml").toURI());
		File exe1 = new File(this.getClass().getClassLoader().getResource("really_an_exe.mp3").toURI());
		File exe2 = new File(this.getClass().getClassLoader().getResource("really_an_exe.wav").toURI());

		assertTrue(transformer.check(wav).isTransformable());
		assertTrue(transformer.check(mp3).isTransformable());
		assertTrue(transformer.check(flac).isTransformable());

		assertFalse(transformer.check(notAudio).isTransformable());
		assertFalse(transformer.check(exe1).isTransformable());
		assertFalse(transformer.check(exe2).isTransformable());

		assertEquals("Not valid extension: xml <> [wav, mp3, flac]", transformer.check(notAudio).getError());
		assertEquals("Unknown media type", transformer.check(exe1).getError());
		assertEquals("Unknown media type", transformer.check(exe2).getError());
	}

	@Test
	public void testWav() throws Exception {
		File wav = new File(this.getClass().getClassLoader().getResource("bat.wav").toURI());
		testTransform(wav, MediaFileType.WAV);
	}

	@Test
	public void testMp3() throws Exception {
		File wav = new File(this.getClass().getClassLoader().getResource("bird.mp3").toURI());
		testTransform(wav, MediaFileType.MP3);
	}

	@Test
	public void testFlac() throws Exception {
		File flac = new File(this.getClass().getClassLoader().getResource("bat.flac").toURI());
		testTransform(flac, MediaFileType.FLAC);
	}

	@Test
	public void testTransformNonAudio() throws Exception {
		File tiff = new File(this.getClass().getClassLoader().getResource("tiny.tiff").toURI());
		try {
			testTransform(tiff, null);
			fail("Should fail");
		} catch (Exception e) {
			assertTrue(e.getMessage().startsWith("Audio transform failed"));
		}
	}

	private void testTransform(File source, MediaFileType originalType) throws IOException {
		transformedMedia = transformer.transform(source, source.getName());

		Map<MediaFileType, File> transformedFiles = transformedMedia.getFilesByType();
		assertNull(transformedFiles.get(MediaFileType.LARGE_IMAGE));
		assertNull(transformedFiles.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE));
		assertNull(transformedFiles.get(MediaFileType.ORIGINAL_IMAGE));

		ImageTransformerTest.assertPng(transformedFiles.get(MediaFileType.FULL_IMAGE));
		ImageTransformerTest.assertJpeg(transformedFiles.get(MediaFileType.THUMBNAIL_IMAGE));

		File transformedAudio = transformedFiles.get(originalType);
		assertNotNull("Should transform a file", transformedAudio);
		assertTrue("Transformed file should exist", transformedAudio.exists());
		assertNotEquals("Should convert not accept files as they are", source, transformedAudio);
		assertEquals("Original file name should stay the same", source.getName(), transformedAudio.getName());
		assertTrue("Does not delete source (yet)", source.exists());

		if (originalType == MediaFileType.WAV || originalType == MediaFileType.FLAC) {
			File mp3 = transformedFiles.get(MediaFileType.MP3);
			assertNotNull("Wav/flac->mp3: should convert mp3", mp3);
			assertTrue("Wav/flac->mp3: mp3 file should exist", mp3.exists());
			assertNotEquals("Wav/flac->mp3: should have new file for mp3", source, mp3);
			assertEquals(FilenameUtils.removeExtension(source.getName())+".mp3", mp3.getName());
		} else if (originalType == MediaFileType.MP3) {
			File wav = transformedFiles.get(MediaFileType.WAV);
			assertNull("Mp3->mp3: should not convert a wav", wav);
			File flac = transformedFiles.get(MediaFileType.FLAC);
			assertNull("Mp3->mp3: should not convert a flac", flac);
		} else {
			fail("Unknown media uri type " + originalType);
		}
	}

	@Test
	public void testRename() throws Exception {
		File source = new File(this.getClass().getClassLoader().getResource("bat.wav").toURI());
		transformedMedia = transformer.transform(source, "renamed.wav");
		assertEquals("renamed.wav", transformedMedia.getFilesByType().get(MediaFileType.WAV).getName());
		for (File transformed : transformedMedia.getFilesByType().values()) {
			assertTrue("Should contain renamed but was " + transformed.getName(), transformed.getName().contains("renamed"));
			assertFalse("Should not contain bat but was " + transformed.getName(), transformed.getName().contains("bat"));
			assertTrue("Transformed file should exist", transformed.exists());
		}
		assertTrue(source.exists());
	}

}