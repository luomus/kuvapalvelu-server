package fi.luomus.kuvapalvelu.service;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NamedResource;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Side;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.kuvapalvelu.model.Urls;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

public class CachedMetaServiceTest {

	private CachedEnumsService service;
	private Map<String, Integer> concreteMetaRepoCallCounts;

	@Before
	public void setup() {
		concreteMetaRepoCallCounts = new TreeMap<>();
		service = new CachedEnumsService(new MediaMetaDataRepository() {

			@Override
			public List<Type> getTypes() throws ApiException {
				count("types", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("type1", Type.class), create("type2", Type.class));
			}

			@Override
			public List<Side> getSides() throws ApiException {
				count("sides", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("side1", Side.class), create("side2", Side.class));
			}

			@Override
			public List<Sex> getSexes() throws ApiException {
				count("sexes", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("sex1", Sex.class), create("sex2", Sex.class));
			}

			@Override
			public List<LifeStage> getPlantLifeStages() throws ApiException {
				count("plantLifeStages", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("plstage1", LifeStage.class), create("plstage2", LifeStage.class));
			}

			@Override
			public List<LifeStage> getLifeStages() throws ApiException {
				count("lifeStages", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("stage1", LifeStage.class), create("stage2", LifeStage.class));
			}

			@Override
			public List<License> getLicenses() throws ApiException {
				count("licenses", concreteMetaRepoCallCounts);
				return Lists.newArrayList(create("license1", License.class), create("license2", License.class));
			}

			@Override
			public Optional<MetaAndUrls> get(String id) throws ApiException { return null; }
			@Override
			public void delete(String id) throws ApiException, NotFoundException {}
			@Override
			public List<WithId> search(String field, List<String> values, int limit, int offset, MediaClass mediaClass) throws ApiException, InvalidParameterException { return null; }
			@Override
			public void save(String id, Meta meta, Urls urls, MediaClass mediaClass) throws ApiException { }
			@Override
			public String newId() throws ApiException { return null; }
			@Override
			public List<String> getSubTaxa(String taxonId) throws ApiException { return null; }

			private void count(String key, Map<String, Integer> concreteMetaRepoCallCounts) {
				if (concreteMetaRepoCallCounts.containsKey(key)) {
					concreteMetaRepoCallCounts.put(key, concreteMetaRepoCallCounts.get(key) + 1);
				} else {
					concreteMetaRepoCallCounts.put(key, 1);
				}

			}

			private <T extends NamedResource> T create(String id, Class<T> resoureClass) {
				try {
					T inst = resoureClass.getConstructor().newInstance();
					inst.setId(id);
					return inst;
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		});
	}

	@Test
	public void test() throws Exception {
		assertEquals("[license1, license2]", service.getLicenses().toString());

		assertEquals("{licenses=1}", concreteMetaRepoCallCounts.toString());

		service.getLicenses();
		service.getLicenses();
		service.getLicenses();
		assertEquals("{licenses=1}", concreteMetaRepoCallCounts.toString());

		assertEquals("[stage1, stage2]", service.getLifeStages().toString());

		assertEquals("[plstage1, plstage2]", service.getPlantLifeStages().toString());

		assertEquals("[sex1, sex2]", service.getSexes().toString());

		assertEquals("[side1, side2]", service.getSides().toString());

		assertEquals("[type1, type2]", service.getTypes().toString());

		assertEquals("" +
				"{licenses=1, lifeStages=1, plantLifeStages=1, sexes=1, sides=1, types=1}",
				concreteMetaRepoCallCounts.toString());
	}

}
