package fi.luomus.kuvapalvelu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.model.TransformedMedia.UploadFile;
import fi.luomus.kuvapalvelu.repository.TempFileRepositoryMock;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.preferences.MapBackedLuomusPreferences;

public class ImageTransformerTest {

	private ImageTransformer imageTransformer;
	private TransformedMedia transformedMedia;

	@Before
	public void setup() {
		imageTransformer = new ImageTransformer(new MapBackedLuomusPreferences(
				ImmutableMap.<String, Object>builder().put(MediaApiPreferences.IMAGE_QUALITY.key, "75").build()),
				new TempFileRepositoryMock());
		transformedMedia = null;
	}

	@After
	public void after() throws Exception {
		if (transformedMedia != null) {
			transformedMedia.close();
		}
	}

	@Test
	public void testNonImage() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("giant.tiff").toURI());
		File notImage = new File(this.getClass().getClassLoader().getResource("logback-test.xml").toURI());
		assertTrue(imageTransformer.check(image).isTransformable());
		assertFalse(imageTransformer.check(notImage).isTransformable());
		assertEquals("Invalid image: unable to retrieve image type", imageTransformer.check(notImage).getError());
	}

	@Test
	public void testGiantTiff() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("giant.tiff").toURI());
		transformedMedia = imageTransformer.transform(image, image.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();
		assertJpegWithSize(transformed.get(MediaFileType.FULL_IMAGE), 3360, 2520);
		assertJpegWithSize(transformed.get(MediaFileType.LARGE_IMAGE), 1280, 960);
		assertJpegWithSize(transformed.get(MediaFileType.THUMBNAIL_IMAGE), 200, 150);
		assertJpegWithSize(transformed.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE), 400, 400);
		assertEquals("giant.tiff", transformed.get(MediaFileType.ORIGINAL_IMAGE).getName());
	}

	@Test
	public void testRotatedGiantTiff() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("giant_rotated.tiff").toURI());
		transformedMedia = imageTransformer.transform(image, image.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();
		assertJpegWithSize(transformed.get(MediaFileType.FULL_IMAGE), 2520, 3360);
		assertJpegWithSize(transformed.get(MediaFileType.LARGE_IMAGE), 960, 1280);
		assertJpegWithSize(transformed.get(MediaFileType.THUMBNAIL_IMAGE), 150, 200);
		assertJpegWithSize(transformed.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE), 400, 400);
	}

	@Test
	public void testTinyTiff() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("tiny.tiff").toURI());
		transformedMedia = imageTransformer.transform(image, image.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();
		assertJpegWithSize(transformed.get(MediaFileType.FULL_IMAGE), 100, 75);
		assertJpegWithSize(transformed.get(MediaFileType.LARGE_IMAGE), 100, 75);
		assertJpegWithSize(transformed.get(MediaFileType.THUMBNAIL_IMAGE), 100, 75);
		assertJpegWithSize(transformed.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE), 75, 75);
	}

	@Test
	@Ignore
	public void test_HEIC_HEIF() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("sample1.heic").toURI());
		assertTrue(imageTransformer.check(image).isTransformable());
		transformedMedia = imageTransformer.transform(image, image.getName());
		assertFalse(transformedMedia.getTransformed().isEmpty());
	}

	@Test
	public void compressingGiantTiff() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("giant_35mb.tiff").toURI());
		assertEquals(37501633, image.length());
		transformedMedia = imageTransformer.transform(image, image.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();
		File compressed = transformed.get(MediaFileType.ORIGINAL_IMAGE);
		assertEquals("giant_35mb.tiff", compressed.getName());
		assertEquals(12500436, compressed.length());
	}

	@Test
	public void noUnnecessaryTransformations() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("bird.jpeg").toURI());
		transformedMedia = imageTransformer.transform(image, image.getName());
		Map<MediaFileType, File> transformed = transformedMedia.getFilesByType();

		File original = transformed.get(MediaFileType.ORIGINAL_IMAGE);
		File full =  transformed.get(MediaFileType.FULL_IMAGE);
		File large = transformed.get(MediaFileType.LARGE_IMAGE);
		File square = transformed.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE);
		File thumb = transformed.get(MediaFileType.THUMBNAIL_IMAGE);

		assertTrue("original and full are same", full.equals(original));
		assertTrue("full and large are same", full.equals(large));
		assertTrue("full and thumb are same", full.equals(thumb));

		assertFalse("square is squared", full.equals(square));
		assertEquals("bird_square.jpg", square.getName());

		transformedMedia.getTransformed().forEach(uf->assertTrue(uf.getMediaType() + " exists", uf.getFile().exists()));
	}

	@Test
	public void testCloseable() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("tiny.tiff").toURI());
		TransformedMedia transformedImages = imageTransformer.transform(image, image.getName());

		Collection<UploadFile> images = transformedImages.getTransformed();

		for (UploadFile i : images) {
			assertTrue(i.getFile().exists());
		}
		assertTrue(image.exists());

		transformedImages.close();

		for (UploadFile i : images) {
			assertFalse(i.getFile().exists());
		}
		assertTrue(image.exists());
	}

	@Test
	public void testRename() throws Exception {
		File image = new File(this.getClass().getClassLoader().getResource("giant.tiff").toURI());
		transformedMedia= imageTransformer.transform(image, "big.tiff");
		assertEquals("big.tiff", transformedMedia.getFilesByType().get(MediaFileType.ORIGINAL_IMAGE).getName());

		for (File file : transformedMedia.getFilesByType().values()) {
			assertTrue(file.getName().contains("big"));
			assertFalse(file.getName().contains("giant"));
		}
	}

	public static void assertJpeg(File file) throws IOException {
		assertEquals("JPEG", getImageFormat(file));
	}

	public static void assertPng(File file) throws IOException {
		assertEquals("png", getImageFormat(file));
	}

	public static void assertJpegWithSize(File file, int width, int height) throws IOException {
		assertEquals("JPEG", getImageFormat(file));
		BufferedImage read = ImageIO.read(file);
		assertEquals(width, read.getWidth());
		assertEquals(height, read.getHeight());
	}

	public static void assertPngWithSize(File file, int width, int height) throws IOException {
		assertEquals("png", getImageFormat(file));
		BufferedImage read = ImageIO.read(file);
		assertEquals(width, read.getWidth());
		assertEquals(height, read.getHeight());
	}

	public static String getImageFormat(File file) throws IOException {
		Iterator<ImageReader> imageReaders;
		try {
			imageReaders = ImageIO.getImageReaders(ImageIO.createImageInputStream(file));
		} catch (Exception e) {
			throw new IOException("Can not read file " + file.getAbsolutePath());
		}
		return imageReaders.hasNext() ? imageReaders.next().getFormatName() : null;
	}

}
