package fi.luomus.kuvapalvelu.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;

import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.repository.TempFileRepositoryImpl;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.MapBackedLuomusPreferences;

public class TempFileStorageImplTest {

	private final byte[] birdJpeg;
	private TempFileRepositoryImpl tempFileStorage;
	private Runnable scheduled;

	public TempFileStorageImplTest() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		birdJpeg = IOUtils.toByteArray(inputStream);
	}

	@Before
	public void setUp() {
		scheduled = null;
		tempFileStorage = new TempFileRepositoryImpl(new TaskScheduler() {
			@Override
			public ScheduledFuture<?> schedule(Runnable task, Trigger trigger) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> schedule(Runnable task, Date startTime) {
				scheduled = task;
				return null;
			}

			@Override
			public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, Date startTime, long delay) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, long delay) {
				throw new RuntimeException();
			}
		}, new MapBackedLuomusPreferences());
	}

	@Test
	public void testSchedule() throws IOException {
		assertNull(scheduled);
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		assertNotNull(scheduled);
		scheduled.run();
		assertFalse(tempFileStorage.getHandle(tempFileInfo.getId()).isPresent());
	}

	@Test
	public void testStoreAndGet() throws IOException {
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		assertNotNull(tempFileInfo.getExpires());
		assertNotNull(tempFileInfo.getId());
		assertTrue(tempFileInfo.getExpires().isAfterNow());

		Optional<File> handle = tempFileStorage.getHandle(tempFileInfo.getId());
		assertTrue(Arrays.equals(birdJpeg, IOUtils.toByteArray(handle.get().toURI())));
	}

	@Test
	public void testDelete() throws IOException, NotFoundException {
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");

		Optional<File> handle = tempFileStorage.getHandle(tempFileInfo.getId());
		assertTrue(handle.get().exists());
		tempFileStorage.delete(tempFileInfo.getId());
		assertFalse(tempFileStorage.getHandle(tempFileInfo.getId()).isPresent());
		try {
			tempFileStorage.delete(tempFileInfo.getId());
			fail("exception not raised");
		} catch (NotFoundException e) {
			//nop
		}
	}

	@Test
	public void testDestroyDefault() throws IOException {
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		File handle = tempFileStorage.getHandle(tempFileInfo.getId()).get();
		tempFileStorage.destroy();
		assertFalse(handle.exists());
	}

	@Test
	public void testDestroySpecified() throws IOException {
		File tempDir = Files.createTempDirectory("temp-fite-storage-test").toFile();
		tempFileStorage = new TempFileRepositoryImpl(new TaskScheduler() {
			@Override
			public ScheduledFuture<?> schedule(Runnable task, Trigger trigger) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> schedule(Runnable task, Date startTime) {
				scheduled = task;
				return null;
			}

			@Override
			public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, Date startTime, long period) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleAtFixedRate(Runnable task, long period) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, Date startTime, long delay) {
				throw new RuntimeException();
			}

			@Override
			public ScheduledFuture<?> scheduleWithFixedDelay(Runnable task, long delay) {
				throw new RuntimeException();
			}
		}, new MapBackedLuomusPreferences(ImmutableMap.<String, Object>builder().put(MediaApiPreferences.TEMPORARY_FILE_LOCATION.key, tempDir.getAbsolutePath()).build()));
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		File handle = tempFileStorage.getHandle(tempFileInfo.getId()).get();
		tempFileStorage.destroy();
		assertFalse(handle.exists());
		assertTrue(tempDir.exists());
	}

	@Test
	public void testGetNotExists() throws IOException {
		assertFalse(tempFileStorage.getHandle("random").isPresent());
	}

	@Test
	public void testDeleteNotExists() throws IOException {
		try {
			tempFileStorage.delete("random");
			fail("notfoundexception not raised");
		} catch (NotFoundException e) {
			// nop
		}
	}

	@Test
	public void testMultiple() throws IOException {
		InputStream testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		TempFileRepository.TempFileInfo tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		String firstId = tempFileInfo.getId();

		testPicture = this.getClass().getClassLoader().getResource("bird.jpeg").openStream();
		tempFileInfo = tempFileStorage.storeTempFile(testPicture, "filename.jpg");
		assertFalse(firstId.equals(tempFileInfo.getId()));
	}

}