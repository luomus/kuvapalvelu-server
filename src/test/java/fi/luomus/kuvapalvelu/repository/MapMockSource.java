package fi.luomus.kuvapalvelu.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import com.google.common.base.Optional;

import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.model.TaxonSearchResult;
import fi.luomus.triplestore.client.util.PredicateUpdateOperation;
import fi.luomus.utils.exceptions.ApiException;

public class MapMockSource implements TriplestoreClient {

	private int counter = 0;
	private Map<String, Model> map = new HashMap<>();

	public MapMockSource() {}

	@Override
	public SearchQuery searchResource() {
		throw new RuntimeException();
	}

	@Override
	public SearchQuery searchLiteral() {
		throw new RuntimeException();
	}

	@Override
	public String getSequence(String namespace) throws ApiException {
		return namespace + "." + counter++;
	}

	@Override
	public Optional<Model> getResource(String resourceName) throws ApiException {
		return Optional.fromNullable(map.get(resourceName));
	}

	@Override
	public Map<String, Resource> getResources(String... strings) throws ApiException {
		return null;
	}

	@Override
	public Map<String, Resource> getResources(List<String> list) throws ApiException {
		return null;
	}

	@Override
	public Optional<Model> getResource(String resourceName, Map<String, String> params) throws ApiException {
		return getResource(resourceName);
	}

	@Override
	public TaxonSearchResult taxonSearch(String s) throws ApiException {
		return null;
	}

	@Override
	public TaxonSearchResult taxonSearch(String s, String s1) throws ApiException {
		return null;
	}

	@Override
	public void updatePredicate(PredicateUpdateOperation predicateUpdateOperation) throws ApiException {

	}


	@Override
	public void upsertResource(String resourceName, Model model) throws ApiException {
		map.put(resourceName, model);
	}

	@Override
	public void delete(String id) throws ApiException {
		map.remove(id);
	}

}