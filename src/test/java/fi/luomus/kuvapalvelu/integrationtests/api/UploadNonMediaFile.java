package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.fail;

import java.util.Arrays;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.client.MediaFormatNotSupported;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class UploadNonMediaFile extends BaseApiIntegrationTest {

	private Meta meta;

	@Before
	public void createMeta() {
		this.meta = new Meta();
		meta.getIdentifications().getTaxonIds().add("MX.65532");
		meta.getIdentifications().getVerbatim().add("elukka");
		meta.setSecret(false);
		meta.setCaptureDateTime(DateTime.now());
		meta.getCapturers().add("capturer");
		meta.setDocumentIds(Arrays.asList("http://id.luomus.fi/foo/blah"));
		meta.setOriginalFilename("foobar.jpg");
		meta.setRightsOwner("owner");
		meta.getTags().add("aTag");
		meta.setLicense("MZ.intellectualRightsARR");
	}

	@Test
	public void tryUploadNonImageFile() throws Exception {
		try {
			uploadFileAndMeta(MediaClass.IMAGE, "logback-test.xml", meta);
			fail("exception not raised");
		} catch (MediaFormatNotSupported e) {
			//nop
		}
	}

	@Test
	public void tryUploadNonAudioile() throws Exception {
		try {
			uploadFileAndMeta(MediaClass.AUDIO, "logback-test.xml", meta);
			fail("exception not raised");
		} catch (MediaFormatNotSupported e) {
			//nop
		}
	}

}