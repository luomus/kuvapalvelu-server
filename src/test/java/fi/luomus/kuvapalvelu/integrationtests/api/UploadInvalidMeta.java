package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URISyntaxException;

import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.utils.exceptions.ApiException;

public class UploadInvalidMeta extends BaseApiIntegrationTest {

	@Test
	public void uploadInvalidMeta() throws URISyntaxException {
		File file = getFile("bird.jpeg");
		Meta meta = meta();
		meta.setSide("foobar");
		try {
			this.mediaApiClient.uploadImage(file, meta);
		} catch (ApiException e) {
			assertEquals("400", e.getCode());
			assertEquals("{\"code\":\"INVALID_PARAMETERS\",\"message\":\"Unknown side: foobar\"}", e.getDetails());
		}
	}

}
