package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;

import fi.luomus.kuvapalvelu.client.MultiSourceMedia;
import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.utils.exceptions.ApiException;

public class UploadAndGetMultiFile extends BaseApiIntegrationTest {

	@Test
	public void uploadAndGetAudioAndImage() throws Exception {
		Meta meta = new Meta().setLicense("MZ.intellectualRightsARR").setRightsOwner("me");

		try (MultiSourceMedia sources = new MultiSourceMedia()) {
			sources
			.set(getFile("bat.wav"), MediaFileType.WAV)
			.set(getFile("bird.jpeg"), MediaFileType.THUMBNAIL_IMAGE);

			String id = mediaApiClient.upload(MediaClass.AUDIO, sources, meta);
			this.cleanUpMediaIds.add(id);

			Media media = mediaApiClient.get(MediaClass.AUDIO, id).get();

			assertNotNull(media.getId());
			assertEquals("bat.wav", media.getMeta().getOriginalFilename());

			assertEquals("bat.mp3", extractFilename(media.getUrls().getMp3()));
			assertEquals("bat_spectrogram.png", extractFilename(media.getUrls().getFull()));

			assertEquals("bird.jpeg", extractFilename(media.getUrls().getThumbnail()));
			assertEquals("bird_square.jpg", extractFilename(media.getUrls().getSquare()));
		}
	}

	@Test
	public void uploadVideo() throws Exception {
		testVideo(MediaClass.VIDEO);
	}

	private void testVideo(MediaClass mediaClass) throws URISyntaxException, ApiException {
		Meta meta = new Meta().setLicense("MZ.intellectualRightsARR").setRightsOwner("me");

		try (MultiSourceMedia sources = new MultiSourceMedia()) {
			sources
			.set(getFile("Capreolus_capreolus_xray.mp4"), MediaFileType.VIDEO)
			.set(getFile("Capreolus_capreolus_xray_thumbnail.jpg"), MediaFileType.THUMBNAIL_IMAGE);

			String id = mediaApiClient.upload(mediaClass, sources, meta);
			this.cleanUpMediaIds.add(id);

			Media media = mediaApiClient.get(mediaClass, id).get();

			assertNotNull(media.getId());
			assertEquals("Capreolus_capreolus_xray.mp4", media.getMeta().getOriginalFilename());

			assertEquals("Capreolus_capreolus_xray.mp4", extractFilename(media.getUrls().getVideo()));

			assertEquals("Capreolus_capreolus_xray_thumbnail.jpg", extractFilename(media.getUrls().getFull()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_thumb.jpg", extractFilename(media.getUrls().getThumbnail()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_square.jpg", extractFilename(media.getUrls().getSquare()));
		}
	}

	@Test
	public void uploadLow3dModel_stl() throws Exception {
		Meta meta = new Meta().setLicense("MZ.intellectualRightsARR").setRightsOwner("me");

		try (MultiSourceMedia sources = new MultiSourceMedia()) {
			sources
			.set(getFile("Pernis_apivorus_EHB_mesh.stl"), MediaFileType.LOW_DETAIL_MODEL)
			.set(getFile("Capreolus_capreolus_xray.mp4"), MediaFileType.VIDEO)
			.set(getFile("Capreolus_capreolus_xray_thumbnail.jpg"), MediaFileType.THUMBNAIL_IMAGE);

			String id = mediaApiClient.upload(MediaClass.MODEL, sources, meta);
			this.cleanUpMediaIds.add(id);

			Media media = mediaApiClient.get(MediaClass.MODEL, id).get();

			assertNotNull(media.getId());

			assertEquals("Pernis_apivorus_EHB_mesh.stl", extractFilename(media.getUrls().getLowDetailModel()));
			assertNull(media.getUrls().getHighDetailModel());
			assertEquals("Capreolus_capreolus_xray.mp4", extractFilename(media.getUrls().getVideo()));

			assertEquals("Capreolus_capreolus_xray_thumbnail.jpg", extractFilename(media.getUrls().getFull()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_thumb.jpg", extractFilename(media.getUrls().getThumbnail()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_square.jpg", extractFilename(media.getUrls().getSquare()));

			assertEquals("Pernis_apivorus_EHB_mesh.stl", media.getMeta().getOriginalFilename());
		}
	}

	@Test
	public void uploadLow3dModel_glb() throws Exception {
		Meta meta = new Meta().setLicense("MZ.intellectualRightsARR").setRightsOwner("me");

		try (MultiSourceMedia sources = new MultiSourceMedia()) {
			sources
			.set(getFile("Pernis_apivorus.glb"), MediaFileType.LOW_DETAIL_MODEL)
			.set(getFile("Capreolus_capreolus_xray.mp4"), MediaFileType.VIDEO)
			.set(getFile("Capreolus_capreolus_xray_thumbnail.jpg"), MediaFileType.THUMBNAIL_IMAGE);

			String id = mediaApiClient.upload(MediaClass.MODEL, sources, meta);
			this.cleanUpMediaIds.add(id);

			Media media = mediaApiClient.get(MediaClass.MODEL, id).get();

			assertNotNull(media.getId());

			assertEquals("Pernis_apivorus.glb", extractFilename(media.getUrls().getLowDetailModel()));
			assertNull(media.getUrls().getHighDetailModel());
			assertEquals("Capreolus_capreolus_xray.mp4", extractFilename(media.getUrls().getVideo()));

			assertEquals("Capreolus_capreolus_xray_thumbnail.jpg", extractFilename(media.getUrls().getFull()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_thumb.jpg", extractFilename(media.getUrls().getThumbnail()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_square.jpg", extractFilename(media.getUrls().getSquare()));

			assertEquals("Pernis_apivorus.glb", media.getMeta().getOriginalFilename());
		}
	}

	@Test
	public void uploadHigh3dModel() throws Exception {
		Meta meta = new Meta().setLicense("MZ.intellectualRightsARR").setRightsOwner("me");

		try (MultiSourceMedia sources = new MultiSourceMedia()) {
			sources
			.set(getFile("Pernis_apivorus_EHB_mesh.stl"), MediaFileType.HIGH_DETAIL_MODEL)
			.set(getFile("Capreolus_capreolus_xray.mp4"), MediaFileType.VIDEO)
			.set(getFile("Capreolus_capreolus_xray_thumbnail.jpg"), MediaFileType.THUMBNAIL_IMAGE);

			String id = mediaApiClient.upload(MediaClass.MODEL, sources, meta);
			this.cleanUpMediaIds.add(id);

			Media media = mediaApiClient.get(MediaClass.MODEL, id).get();

			assertNotNull(media.getId());

			assertEquals("Pernis_apivorus_EHB_mesh.stl", extractFilename(media.getUrls().getHighDetailModel()));
			assertNull(media.getUrls().getLowDetailModel());
			assertEquals("Capreolus_capreolus_xray.mp4", extractFilename(media.getUrls().getVideo()));

			assertEquals("Capreolus_capreolus_xray_thumbnail.jpg", extractFilename(media.getUrls().getFull()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_thumb.jpg", extractFilename(media.getUrls().getThumbnail()));
			assertEquals("Capreolus_capreolus_xray_thumbnail_square.jpg", extractFilename(media.getUrls().getSquare()));

			assertEquals("Pernis_apivorus_EHB_mesh.stl", media.getMeta().getOriginalFilename());
		}
	}

	public static String extractFilename(URI uri) {
		String path = uri.getPath();
		return new java.io.File(path).getName();
	}

}