package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class UploadAndGet extends BaseApiIntegrationTest {

	private Meta meta;
	private Media media;

	@Before
	public void upload() {
		this.meta = new Meta();
		meta.getIdentifications().getTaxonIds().add("MX.65532");
		meta.getIdentifications().getVerbatim().add("elukka");
		meta.setSecret(false);
		meta.setCaptureDateTime(DateTime.now());
		meta.getCapturers().add("capturer");
		meta.setDocumentIds(Arrays.asList("http://id.luomus.fi/foo/blah"));
		meta.setLicense("MZ.intellectualRightsARR");
		meta.setSourceSystem("foo blah"); // should be ignored
		meta.setRightsOwner("öwner");
		meta.getTags().add("aTag");
		meta.addSex("MY.sexF");
		meta.setType("MM.typeEnumLive");
		meta.addPrimaryForTaxon("MX.65532");
		meta.addLifeStage("MY.lifeStagePupa");
		meta.addPlantLifeStage("MY.plantLifeStageSeed");
		meta.setSortOrder(1);
	}

	@Test
	public void uploadAndGetImage() throws Exception {
		testUploadAndGet("bird.jpeg", MediaClass.IMAGE);
	}

	@Test
	public void uploadAndGetImage_broken_icc_profile() throws Exception {
		testUploadAndGet("broken_icc_profile.png", MediaClass.IMAGE);
	}

	@Test
	public void uploadAndGetWav() throws Exception {
		testUploadAndGet("bat.wav", MediaClass.AUDIO);
	}

	@Test
	public void uploadAndGetMp3() throws Exception {
		testUploadAndGet("bird.mp3", MediaClass.AUDIO);
	}

	public void testUploadAndGet(String filename, MediaClass mediaClass) throws Exception {
		this.media = uploadFileAndMeta(mediaClass, filename, meta);
		assertNotNull(media.getId());
		// make sure the time gets stored right
		assertTrue(Math.abs(meta.getCaptureDateTime().getMillis() -
				media.getMeta().getCaptureDateTime().getMillis()) < 1000);

		// make the metadata match the expected results
		meta.setOriginalFilename(filename);
		meta.setSourceSystem("KE.21");
		meta.setUploadDateTime(media.getMeta().getUploadDateTime());
		meta.setCaptureDateTime(media.getMeta().getCaptureDateTime());
		assertEquals(meta, media.getMeta());
		assertNull(media.getSecretKey());
	}

	@Test
	public void uploadAdGetPdf() throws Exception {
		this.meta = new Meta();
		meta.setSecret(false);
		meta.setCaptureDateTime(DateTime.now());
		meta.setDocumentIds(Arrays.asList("http://id.luomus.fi/foo/blah"));
		meta.setLicense("MZ.intellectualRightsARR");
		meta.setSourceSystem("foo blah"); // should be ignored
		meta.setRightsOwner("öwner");
		meta.getTags().add("aTag");

		this.media = uploadFileAndMeta(MediaClass.PDF, "test.pdf", meta);
		assertNotNull(media.getId());
		// make sure the time gets stored right
		assertTrue(Math.abs(meta.getCaptureDateTime().getMillis() -
				media.getMeta().getCaptureDateTime().getMillis()) < 1000);

		// make the metadata match the expected results
		meta.setOriginalFilename("test.pdf");
		meta.setSourceSystem("KE.21");
		meta.setUploadDateTime(media.getMeta().getUploadDateTime());
		meta.setCaptureDateTime(media.getMeta().getCaptureDateTime());
		assertEquals(meta, media.getMeta());
		assertNull(media.getSecretKey());
	}

}