package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class Update extends BaseApiIntegrationTest {
	private Media media;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.setDocumentIds(Arrays.asList("original"));

		this.media = uploadFileAndMeta("tiny.tiff", meta);
	}

	@Test
	public void testUpdate() throws Exception {
		assertEquals(Arrays.asList("original"), media.getMeta().getDocumentIds());
		Meta meta = media.getMeta();
		meta.setDocumentIds(Collections.singletonList("new"));
		this.mediaApiClient.update(MediaClass.IMAGE, media.getId(), meta);
		Media gotMedia = this.mediaApiClient.get(MediaClass.IMAGE, media.getId()).get();
		//make modifiedDate match
		media.getMeta().setModifiedDateTime(gotMedia.getMeta().getModifiedDateTime());

		assertEquals(media, gotMedia);

		assertEquals(Arrays.asList("new"), gotMedia.getMeta().getDocumentIds());
		assertNotNull(gotMedia.getMeta().getModifiedDateTime());
	}

}