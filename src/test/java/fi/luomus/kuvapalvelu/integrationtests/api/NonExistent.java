package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;

import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

public class NonExistent extends BaseApiIntegrationTest {
	private final String nonexistentId = "MX.DOESNOTEXIST";

	@Test
	public void nonExistentGet() throws ApiException {
		assertFalse(this.mediaApiClient.get(MediaClass.IMAGE, nonexistentId).isPresent());
	}

	@Test
	public void updateNonExistent() throws ApiException {
		try {
			this.mediaApiClient.update(MediaClass.IMAGE, nonexistentId, meta());
			fail("exception not raised");
		} catch (NotFoundException e) {
		}
	}

	@Test
	public void deleteNonExistent() throws ApiException {
		try {
			this.mediaApiClient.delete(MediaClass.IMAGE, nonexistentId);
			fail("exception not raised");
		} catch (NotFoundException e) {
		}
	}
}
