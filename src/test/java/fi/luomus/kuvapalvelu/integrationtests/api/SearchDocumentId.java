package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class SearchDocumentId extends BaseApiIntegrationTest {

	private String expectedDocumentId;
	private List<Media> getImagesResult;

	@Before
	public void upload() throws Exception {
		this.expectedDocumentId = UUID.randomUUID().toString();
		Meta meta = meta();
		meta.setDocumentIds(Arrays.asList(this.expectedDocumentId));
		uploadFileAndMeta("bird.jpeg", meta);
		this.getImagesResult = this.mediaApiClient.search(MediaClass.IMAGE, "documentIds", expectedDocumentId);
	}

	@Test
	public void searchDocumentId() {
		assertTrue(getImagesResult.size() == 1);
		assertEquals(Arrays.asList(expectedDocumentId), getImagesResult.get(0).getMeta().getDocumentIds());
	}

}
