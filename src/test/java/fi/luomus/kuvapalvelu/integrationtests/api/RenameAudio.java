package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class RenameAudio extends BaseApiIntegrationTest {

	private final String SPECIFIED_FILE_NAME = "renamed_bat.wav";
	private Media gotMedia;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.setOriginalFilename(SPECIFIED_FILE_NAME);
		this.gotMedia = uploadFileAndMeta(MediaClass.AUDIO, "bat.wav", meta);
	}

	@Test
	public void testRename() {
		assertEquals(SPECIFIED_FILE_NAME, gotMedia.getMeta().getOriginalFilename());
		// make sure the uris use the new name as well
		String[] split = gotMedia.getUrls().getWav().getPath().split("/");
		String fileName = split[split.length - 1];
		assertEquals(SPECIFIED_FILE_NAME, fileName);
	}

}
