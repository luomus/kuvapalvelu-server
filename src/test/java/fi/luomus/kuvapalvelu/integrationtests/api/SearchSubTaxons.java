package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class SearchSubTaxons extends BaseApiIntegrationTest {

	private final String UPPER_TAXON = "MX.37927";
	private final String FIRST_SUBTAXON = "MX.37928";
	private final String SECON_SUBTAXON = "MX.37929";
	int totalOfAllTaxons;

	@Before
	public void ensureTaxonImagesPresent() throws Exception {
		this.totalOfAllTaxons = getAmountAndUploadIfNecessary(UPPER_TAXON) +
				getAmountAndUploadIfNecessary(FIRST_SUBTAXON) +
				getAmountAndUploadIfNecessary(SECON_SUBTAXON);

	}

	private int getAmountAndUploadIfNecessary(String taxonId) throws Exception {
		int amount = this.mediaApiClient.search(MediaClass.IMAGE, "identifications.taxonIds", taxonId).size();
		if (amount == 0) {
			Meta meta = meta();
			meta.getIdentifications().getTaxonIds().add(taxonId);
			uploadFileAndMeta("tiny.tiff", meta);
			return getAmountAndUploadIfNecessary(taxonId);
		}
		return amount;
	}

	@Test
	public void testSearchSubtaxons() throws Exception {
		assertTrue(totalOfAllTaxons >= 3);
		assertEquals(totalOfAllTaxons, this.mediaApiClient.searchSubtaxonMedia(MediaClass.IMAGE, UPPER_TAXON).size());
	}

}
