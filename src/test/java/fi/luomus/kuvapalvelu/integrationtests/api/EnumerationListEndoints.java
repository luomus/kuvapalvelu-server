package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.NamedResource;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.utils.exceptions.ApiException;

public class EnumerationListEndoints extends BaseApiIntegrationTest {

	@Test
	public void licenses() throws ApiException {
		List<License> licenses = mediaApiClient.getLicenses();
		assertEquals(License.class, licenses.get(0).getClass());
		assertHas("MZ.intellectualRightsCC-BY-4.0", "Creative Commons Attribution 4.0", "Creative Commons Nimeä 4.0", "Creative Commons erkännande 4.0", licenses);
		assertTrue("There are at least 20 licenses but there are " + licenses.size(), licenses.size() >= 20);
	}

	@Test
	public void types() throws ApiException {
		List<Type> types = mediaApiClient.getTypes();
		assertEquals(Type.class, types.get(0).getClass());
		assertHas("MM.typeEnumLive", "Live", "Luonnossa otettu", "Levande", types);
		assertHas("MM.typeEnumSpecimen", "Specimen", "Näyte", "Prov", types);
		assertHas("MM.typeEnumGenitalia", "Genitalia", "Genitaali", "Genitalier", types);
		assertHas("MM.typeEnumMicroscopy", "Microscopy", "Mikroskooppi", "Mikroskopi", types);
		assertEquals(4, types.size());
	}

	@Test
	public void sexes() throws ApiException {
		List<Sex> sexes = mediaApiClient.getSexes();
		assertEquals(Sex.class, sexes.get(0).getClass());
		assertHas("MY.sexW", "W - Worker", "työläinen", "arbetare", sexes);
		assertTrue("There are at least 8 sexes but there are " + sexes.size(), sexes.size() >= 8);
	}

	@Test
	public void lifeStages() throws ApiException {
		List<LifeStage> lifeStages = mediaApiClient.getLifeStages();
		assertEquals(LifeStage.class, lifeStages.get(0).getClass());
		assertHas("MY.lifeStageTadpole", "tadpole", "nuijapää", "grodyngel", lifeStages);
		assertTrue("There are at least 18 lifeStages but there are " + lifeStages.size(), lifeStages.size() >= 18);
	}

	@Test
	public void plantLifeStages() throws ApiException {
		List<LifeStage> plantLifeStages = mediaApiClient.getPlantLifeStages();
		assertEquals(LifeStage.class, plantLifeStages.get(0).getClass());
		assertHas("MY.plantLifeStageSterile", "sterile", "steriili", "steril", plantLifeStages);
		assertTrue("There are at least 10 plant lifeStages byt there are " + plantLifeStages.size(), plantLifeStages.size() > 10);
	}

	private void assertHas(String id, String en, String fi, String sv, List<? extends NamedResource> resources) {
		for (NamedResource r : resources) {
			if (id.equals(r.getId())) {
				assertEquals("Label en for " + id, en, r.getLabel().get("en"));
				assertEquals("Label fi for " + id, fi, r.getLabel().get("fi"));
				assertEquals("Label sv for " + id, sv, r.getLabel().get("sv"));
				return;
			}
		}
		fail("Expected enumeration with id " + id + " was not found");
	}

}
