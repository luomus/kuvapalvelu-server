package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class EmptyDocumentIds extends BaseApiIntegrationTest {

	private Meta meta;
	private Media media;

	@Before
	public void upload() throws Exception {
		this.meta = new Meta();
		meta.getIdentifications().getTaxonIds().add("MX.65532");
		meta.getIdentifications().getVerbatim().add("elukka");
		meta.setSecret(false);
		meta.setCaptureDateTime(DateTime.now());
		meta.addCapturer("capturer");
		meta.setDocumentIds(Collections.<String>emptyList());
		meta.setLicense("MZ.intellectualRightsARR");
		meta.setRightsOwner("öwner");
		meta.addTag("aTag");
		this.media = uploadFileAndMeta("bird.jpeg", meta);
	}

	@Test
	public void testIsEmpty() throws Exception {
		assertTrue(this.mediaApiClient.get(MediaClass.IMAGE, this.media.getId()).get().getMeta().getDocumentIds().isEmpty());
	}
}
