package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.NewMedia;

public class UploadDoesNotExist extends BaseApiIntegrationTest {

	private Response response;

	@Before
	public void tryUploadNonExisting() {
		NewMedia newMedia = new NewMedia();
		newMedia.setTempFileId("doesnotexist");
		newMedia.setMeta(meta());
		this.response = this.target.path("api").path("images").request().post(Entity.json(Arrays.asList(newMedia)));
	}

	@Test
	public void uploadNotExist() {
		assertEquals(404, response.getStatus());
		Map<?, ?> map = response.readEntity(Map.class);
		assertTrue(map.containsKey("code"));
		assertEquals("TEMP_FILE_NOT_FOUND", map.get("code"));
		assertTrue(map.containsKey("missingTempFiles"));
		assertEquals(map.get("missingTempFiles"), Arrays.asList("doesnotexist"));
	}

}