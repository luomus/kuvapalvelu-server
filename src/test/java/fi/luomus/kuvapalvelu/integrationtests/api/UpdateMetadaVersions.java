package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class UpdateMetadaVersions extends BaseApiIntegrationTest {
	private Media media;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();

		// v2 fields
		meta.setType("MM.typeEnumMicroscopy");
		meta.getTaxonDescriptionCaption().put("fi", "v2 tax desc caption");
		meta.addLifeStage("MY.lifeStageLarva");
		meta.setSortOrder(5);

		// some old fields
		meta.setCaption("v1 caption");
		meta.addTag("aTag");


		this.media = uploadFileAndMeta("tiny.tiff", meta);
	}

	@Test
	public void testUpdate_v2_meta() throws Exception {
		Meta meta = media.getMeta();

		meta.setType(null);
		meta.setTaxonDescriptionCaption(null);
		meta.getLifeStage().clear();
		meta.setSortOrder(null);
		meta.setCaption(null);
		meta.getTags().clear();

		this.mediaApiClient.update(MediaClass.IMAGE, media.getId(), meta);
		assertEquals(Meta.LATEST_META_VERSION, meta.getMetadataVersion()); // the client sets meta to latest version

		Media gotMedia = this.mediaApiClient.get(MediaClass.IMAGE, media.getId()).get();

		// Because we updated v2 meta, all fields that were set empty/null are cleared
		assertEquals(null, gotMedia.getMeta().getType());
		assertEquals(0, gotMedia.getMeta().getTaxonDescriptionCaption().size());
		assertEquals(0, gotMedia.getMeta().getLifeStage().size());
		assertEquals(null, gotMedia.getMeta().getSortOrder());
		assertEquals(null, gotMedia.getMeta().getCaption());
		assertEquals(0, gotMedia.getMeta().getTags().size());
	}

	@Test
	public void testUpdate_v1_meta() throws Exception {
		Meta meta = media.getMeta();
		meta.setMetadataVersion("foobar"); // simulate system that does not set metadata version to be v2 and does not know about the new fields

		meta.setType(null);
		meta.setTaxonDescriptionCaption(null);
		meta.getLifeStage().clear();
		meta.setSortOrder(null);
		meta.setCaption(null);
		meta.getTags().clear();

		this.mediaApiClient.update(MediaClass.IMAGE, media.getId(), meta);
		assertEquals("foobar", meta.getMetadataVersion()); // the client doesn't override version to latest (mostly for being able to implement these tests)

		Media gotMedia = this.mediaApiClient.get(MediaClass.IMAGE, media.getId()).get();

		// Because we updated meta from legacy system, values of v2 fields that were set empty/null are kept
		// v2 fields
		assertEquals("MM.typeEnumMicroscopy", gotMedia.getMeta().getType());
		assertEquals(1, gotMedia.getMeta().getTaxonDescriptionCaption().size());
		assertEquals("[MY.lifeStageLarva]", gotMedia.getMeta().getLifeStage().toString());
		assertEquals(5, gotMedia.getMeta().getSortOrder().intValue());

		// some old fields; these were cleared
		assertEquals(null, gotMedia.getMeta().getCaption());
		assertEquals(0, gotMedia.getMeta().getTags().size());
	}

	@Test
	public void testUpdate_v1_meta_with_changed_values() throws Exception {
		Meta meta = media.getMeta();
		meta.setMetadataVersion("foobar"); // simulate system that does not set metadata version to be v2 and does not know about the new fields

		meta.setType("MM.typeEnumLive");
		meta.addLifeStage("MY.lifeStageEgg");
		meta.setSortOrder(999);

		this.mediaApiClient.update(MediaClass.IMAGE, media.getId(), meta);
		assertEquals("foobar", meta.getMetadataVersion()); // the client doesn't override version to latest (mostly for being able to implement these tests)

		Media gotMedia = this.mediaApiClient.get(MediaClass.IMAGE, media.getId()).get();

		// Even though meta version doesn't declare v2, if values of v2 field are given, they are used
		assertEquals("MM.typeEnumLive", gotMedia.getMeta().getType());
		assertEquals(2, gotMedia.getMeta().getLifeStage().size());
		assertEquals(999, gotMedia.getMeta().getSortOrder().intValue());
	}

}