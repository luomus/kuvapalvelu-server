package fi.luomus.kuvapalvelu.integrationtests.api;

import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class UploadAndDelete extends BaseApiIntegrationTest {

	@Test
	public void isDeleted() throws Exception {
		Meta meta = meta();
		meta.setOriginalFilename("bird.jpeg");
		Media media = uploadFileAndMeta(MediaClass.IMAGE, "bird.jpeg", meta);
		this.mediaApiClient.delete(MediaClass.IMAGE, media.getId());
	}

	@Test
	public void isDeletedAudio() throws Exception {
		Meta meta = meta();
		meta.setOriginalFilename("bat.wav");
		Media media = uploadFileAndMeta(MediaClass.AUDIO, "bat.wav", meta);
		this.mediaApiClient.delete(MediaClass.AUDIO, media.getId());
	}

}