package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class Limit extends BaseApiIntegrationTest {
	private String randomDocumentId;
	private Set<String> uploadedIds;

	@Before
	public void upload() throws Exception {
		this.randomDocumentId = UUID.randomUUID().toString();
		Meta meta = meta();
		meta.setDocumentIds(Arrays.asList(this.randomDocumentId));
		this.uploadedIds = new HashSet<>();
		uploadedIds.add(uploadFileAndMeta("tiny.tiff", meta).getId());
		uploadedIds.add(uploadFileAndMeta("tiny.tiff", meta).getId());
		uploadedIds.add(uploadFileAndMeta("tiny.tiff", meta).getId());
	}

	@Test
	public void testLimit() throws Exception {
		assertTrue(mediaApiClient.search(MediaClass.IMAGE, "documentIds", randomDocumentId).size() == 3);
		Set<String> gotIds = new HashSet<>();
		// get one-by-one
		for (int i = 0; i < uploadedIds.size(); i++) {
			List<Media> images = mediaApiClient.search(MediaClass.IMAGE, "documentIds", randomDocumentId, 1, i);
			assertEquals(1, images.size());
			gotIds.add(images.get(0).getId());
		}
		assertEquals(uploadedIds, gotIds);

		// offsets beyond the amount of results should result in an empty result set
		assertTrue(mediaApiClient.search(MediaClass.IMAGE, "documentIds", randomDocumentId, 1, 4).size() == 0);
	}
}
