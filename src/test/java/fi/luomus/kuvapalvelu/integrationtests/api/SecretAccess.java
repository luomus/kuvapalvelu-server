package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.Meta;

public class SecretAccess extends BaseApiIntegrationTest {

	private Media gotMedia;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.setSecret(true);
		this.gotMedia = uploadFileAndMeta("tiny.tiff", meta);
	}

	@Test
	public void secretAccess() {
		assertNotNull(gotMedia.getSecretKey());
		WebTarget webTarget = ClientBuilder.newClient().target(this.baseUri);
		assertEquals(401, webTarget.path("api").path("images").path(gotMedia.getId()).request().get().getStatus());

		target = ClientBuilder.newClient().register(HttpAuthenticationFeature.basic("KE.22", "password")).target(baseUri);
		assertEquals(403, target.path("api").path("images").path(gotMedia.getId()).request().get().getStatus());
	}

}