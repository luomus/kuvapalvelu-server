package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class SearchAudio extends BaseApiIntegrationTest {

	private final String SPECIES_ID = "MX.65533";
	private List<Media> gotMedias;
	private String gotId;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.getIdentifications().getTaxonIds().add(SPECIES_ID);
		Media media = uploadFileAndMeta(MediaClass.AUDIO, "bat.wav", meta);
		assertNotNull(media.getId());
		this.gotId = media.getId();
		this.gotMedias = this.mediaApiClient.search(MediaClass.AUDIO, "identifications.taxonIds", SPECIES_ID);
	}

	@Test
	public void searchContainsResult() {
		assertTrue(gotMedias.size() >= 1);
		boolean found = false;
		for (Media m : gotMedias) {
			assertTrue(m.getMeta().getIdentifications().getTaxonIds().contains(SPECIES_ID));
			if (m.getId().equals(gotId)) {
				found = true;
			}
		}
		assertTrue(found);
	}

}
