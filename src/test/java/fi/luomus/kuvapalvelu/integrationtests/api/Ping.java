package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

public class Ping extends BaseApiIntegrationTest {

	@Test
	public void ping() {
		@SuppressWarnings("unchecked")
		Map<String, String> response = target.path("ping").request().get(Map.class);
		assertEquals("pong", response.get("response"));
	}

}
