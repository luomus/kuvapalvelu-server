package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.Meta;

public class NoUnnecessaryTransformations extends BaseApiIntegrationTest {

	private Media media;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		this.media = uploadFileAndMeta("bird.jpeg", meta);
	}

	@Test
	public void noUnnecessaryTransformations() {
		assertNotNull(media.getUrls().getOriginal());

		for (URI uri : Arrays.asList(media.getUrls().getLarge(), media.getUrls().getFull(), media.getUrls().getThumbnail())) {
			assertEquals(media.getUrls().getOriginal(), uri);
		}
	}

}
