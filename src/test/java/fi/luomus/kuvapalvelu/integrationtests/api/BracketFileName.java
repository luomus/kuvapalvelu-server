package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class BracketFileName extends BaseApiIntegrationTest {

	private final String FILE_NAME = "WP_20140916_001[1].jpg";
	private String id;

	@Before
	public void uploadBracketFilenamed() throws Exception {
		Meta meta = meta();
		String id = uploadFileAndMeta(FILE_NAME, meta).getId();
		this.id = id;
	}

	@Test
	public void test() throws Exception {
		Optional<Media> mediaOptional = this.mediaApiClient.get(MediaClass.IMAGE, this.id);
		assertTrue(mediaOptional.isPresent());
		assertEquals(FILE_NAME.replace("[", "_").replace("]", "_"), mediaOptional.get().getMeta().getOriginalFilename());
	}

}
