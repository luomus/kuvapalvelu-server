package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.ws.rs.client.ClientBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import net.avh4.test.junit.Nested;

/**
 * Ensure that the image store also updates protected status
 */
@RunWith(Nested.class)
public class UpdateSecret extends BaseApiIntegrationTest {

	private Media gotMedia;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.setSecret(true);
		meta.setCaption("update secret: initially private");
		this.gotMedia = uploadFileAndMeta("tiny.tiff", meta);
	}

	@Test
	public void cantAccess() {
		assertEquals(401, ClientBuilder.newClient().target(gotMedia.getUrls().getFull()).request().get().getStatus());
	}

	public class SetToNotSecret {
		private Media afterSet;

		@Before
		public void update() throws NotFoundException, ApiException {
			Meta meta = gotMedia.getMeta();
			meta.setSecret(false);
			meta.setCaption("update secret: changed private -> public");
			mediaApiClient.update(MediaClass.IMAGE, gotMedia.getId(), meta);
			this.afterSet = mediaApiClient.get(MediaClass.IMAGE, gotMedia.getId()).get();
		}

		@Test
		public void test() {
			assertFalse(this.afterSet.getMeta().isSecret());
			assertNull(this.afterSet.getSecretKey());
			assertEquals(200, ClientBuilder.newClient().target(gotMedia.getUrls().getFull())
					.request().get().getStatus());
		}

		public class SetToSecret {
			private Media afterSet;

			@Before
			public void update() throws NotFoundException, ApiException {
				Meta meta = gotMedia.getMeta();
				meta.setSecret(true);
				meta.setCaption("update secret: changed public -> private");
				mediaApiClient.update(MediaClass.IMAGE, gotMedia.getId(), meta);
				this.afterSet = mediaApiClient.get(MediaClass.IMAGE, gotMedia.getId()).get();
			}

			@Test
			public void test() {
				assertEquals(true, afterSet.getMeta().isSecret());
				assertNotNull(afterSet.getSecretKey());
				assertEquals(401, ClientBuilder.newClient().target(afterSet.getUrls().getFull())
						.request().get().getStatus());
			}
		}
	}
}