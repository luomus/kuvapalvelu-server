package fi.luomus.kuvapalvelu.integrationtests.api;

import java.io.File;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.junit.After;
import org.junit.Before;

import fi.luomus.kuvapalvelu.client.MediaApiClient;
import fi.luomus.kuvapalvelu.client.MediaApiClientImpl;
import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

public class BaseApiIntegrationTest {
	protected MediaApiClient mediaApiClient;
	protected WebTarget target;
	protected String baseUri;
	protected Set<String> cleanUpMediaIds;

	@Before
	public void setUp() {
		String username = System.getProperty("kuvapalvelu.username");
		String password = System.getProperty("kuvapalvelu.password");
		this.baseUri = System.getProperty("kuvapalvelu.baseUrl");
		this.mediaApiClient = MediaApiClientImpl.builder()
				.uri(baseUri)
				.username(username)
				.password(password)
				.build();
		this.target = ClientBuilder.newClient().register(HttpAuthenticationFeature.basic(username, password)).target(baseUri);
		this.cleanUpMediaIds = new HashSet<>();
	}

	@After
	public void cleanUp() {
		for (String id : cleanUpMediaIds) {
			try {
				this.mediaApiClient.delete(MediaClass.IMAGE, id);
			} catch (ApiException | NotFoundException e) {
				// nop
			}
		}
		if (mediaApiClient != null) try {mediaApiClient.close(); } catch (Exception e) {}
	}

	protected Media uploadFileAndMeta(String resourcename, Meta meta) throws Exception {
		return uploadFileAndMeta(MediaClass.IMAGE, resourcename, meta);
	}

	protected Media uploadFileAndMeta(MediaClass mediaClass, String resourcename, Meta meta) throws Exception {
		File file = getFile(resourcename);
		String id = null;
		if (mediaClass == MediaClass.IMAGE) {
			id = this.mediaApiClient.uploadImage(file, meta);
		} else if (mediaClass == MediaClass.AUDIO) {
			id = this.mediaApiClient.uploadAudio(file, meta);
		} else if (mediaClass == MediaClass.PDF) {
			id = this.mediaApiClient.uploadPdf(file, meta);
		} else {
			throw new UnsupportedOperationException("for " + mediaClass);
		}
		this.cleanUpMediaIds.add(id);
		return this.mediaApiClient.get(mediaClass, id).get();
	}

	protected File getFile(String resourcename) throws URISyntaxException {
		File file = new File(this.getClass().getClassLoader().getResource(resourcename).toURI());
		return file;
	}

	protected Meta meta() {
		return new Meta()
				.setSecret(false)
				.setRightsOwner("rightsOwner")
				.addCapturer("capturer")
				.setLicense("MZ.intellectualRightsARR");
	}

}
