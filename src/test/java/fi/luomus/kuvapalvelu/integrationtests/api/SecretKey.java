package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import java.net.URI;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.Meta;

public class SecretKey extends BaseApiIntegrationTest {

	private String secretKey;
	private URI imageUri;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		meta.setSecret(true);
		Media media = uploadFileAndMeta("tiny.tiff", meta);
		this.secretKey = media.getSecretKey();
		this.imageUri = media.getUrls().getFull();
	}

	@Test
	public void secretKey() {
		WebTarget webTarget = ClientBuilder.newClient().target(this.imageUri);
		assertEquals(401, webTarget.request().get().getStatus());
		assertEquals(403, webTarget.queryParam("secret", "notValid").request().get().getStatus());
		assertEquals(200, webTarget.queryParam("secret", this.secretKey).request().get().getStatus());
	}

}