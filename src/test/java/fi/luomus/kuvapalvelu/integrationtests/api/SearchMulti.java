package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;

public class SearchMulti extends BaseApiIntegrationTest {

	private int diff;

	@Before
	public void testSearchMulti() throws Exception {
		final String FIRST_ID = UUID.randomUUID().toString();
		final String SECOND_ID = UUID.randomUUID().toString();

		Meta meta = new Meta()
				.addTaxonId("MX.65532")
				.addTaxonVerbatim("elukka")
				.setSecret(false)
				.setCaptureDateTime(DateTime.now())
				.addCapturer("capturer")
				.addDocumentId(FIRST_ID)
				.setOriginalFilename("bird.jpeg")
				.setRightsOwner("owner")
				.addTag("aTag")
				.setLicense("MZ.intellectualRightsARR");

		int oldAmount = this.mediaApiClient.search(MediaClass.IMAGE, "documentIds", Arrays.asList(FIRST_ID, SECOND_ID)).size();
		uploadFileAndMeta("bird.jpeg", meta);
		meta.setDocumentIds(Arrays.asList(SECOND_ID));
		uploadFileAndMeta("bird.jpeg", meta);
		int newAmount = this.mediaApiClient.search(MediaClass.IMAGE, "documentIds", Arrays.asList(FIRST_ID, SECOND_ID)).size();
		this.diff = newAmount - oldAmount;
	}

	@Test
	public void twoResultsMore() {
		assertEquals(2, this.diff);
	}

}
