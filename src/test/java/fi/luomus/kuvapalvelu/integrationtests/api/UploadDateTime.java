package fi.luomus.kuvapalvelu.integrationtests.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.Meta;

public class UploadDateTime extends BaseApiIntegrationTest {

	private Media gotMedia;
	private DateTime uploadMoment;

	@Before
	public void upload() throws Exception {
		Meta meta = meta();
		this.uploadMoment = new DateTime();
		this.gotMedia = uploadFileAndMeta("bird.jpeg", meta);
	}

	@Test
	public void uploadedDateTime() {
		assertNotNull(gotMedia.getMeta().getUploadDateTime());
		assertTrue(Math.abs(gotMedia.getMeta().getUploadDateTime().getMillis() - uploadMoment.getMillis()) < 10000);
	}

}