package fi.luomus.kuvapalvelu.integrationtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Map;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.MediaFileRepositoryImpl;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.MapBackedLuomusPreferences;


public class MediaRepositoryImplTest {
	@Test
	public void testUploadOneAndDelete() throws Exception {
		File file = new File(this.getClass().getClassLoader().getResource("bird.jpeg").toURI());
		testUploadOneAndDelete(file);
	}

	@Test
	public void testSpace() throws Exception {
		File withSpace = new File(this.getClass().getClassLoader().getResource("bird space.jpeg").toURI());
		testUploadOneAndDelete(withSpace);
	}

	private void testUploadOneAndDelete(File image) throws ApiException, IOException, FileNotFoundException, NotFoundException {
		MediaFileRepositoryImpl repo = repo();

		TransformedMedia transformedImages = new TransformedMedia();
		transformedImages.add(image, MediaFileType.ORIGINAL_IMAGE, new MediaType("image", "jpg"));

		String resourceId = "test." + new Double(Math.random() * 5000).intValue();
		Map<MediaFileType, URI> uploadedFileUris = repo.upload(resourceId, transformedImages, false);

		assertEquals(1, uploadedFileUris.size());
		assertTrue(uploadedFileUris.containsKey(MediaFileType.ORIGINAL_IMAGE));

		URI imageURL = uploadedFileUris.get(MediaFileType.ORIGINAL_IMAGE);
		assertNotNull(imageURL);

		InputStream got = ClientBuilder.newClient().target(imageURL).request().get(InputStream.class);

		assertTrue(Arrays.equals(IOUtils.toByteArray(new FileInputStream(image)), IOUtils.toByteArray(got)));

		repo.delete(resourceId);
		assertEquals(404, ClientBuilder.newClient().target(imageURL).request().get().getStatus());
	}

	private MediaFileRepositoryImpl repo() {
		MediaFileRepositoryImpl repo = new MediaFileRepositoryImpl(new MapBackedLuomusPreferences(ImmutableMap.<String, Object>builder()
				.put(MediaApiPreferences.MEDIA_REPOSITORY_URL.key, "https://imagetest.laji.fi/")
				.put(MediaApiPreferences.MEDIA_REPOSITORY_USERNAME.key, "image")
				.put(MediaApiPreferences.MEDIA_REPOSITORY_PASSWORD.key, "library")
				.build()));
		return repo;
	}

}
