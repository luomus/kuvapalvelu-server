package fi.luomus.kuvapalvelu.integrationtests;

import java.io.IOException;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.kuvapalvelu.util.BeanConfiguration;
import fi.luomus.utils.exceptions.ApiException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class MediaMetaDataRepositoryImplTest {

	@Inject
	MediaMetaDataRepository mediaMetaDataRepository;

	@Test
	public void testUpdate() throws IOException, ApiException {
		final String originalJson = "{\"id\":\"MM.91722\",\"secretKey\":null,\"urls\":{\"original\":\"https://image.laji.fi/MM.91722/WP_20150730_005%5B1%5D.jpg\",\"full\":\"https://image.laji.fi/MM.91722/WP_20150730_005%5B1%5D.jpg\",\"large\":\"https://image.laji.fi/MM.91722/WP_20150730_005%5B1%5D_large.jpg\",\"square\":\"https://image.laji.fi/MM.91722/WP_20150730_005%5B1%5D_square.jpg\",\"thumbnail\":\"https://image.laji.fi/MM.91722/WP_20150730_005%5B1%5D_thumb.jpg\"},\"meta\":{\"originalFilename\":\"WP_20150730_005[1].jpg\",\"capturers\":[\"Tea von Bonsdorff\"],\"rightsOwner\":\"Tea von Bonsdorff\",\"license\":\"MZ.intellectualRightsCC-BY-SA-4.0\",\"luomusRights\":null,\"identifications\":{\"taxonIds\":[],\"verbatim\":[\"Russula pectinatoides\"]},\"tags\":[],\"documentIds\":[\"55b9d2c92d0e222a8abbaaa2\"],\"uploadedBy\":\"Tea von Bonsdorff\",\"sourceSystem\":\"KE.176\",\"orginalFileChecksum\":null,\"sortOrder\":null,\"secret\":false,\"wgs84Coordinates\":{\"lat\":60.174797181909085,\"lon\":24.949371814727783},\"captureDateTime\":1438203600,\"uploadDateTime\":1438241482}}";
		Meta meta = new Meta();
		meta.getIdentifications().getTaxonIds().add("MX.65532");
		meta.getIdentifications().getVerbatim().add("elukka");
		meta.setSecret(false);
		meta.setCaptureDateTime(DateTime.now());
		meta.getCapturers().add("capturer");
		meta.setOriginalFilename("foobar.tiff");
		meta.setRightsOwner("owner");
		meta.getTags().add("aTag");
		meta.setLicense("MZ.intellectualRightsARR");
		Media original = new BeanConfiguration().objectMapper().readValue(originalJson, Media.class);
		String newId = mediaMetaDataRepository.newId();
		mediaMetaDataRepository.save(newId, meta, original.getUrls(), MediaClass.IMAGE);
	}

}
