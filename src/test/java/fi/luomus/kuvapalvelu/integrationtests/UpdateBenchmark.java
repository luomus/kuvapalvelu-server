package fi.luomus.kuvapalvelu.integrationtests;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NewMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService;
import fi.luomus.kuvapalvelu.util.BeanConfiguration;
import fi.luomus.utils.exceptions.ApiException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class UpdateBenchmark {
	@Inject
	private MediaService mediaService;
	@Inject
	private TempFileRepository tempFileRepository;
	private Long timing;

	@Test
	@Ignore // enable for manual performance benchmarking
	public void test() throws IOException, ApiException {
		InputStream inputStream = this.getClass().getClassLoader().getResource("tiny.tiff").openStream();
		NewMedia newMedia = new NewMedia();
		String json = "{\"originalFilename\":\"giant.tiff\",\"capturers\":[\"capturer\"],\"rightsOwner\":\"rightsOwner\" ,\"license\":\"MZ.intellectualRightsODBL-1.0\",\"luomusRights\":null,\"identifications\":{\"taxonIds\":[],\"verbatim\":[]},\"captureDateTime\":null,\"tags\":[],\"wgs84Coordinates\":{\"lat\": null,\"lon\":null},\"documentId\":\"http://id.luomus.fi/JA.1\",\"uploadedBy\":\"MA.97\",\"sourceSystem\":\"KE.21\",\"secret\":false}";
		startTiming("saving temp file");
		Meta meta = new BeanConfiguration().objectMapper().readValue(json, Meta.class);
		newMedia.setMeta(meta);
		TempFileRepository.TempFileInfo tempFileInfo = tempFileRepository.storeTempFile(inputStream, "tiny.tiff");
		stopTiming();
		newMedia.setTempFileId(tempFileInfo.getId());
		newMedia.setMediaClass(MediaClass.IMAGE);

		startTiming("creating media");
		Media createdMedia = mediaService.create(newMedia);
		stopTiming();

		startTiming("updating media");
		mediaService.updateMeta(meta, createdMedia.getId(), MediaClass.IMAGE);
		stopTiming();

		startTiming("updating media with warmed up caches");
		mediaService.updateMeta(meta, createdMedia.getId(), MediaClass.IMAGE);
		stopTiming();
	}

	private void startTiming(String action) {
		System.out.println("started timing " + action);
		if (timing != null) {
			throw new IllegalStateException();
		}
		timing = System.nanoTime();
	}

	private void stopTiming() {
		if (timing == null) {
			throw new IllegalStateException();
		}
		long result = (System.nanoTime() - timing) / 1000000;
		timing = null;
		System.out.println("took " + result + " milliseconds");
	}
}
