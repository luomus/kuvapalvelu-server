package fi.luomus.kuvapalvelu.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fi.luomus.kuvapalvelu.service.NameCleaner;

public class NameCleanerTests {

	@Test
	public void trim() {
		String filename = "very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name.jpg";
		String cleaned = NameCleaner.clean(filename);
		assertEquals("very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_very_long_name_.jpg", cleaned);
	}

	@Test
	public void replace() {
		String filename = "Ääneskoski, \"rykäkallio\", 14.2.2009|E&V Väänänen.JPG";
		String cleaned = NameCleaner.clean(filename);
		assertEquals("Aaneskoski_rykakallio_14.2.2009_E&V_Vaananen.JPG", cleaned);
	}

	@Test
	public void more_special_chars() {
		assertEquals("asd(s)ASD_OA_(2).jpg", NameCleaner.clean("asd(s)ASD[ÖÄ](2).jpg"));
	}

}
