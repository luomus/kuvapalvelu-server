package fi.luomus.kuvapalvelu.util;

import java.util.HashMap;
import java.util.Map;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

public class MockPreferences extends AbstractPreferences {
	private Map<String, String> map;

	public MockPreferences(Map<String, String> map) {
		super(null, "");
		this.map = map;
	}

	@Override
	protected void putSpi(String key, String value) {
		map.put(key, value);
	}

	@Override
	protected String getSpi(String key) {
		return map.get(key);
	}

	@Override
	protected void removeSpi(String key) {
		map.remove(key);
	}

	@Override
	protected void removeNodeSpi() throws BackingStoreException {
		map = new HashMap<>();
	}

	@Override
	protected String[] keysSpi() throws BackingStoreException {
		return map.keySet().toArray(new String[0]);
	}

	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException {
		return new String[0];
	}

	@Override
	protected AbstractPreferences childSpi(String name) {
		return null;
	}

	@Override
	protected void syncSpi() throws BackingStoreException {
	}

	@Override
	protected void flushSpi() throws BackingStoreException {
	}

}