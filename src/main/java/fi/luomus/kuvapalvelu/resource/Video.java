package fi.luomus.kuvapalvelu.resource;

import javax.ws.rs.Path;

import fi.luomus.kuvapalvelu.model.MediaClass;

@Path("api/video")
public class Video extends AbstractMediaEndpoint {

	@Override
	protected MediaClass mediaClass() {
		return MediaClass.VIDEO;
	}

}
