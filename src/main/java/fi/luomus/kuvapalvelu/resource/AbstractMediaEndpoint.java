package fi.luomus.kuvapalvelu.resource;

import static fi.luomus.kuvapalvelu.util.UtilClass.generateError;
import static fi.luomus.kuvapalvelu.util.UtilClass.notFound;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import com.google.common.base.Optional;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NewMedia;
import fi.luomus.kuvapalvelu.model.NewMultiSourceMedia;
import fi.luomus.kuvapalvelu.repository.MediaFileRepository;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.model.LocalizedString;

public abstract class AbstractMediaEndpoint {

	private static final String INVALID_PARAMETERS = "INVALID_PARAMETERS";
	private static final String ACCESS_DENIED = "ACCESS_DENIED";
	private static final String KUVAPALVELU_ADMIN = "kuvapalvelu-admin";
	public static final String SEARCH_FOR_SUB_TAXA = "searchForSubTaxons";
	public static final String OFFSET = "offset";
	public static final String LIMIT = "limit";

	private static final Set<String> PARAMETERS;
	static {
		PARAMETERS = new HashSet<>();
		PARAMETERS.add(SEARCH_FOR_SUB_TAXA);
		PARAMETERS.add(OFFSET);
		PARAMETERS.add(LIMIT);
	}
	private static final int MAX_COUNT_OF_SUB_TAXA = 100;

	@Inject
	private SecurityContext securityContext;

	@Inject
	private MediaMetaDataRepository mediaMetaDataRepository;
	@Inject
	private MediaFileRepository mediaFileRepository;
	@Inject
	private TempFileRepository tempFileRepository;
	@Inject
	private MediaService mediaService;

	protected abstract MediaClass mediaClass();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Media> create(@Valid List<NewMedia> medias) throws IOException, ApiException {
		try {
			checkTempFiles(medias);
			List<Media> result = new ArrayList<>();
			for (NewMedia media : medias) {
				if (media.getMediaClass() == null) media.setMediaClass(mediaClass());
				media.getMeta().setSourceSystem(securityContext.getUserPrincipal().getName());
				result.add(mediaService.create(media));
			}
			return result;
		} catch (IllegalArgumentException e) {
			throw metaValidationException(e);
		}
	}

	@Path("multi")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Media> createMultiSource(@Valid List<NewMultiSourceMedia> multiSources) throws IOException, ApiException {
		try {
			checkMultiTempFiles(multiSources);
			List<Media> result = new ArrayList<>();
			for (NewMultiSourceMedia multiSource : multiSources) {
				if (multiSource.getMediaClass() == null) multiSource.setMediaClass(mediaClass());
				multiSource.getMeta().setSourceSystem(securityContext.getUserPrincipal().getName());
				result.add(mediaService.create(multiSource));
			}
			return result;
		} catch (IllegalArgumentException e) {
			throw metaValidationException(e);
		}
	}

	private void checkTempFiles(List<NewMedia> medias) throws IOException {
		final Set<String> missingIds = new HashSet<>();
		for (NewMedia media : medias) {
			Optional<File> handle = tempFileRepository.getHandle(media.getTempFileId());
			if (!handle.isPresent()) {
				missingIds.add(media.getTempFileId());
			}
		}
		if (missingIds.size() > 0) {
			throw generateError(Response.Status.NOT_FOUND.getStatusCode(), new BaseErrorEntity("TEMP_FILE_NOT_FOUND") {
				@SuppressWarnings("unused")
				public Set<String> getMissingTempFiles() {
					return missingIds;
				}
			});
		}
	}

	private void checkMultiTempFiles(List<NewMultiSourceMedia> multiSources) throws IOException {
		final Set<String> missingIds = new HashSet<>();
		for (NewMultiSourceMedia multiSource : multiSources) {
			for (NewMultiSourceMedia.MediaFile media : multiSource) {
				Optional<File> handle = tempFileRepository.getHandle(media.getTempFileId());
				if (!handle.isPresent()) {
					missingIds.add(media.getTempFileId());
				}
			}
		}

		if (missingIds.size() > 0) {
			throw generateError(Response.Status.NOT_FOUND.getStatusCode(), new BaseErrorEntity("TEMP_FILE_NOT_FOUND") {
				@SuppressWarnings("unused")
				public Set<String> getMissingTempFiles() {
					return missingIds;
				}
			});
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Media> search(@QueryParam(LIMIT) @DefaultValue("1000") Integer limit,
			@QueryParam(OFFSET) @DefaultValue("0") Integer offset,
			@QueryParam(SEARCH_FOR_SUB_TAXA) @DefaultValue("false") Boolean searchForSubTaxa,
			@Context UriInfo uriInfo) throws ApiException {
		MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
		Optional<String> searchKeyOptional = validateAndGetSearchKey(queryParameters);
		if (!searchKeyOptional.isPresent()) {
			throw generateError(Response.Status.BAD_REQUEST, new BaseErrorEntity(INVALID_PARAMETERS) {
				@SuppressWarnings("unused")
				public String getCause() {
					return "query parameter must be provided";
				}
			});
		}
		String searchKey = searchKeyOptional.get();
		List<String> values = queryParameters.get(searchKey);
		if (searchForSubTaxa) {
			values.addAll(validateAndGetSubTaxa(searchKey, values));
		}

		return searchResult(searchKey, values, limit, offset);
	}


	private Optional<String> validateAndGetSearchKey(MultivaluedMap<String, String> queryParameters) {
		String searchKey = null;
		for (String parameter : queryParameters.keySet()) {
			if (PARAMETERS.contains(parameter)) {
				continue;
			}
			if (searchKey != null) {
				// searchkey has been found already so there must be multiple
				throw generateError(Response.Status.BAD_REQUEST, new BaseErrorEntity(INVALID_PARAMETERS) {
					@SuppressWarnings("unused")
					public String getCause() {
						return "only one query parameter supported";
					}
				});
			}
			// found it
			searchKey = parameter;
		}

		return Optional.fromNullable(searchKey);
	}

	private List<String> validateAndGetSubTaxa(String searchKey, List<String> values) throws ApiException {
		if (!searchKey.equals("identifications.taxonIds")) {
			throw generateError(Response.Status.BAD_REQUEST, new BaseErrorEntity(INVALID_PARAMETERS) {
				@SuppressWarnings("unused")
				public String getCause() {
					return "cannot search for subtaxons for non-taxon field key";
				}
			});
		}
		List<String> allSubTaxa = new ArrayList<>();
		for (String value : values) {
			List<String> subTaxa = this.mediaMetaDataRepository.getSubTaxa(value);
			if (allSubTaxa.size() + subTaxa.size() > MAX_COUNT_OF_SUB_TAXA) {
				return allSubTaxa;
			}
			allSubTaxa.addAll(subTaxa);
		}

		return allSubTaxa;
	}

	private List<Media> searchResult(String searchKey, List<String> values, int limit, int offset) throws ApiException {
		try {
			List<Media> searchResult = mediaService.search(searchKey, values, limit, offset, mediaClass());
			Iterator<Media> iterator = searchResult.iterator();
			// remove the ones that do not have read rights
			while (iterator.hasNext()) {
				if (!hasReadRights(iterator.next().getMeta())) {
					iterator.remove();
				}
			}

			return searchResult;
		} catch (final MediaMetaDataRepository.InvalidParameterException e) {
			throw generateError(Response.Status.BAD_REQUEST, new BaseErrorEntity(INVALID_PARAMETERS) {
				@SuppressWarnings("unused")
				public List<String> getFields() {
					return e.getInvalidParams();
				}
			});
		}
	}

	@Path("{id}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Media get(@PathParam("id") String id) throws ApiException {
		Optional<Media> optional = getOne(id);
		if (!optional.isPresent()) {
			throw notFound();
		}
		return optional.get();
	}

	@Path("{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateOne(@PathParam("id") String id, @Valid Meta meta) throws ApiException {
		try {
			Meta existingMeta = checkExistsAndHasWriteRights(id);
			setExistingMeta(meta, existingMeta);
			this.mediaService.updateMeta(meta, id, mediaClass());
			return Response.noContent().build();
		} catch (IllegalArgumentException e) {
			throw metaValidationException(e);
		}
	}

	private WebApplicationException metaValidationException(IllegalArgumentException e) {
		return generateError(Response.Status.BAD_REQUEST, new BaseErrorEntity(INVALID_PARAMETERS) {
			@SuppressWarnings("unused")
			public String getMessage() {
				return e.getMessage();
			}
		});
	}

	private void setExistingMeta(Meta newMeta, Meta existingMeta) {
		// These are not allowed to be updated
		newMeta.setOriginalFilename(existingMeta.getOriginalFilename());
		newMeta.setSourceSystem(existingMeta.getSourceSystem());
		newMeta.setUploadDateTime(existingMeta.getUploadDateTime());
		newMeta.setUploadedBy(existingMeta.getUploadedBy());

		if (!"v2".equals(newMeta.getMetadataVersion())) {
			// Don't delete existing meta if meta is being updated by a system that does not support v2 fields
			if (given(existingMeta.getLifeStage()) && !given(newMeta.getLifeStage()))
				newMeta.setLifeStage(existingMeta.getLifeStage());
			if (given(existingMeta.getPlantLifeStage()) && !given(newMeta.getPlantLifeStage()))
				newMeta.setPlantLifeStage(existingMeta.getPlantLifeStage());
			if (given(existingMeta.getPrimaryForTaxon()) && !given(newMeta.getPrimaryForTaxon()))
				newMeta.setPrimaryForTaxon(existingMeta.getPrimaryForTaxon());
			if (given(existingMeta.getSex()) && !given(newMeta.getSex()))
				newMeta.setSex(existingMeta.getSex());
			if (given(existingMeta.getSide()) && !given(newMeta.getSide()))
				newMeta.setSide(existingMeta.getSide());
			if (given(existingMeta.getType()) && !given(newMeta.getType()))
				newMeta.setType(existingMeta.getType());
			if (given(existingMeta.getTaxonDescriptionCaption()) && !given(newMeta.getTaxonDescriptionCaption()))
				newMeta.setTaxonDescriptionCaption(existingMeta.getTaxonDescriptionCaption());
			if (given(existingMeta.getFullResolutionMediaAvailable()) && !given(newMeta.getFullResolutionMediaAvailable()))
				newMeta.setFullResolutionMediaAvailable(existingMeta.getFullResolutionMediaAvailable());
			if (given(existingMeta.getSortOrder()) && !given(newMeta.getSortOrder()))
				newMeta.setSortOrder(existingMeta.getSortOrder());
		}
	}

	private boolean given(List<?> list) {
		return list != null && !list.isEmpty();
	}

	private boolean given(Object o) {
		return o != null;
	}

	private boolean given(LocalizedString s) {
		return s != null && !s.isEmpty();
	}

	@Path("{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteOne(@PathParam("id") String id) throws ApiException, NotFoundException {
		checkExistsAndHasWriteRights(id);
		mediaMetaDataRepository.delete(id);
		mediaFileRepository.delete(id);
		return Response.noContent().build();
	}

	private Meta checkExistsAndHasWriteRights(String id) throws ApiException {
		Optional<MediaMetaDataRepository.MetaAndUrls> optional = mediaMetaDataRepository.get(id);
		if (!optional.isPresent()) {
			throw notFound();
		}
		if (!hasWriteRights(optional.get().meta)) {
			throw generateError(Response.Status.FORBIDDEN, ACCESS_DENIED);
		}
		return optional.get().meta;
	}

	private Optional<Media> getOne(String id) throws ApiException {
		Optional<Media> result = mediaService.get(id);

		// The resource is protected, can only be viewed by an admin or the owner of the resource
		if (result.isPresent() && !hasReadRights(result.get().getMeta())) {
			throw generateError(Response.Status.FORBIDDEN, ACCESS_DENIED);
		}

		return result;
	}

	private boolean hasReadRights(Meta meta) {
		return !meta.isSecret() || securityContext.getUserPrincipal().getName().equals(meta.getSourceSystem()) ||
				securityContext.isUserInRole(KUVAPALVELU_ADMIN);
	}

	private boolean hasWriteRights(Meta meta) {
		return securityContext.getUserPrincipal().getName().equals(meta.getSourceSystem())
				|| securityContext.isUserInRole(KUVAPALVELU_ADMIN);
	}
}
