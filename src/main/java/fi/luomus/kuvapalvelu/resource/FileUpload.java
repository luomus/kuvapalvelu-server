package fi.luomus.kuvapalvelu.resource;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.MultiPart;

import com.google.common.base.Charsets;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.kuvapalvelu.model.FileUploadResponse;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.service.NameCleaner;
import fi.luomus.kuvapalvelu.util.UtilClass;
import fi.luomus.utils.exceptions.NotFoundException;

@Path("api/fileUpload")
public class FileUpload {

	@Inject
	private TempFileRepository tempFileStorage;
	@Inject
	private MediaService mediaService;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public List<FileUploadResponse> upload(MultiPart multipart, @QueryParam("mediaClass") String mediaClassParam) throws IOException {
		List<FileUploadResponse> response = new ArrayList<>();
		for (BodyPart bodyPart : multipart.getBodyParts()) {
			response.add(processPart(bodyPart, mediaClassParam));
		}
		return response;
	}

	private FileUploadResponse processPart(BodyPart bodyPart, String mediaClassParam) throws IOException {
		final String bodyPartNameMediaTypeParam = ((FormDataContentDisposition) bodyPart.getContentDisposition()).getName();
		final String originalFilename = parseOriginalFileName(bodyPart, bodyPartNameMediaTypeParam);
		MediaClass mediaClass = resolveMediaClass(mediaClassParam, bodyPartNameMediaTypeParam);
		MediaFileType mediaType = resolveMediaType(bodyPartNameMediaTypeParam);

		TempFileRepository.TempFileInfo tempFile = storeTempFile(bodyPart, originalFilename);

		validate(bodyPartNameMediaTypeParam, originalFilename, mediaClass, mediaType, tempFile);

		return response(bodyPartNameMediaTypeParam, originalFilename, tempFile);
	}

	private FileUploadResponse response(final String bodyPartName, final String originalFilename, TempFileRepository.TempFileInfo tempFile) {
		FileUploadResponse fileUploadResponse = new FileUploadResponse();
		fileUploadResponse.setName(bodyPartName);
		fileUploadResponse.setFilename(originalFilename);
		fileUploadResponse.setExpires(tempFile.getExpires());
		fileUploadResponse.setId(tempFile.getId());
		return fileUploadResponse;
	}

	private void validate(final String bodyPartName, final String originalFilename, MediaClass mediaClass, MediaFileType mediaType, TempFileRepository.TempFileInfo tempFile) throws IOException {
		File file = tempFileStorage.getHandle(tempFile.getId()).get();
		final CanTransform transformationCheck = mediaService.check(file, mediaClass, mediaType);
		if (!transformationCheck.isTransformable()) {
			try {
				tempFileStorage.delete(tempFile.getId());
			} catch (NotFoundException e) {
				// nop
			}
			throw error(bodyPartName, originalFilename, transformationCheck.getError());
		}
	}

	private TempFileRepository.TempFileInfo storeTempFile(BodyPart bodyPart, final String originalFilename) throws IOException {
		BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();

		TempFileRepository.TempFileInfo tempFile = tempFileStorage.storeTempFile(bpe.getInputStream(), originalFilename);
		return tempFile;
	}

	private String parseOriginalFileName(BodyPart bodyPart, String bodyPartName) {
		String fileName = bodyPart.getContentDisposition().getFileName();
		if (fileName == null) {
			throw error(bodyPartName, null, "content-disposition filename is missing");
		}
		// encoding hack
		fileName = new String(fileName.getBytes(Charsets.ISO_8859_1), Charsets.UTF_8);
		return NameCleaner.clean(fileName);
	}

	private MediaClass resolveMediaClass(String mediaClassParam, String fileTypeParam) {
		for (MediaFileType t : MediaFileType.values()) {
			if (t.name().equals(fileTypeParam)) {
				return t.getMediaClass();
			}
		}
		for (MediaClass cl : MediaClass.values()) {
			if (cl.name().equals(mediaClassParam)) {
				return cl;
			}
		}
		return null;
	}

	private MediaFileType resolveMediaType(String fileTypeParam) {
		for (MediaFileType t : MediaFileType.values()) {
			if (t.name().equals(fileTypeParam)) {
				return t;
			}
		}
		return null;
	}


	private WebApplicationException error(final String name, final String filename, final String error) throws WebApplicationException {
		return UtilClass.generateError(Response.Status.BAD_REQUEST.getStatusCode(), new BaseErrorEntity("INVALID_FILE") {
			@SuppressWarnings("unused")
			public String getName() {
				return name;
			}

			@SuppressWarnings("unused")
			public String getFilename() {
				return filename;
			}

			@SuppressWarnings("unused")
			public String getError() {
				return error;
			}
		});
	}

}
