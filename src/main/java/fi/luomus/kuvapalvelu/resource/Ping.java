package fi.luomus.kuvapalvelu.resource;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.joda.time.DateTime;

/**
 * Used to ensure that the service is deployed and running, also for making sure exception reporting works
 */
@Path("ping")
public class Ping {

	private static DateTime lastThrown;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, String> ping(@QueryParam("exception") String exception) {
		Map<String, String> response = new HashMap<>();
		response.put("response", "pong");
		if (exception != null && exception.length() > 0) {
			causeException(exception);
		}

		return response;
	}

	private synchronized static void causeException(String exception) {
		DateTime now = DateTime.now();
		if (lastThrown != null && lastThrown.plusSeconds(30).isAfter(now)) {
			return;
		}
		lastThrown = now;
		throw new RuntimeException(exception);
	}

}
