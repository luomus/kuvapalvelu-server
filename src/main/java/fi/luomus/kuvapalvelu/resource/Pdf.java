package fi.luomus.kuvapalvelu.resource;

import javax.ws.rs.Path;

import fi.luomus.kuvapalvelu.model.MediaClass;

@Path("api/pdf")
public class Pdf extends AbstractMediaEndpoint {

	@Override
	protected MediaClass mediaClass() {
		return MediaClass.PDF;
	}

}
