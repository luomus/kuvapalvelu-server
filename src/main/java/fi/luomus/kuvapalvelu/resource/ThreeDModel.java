package fi.luomus.kuvapalvelu.resource;

import javax.ws.rs.Path;

import fi.luomus.kuvapalvelu.model.MediaClass;

@Path("api/3d-model")
public class ThreeDModel extends AbstractMediaEndpoint {

	@Override
	protected MediaClass mediaClass() {
		return MediaClass.MODEL;
	}

}
