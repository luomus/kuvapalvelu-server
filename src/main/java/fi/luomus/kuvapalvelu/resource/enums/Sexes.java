package fi.luomus.kuvapalvelu.resource.enums;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.utils.exceptions.ApiException;

@Path("api/sexes")
public class Sexes {

	@Inject
	private MediaMetaDataRepository mediaMetaDataRepository;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Sex> get() throws ApiException {
		return mediaMetaDataRepository.getSexes();
	}

}
