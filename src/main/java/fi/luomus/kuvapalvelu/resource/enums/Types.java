package fi.luomus.kuvapalvelu.resource.enums;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.utils.exceptions.ApiException;

@Path("api/types")
public class Types {

	@Inject
	private MediaMetaDataRepository mediaMetaDataRepository;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Type> getLicenses() throws ApiException {
		return mediaMetaDataRepository.getTypes();
	}

}
