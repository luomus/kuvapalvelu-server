package fi.luomus.kuvapalvelu.service;

import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import fi.luomus.kuvapalvelu.model.NamedResource;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;

@Service
public class CachedEnumsService {

	private final LoadingCache<String, Set<String>> cache;

	@Inject
	public CachedEnumsService(MediaMetaDataRepository mediaMetaDataRepository) {
		this.cache = CacheBuilder.newBuilder().expireAfterWrite(3, TimeUnit.HOURS).build(new CacheLoader<String, Set<String>>() {

			@Override
			public Set<String> load(String key) throws Exception {
				Method getter = MediaMetaDataRepository.class.getDeclaredMethod("get"+key);
				return ids(getter.invoke(mediaMetaDataRepository));
			}

			private Set<String> ids(Object vals) {
				@SuppressWarnings("unchecked")
				List<? extends NamedResource> list = (List<? extends NamedResource>) vals;
				Set<String> set = new LinkedHashSet<>(list.size());
				list.stream().map(r->r.getId()).forEach(set::add);
				return set;
			}

		});
	}

	/**
	 * Return a list of available licenses
	 *
	 * @return List of licenses
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getLicenses() throws ExecutionException {
		return cache.get("Licenses");
	}

	/**
	 * Return a list of available sexes
	 *
	 * @return List of sexes
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getSexes() throws ExecutionException {
		return cache.get("Sexes");
	}

	/**
	 * Return a list of available life stages
	 *
	 * @return List of life stages
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getLifeStages() throws ExecutionException {
		return cache.get("LifeStages");
	}

	/**
	 * Return a list of available plant life stages
	 *
	 * @return List of plant life stages
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getPlantLifeStages() throws ExecutionException {
		return cache.get("PlantLifeStages");
	}

	/**
	 * Return a list of available taxon image sides
	 *
	 * @return List of sides
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getSides() throws ExecutionException {
		return cache.get("Sides");
	}

	/**
	 * Return a list of available taxon image types
	 *
	 * @return List of types
	 * @throws ExecutionException Thrown if a communication error occurred during the operation
	 */
	Set<String> getTypes() throws ExecutionException {
		return cache.get("Types");
	}

}
