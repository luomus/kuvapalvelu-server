package fi.luomus.kuvapalvelu.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class LowDetailModelTransformer extends AbstractModelTransformer {

	private static final long MAX_FILE_SIZE = 30 * 1024 * 1024; // 30 MB

	@Inject
	public LowDetailModelTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected MediaFileType mediaFileType() {
		return MediaFileType.LOW_DETAIL_MODEL;
	}

	@Override
	protected long maxSize() {
		return MAX_FILE_SIZE;
	}

}
