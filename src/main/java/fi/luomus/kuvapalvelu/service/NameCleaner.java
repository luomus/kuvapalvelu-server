package fi.luomus.kuvapalvelu.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;

public class NameCleaner {

	private static final Set<Character> ALLOWED = initAllowed();
	private static final Map<Character, Character> REPLACE = initReplace();

	private static Map<Character, Character> initReplace() {
		Map<Character, Character> m = new HashMap<>();
		m.put('ä', 'a');
		m.put('Ä', 'A');
		m.put('ö', 'o');
		m.put('Ö', 'O');
		m.put('å', 'a');
		m.put('Å', 'A');
		m.put(' ', '_');
		return m;
	}

	private static Set<Character> initAllowed() {
		String allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._&()";
		Set<Character> s = new HashSet<>();
		for (char c : allowed.toCharArray()) {
			s.add(c);
		}
		return s;
	}

	public static String clean(String filename) {
		if (filename == null) return null;

		StringBuilder b = new StringBuilder();
		for (char c : filename.toCharArray()) {
			if (REPLACE.containsKey(c)) {
				b.append(REPLACE.get(c));
			} else if (ALLOWED.contains(c)) {
				b.append(c);
			} else {
				b.append("_");
			}
		}
		filename = b.toString();
		while (filename.contains("__")) {
			filename = filename.replace("__", "_");
		}
		if (filename.length() > 150) {
			filename = b.toString();
			String filenameExtension = FilenameUtils.getExtension(filename);
			return filename.substring(0, 120) + "." + filenameExtension;
		}
		return filename;
	}

}
