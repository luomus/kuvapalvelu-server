package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil.CommandExecutionResult;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class AudioTransformer extends AbstractTransformer {

	private static final int MAX_FILE_SIZE = 100 * 1024 * 1024; // 100 MB
	private static final MediaType MP3_MEDIA_TYPE = new MediaType("audio", "mpeg");
	private static final MediaType WAV_MEDIA_TYPE = new MediaType("audio", "x-wav");
	private static final MediaType FLAC_MEDIA_TYPE = new MediaType("audio", "flac");

	private static List<String> ALLOWED_EXTENSIONS;
	static {
		ALLOWED_EXTENSIONS = new ArrayList<>();
		ALLOWED_EXTENSIONS.add("wav");
		ALLOWED_EXTENSIONS.add("mp3");
		ALLOWED_EXTENSIONS.add("flac");
	}

	private static final Map<String, MediaType> ALLOWED_MEDIA_TYPES;
	static {
		ALLOWED_MEDIA_TYPES = new HashMap<>();
		ALLOWED_MEDIA_TYPES.put("audio/x-wav", WAV_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/wav", WAV_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/wave", WAV_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/vnd.wave", WAV_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/mpeg", MP3_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("application/octet-stream", MP3_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/flac", FLAC_MEDIA_TYPE);
		ALLOWED_MEDIA_TYPES.put("audio/x-flac", FLAC_MEDIA_TYPE);
	}

	@Inject
	public AudioTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected CanTransform canTransformActual(File source) {
		try {
			String ext = FilenameUtils.getExtension(source.getName()).toLowerCase();
			if (!ALLOWED_EXTENSIONS.contains(ext)) {
				return CanTransform.no("Not valid extension: " + ext + " <> " + ALLOWED_EXTENSIONS);
			}
			MediaType type = getAudioType(source);
			if (type == null) return CanTransform.no("Unknown media type");
			return CanTransform.yes();
		} catch (Exception e) {
			return CanTransform.no("Exception: " + e.getMessage());
		}
	}

	@Override
	protected TransformedMedia transformActual(File source, String fileName) {
		try {
			TransformedMedia transformedMedia = new TransformedMedia();
			MediaType originalMediaType = getAudioType(source);
			if (originalMediaType == WAV_MEDIA_TYPE) {
				transformedMedia.add(transformWav(source, fileName), MediaFileType.WAV, WAV_MEDIA_TYPE);
				transformedMedia.add(transformMp3(source, fileName), MediaFileType.MP3, MP3_MEDIA_TYPE);
				generateSpectrogram(source, fileName, transformedMedia);
				return transformedMedia;
			}
			if (originalMediaType == MP3_MEDIA_TYPE) {
				transformedMedia.add(transformMp3(source, fileName), MediaFileType.MP3, MP3_MEDIA_TYPE);
				generateSpectrogram(source, fileName, transformedMedia);
				return transformedMedia;
			}
			if (originalMediaType == FLAC_MEDIA_TYPE) {
				transformedMedia.add(transformFlac(source, fileName), MediaFileType.FLAC, FLAC_MEDIA_TYPE);
				transformedMedia.add(transformMp3(source, fileName), MediaFileType.MP3, MP3_MEDIA_TYPE);
				generateSpectrogram(source, fileName, transformedMedia);
				return transformedMedia;
			}
			throw new UnsupportedOperationException("Not yet implemented for " + originalMediaType);
		} catch (Exception e) {
			throw new RuntimeException("Audio transform failed " + source.getName(), e);
		}
	}

	private File transformWav(File source, String fileName) throws IOException {
		File transformed = getFile(fileName, ".wav");
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/sox", getFilePath(source), getFilePath(transformed));
		if (!result.success()) throw new RuntimeException("sox failed " + result.getOutput());
		return transformed;
	}

	private File transformMp3(File source, String fileName) throws IOException {
		File transformed = getFile(fileName, ".mp3");
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/sox", getFilePath(source), getFilePath(transformed));
		if (!result.success()) throw new RuntimeException("sox failed " + result.getOutput());
		return transformed;
	}

	private File transformFlac(File source, String fileName) throws IOException {
		File transformed = getFile(fileName, ".flac");
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/sox", getFilePath(source), getFilePath(transformed));
		if (!result.success()) throw new RuntimeException("sox failed " + result.getOutput());
		return transformed;
	}

	private void generateSpectrogram(File source, String fileName, TransformedMedia transformedMedia) {
		try {
			File spectrogram = generateSpectrogram(source, fileName);
			transformedMedia.add(spectrogram, MediaFileType.FULL_IMAGE, PNG_MEDIA_TYPE);
			transformedMedia.add(thumbnail(spectrogram), MediaFileType.THUMBNAIL_IMAGE, JPEG_MEDIA_TYPE);
		} catch (Exception e) {
			throw new RuntimeException("Spectrogram transform failed " + source.getName(), e);
		}
	}

	private File generateSpectrogram(File transformedOriginal, String fileName) throws IOException {
		File spectrogram = getFile(fileName, "_spectrogram.png");
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/sox", getFilePath(transformedOriginal), "-n", "spectrogram", "-o", getFilePath(spectrogram));
		if (!result.success()) throw new RuntimeException("sox failed " + result.getOutput());
		return spectrogram;
	}

	private File getFile(String fileName, String extension) throws IOException {
		return getNewFile(FilenameUtils.removeExtension(fileName) + extension);
	}

	private MediaType getAudioType(File file) {
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/file", "-b", "--mime-type", getFilePath(file));
		if (!result.success()) return null;
		for (String line : result.getOutput().split(Pattern.quote("\n"))) {
			MediaType type = ALLOWED_MEDIA_TYPES.get(line);
			if (type != null) return type;
		}
		return null;
	}

	@Override
	protected long maxSize() {
		return MAX_FILE_SIZE;
	}

}
