package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import fi.luomus.kuvapalvelu.model.Media;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NewMedia;
import fi.luomus.kuvapalvelu.model.NewMultiSourceMedia;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.model.Urls;
import fi.luomus.kuvapalvelu.repository.MediaFileRepository;
import fi.luomus.kuvapalvelu.repository.MediaMetaDataRepository;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.util.Hasher;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class MediaService {

	private static final String MP3 = "mp3";
	private static final String WAV = "wav";
	private static final String FLAC = "flac";
	private static final String PDF = "pdf";

	@Inject
	private TempFileRepository tempFileRepository;
	@Inject
	private ImageTransformer imageTransformer;
	@Inject
	private AudioTransformer audioTransformer;
	@Inject
	private VideoTransformer videoTransformer;
	@Inject
	private LowDetailModelTransformer lowDetailModelTransformer;
	@Inject
	private HighDetailModelTransformer highDetailModelTransformer;
	@Inject
	private PdfTransformer pdfTransformer;
	@Inject
	private MediaMetaDataRepository mediaMetaDataRepository;
	@Inject
	private MediaFileRepository mediaFileRepository;
	@Inject
	CachedEnumsService enumsService;
	@Inject
	private LuomusPreferences preferences;

	public static class CanTransform {
		private final boolean canTransform;
		private final String whyNot;
		private CanTransform(boolean canTransform, String whyNot) {
			this.canTransform = canTransform;
			this.whyNot = whyNot;
		}
		public boolean isTransformable() {
			return canTransform;
		}
		public String getError() {
			return whyNot;
		}
		public static CanTransform yes() {
			return new CanTransform(true, null);
		}
		public static CanTransform no(String whyNot) {
			return new CanTransform(false, whyNot);
		}
	}

	public CanTransform check(File file, MediaClass mediaClass, MediaFileType mediaType) {
		if (mediaType != null) {
			if (mediaType.getMediaClass() == MediaClass.IMAGE) {
				return imageTransformer.check(file);
			}
			if (mediaType.getMediaClass() == MediaClass.AUDIO) {
				return audioTransformer.check(file);
			}
			if (mediaType == MediaFileType.VIDEO) {
				return videoTransformer.check(file);
			}
			if (mediaType == MediaFileType.LOW_DETAIL_MODEL) {
				return lowDetailModelTransformer.check(file);
			}
			if (mediaType == MediaFileType.HIGH_DETAIL_MODEL) {
				return highDetailModelTransformer.check(file);
			}
			if (mediaType == MediaFileType.PDF) {
				return pdfTransformer.check(file);
			}
		}
		if (mediaClass == null) mediaClass = tryToResolveMediaClass(file);
		if (mediaClass == MediaClass.IMAGE) {
			return imageTransformer.check(file);
		}
		if (mediaClass == MediaClass.AUDIO) {
			return audioTransformer.check(file);
		}
		if (mediaClass == MediaClass.PDF) {
			return pdfTransformer.check(file);
		}
		return CanTransform.no("Not supported media type " + mediaClass);
	}

	private MediaClass tryToResolveMediaClass(File file) {
		String extension = getExtension(file);
		if (audio(extension)) return MediaClass.AUDIO;
		if (pdf(extension)) return MediaClass.PDF;
		return MediaClass.IMAGE;
	}

	private boolean audio(String extension) {
		return WAV.equals(extension) || MP3.equals(extension) || FLAC.equals(extension);
	}

	private boolean pdf(String extension) {
		return PDF.equals(extension);
	}

	private String getExtension(File file) {
		String filenameExtension = FilenameUtils.getExtension(file.getName());
		if (filenameExtension == null) return "";
		return filenameExtension.toLowerCase();
	}

	/**
	 * Create the given media
	 *
	 * @param multiSource An object containing metadata, media type and one or more temporary file ids and their media types
	 * @return The created media
	 * @throws ApiException
	 */
	public Media create(NewMedia media) throws ApiException {
		validate(media.getMeta());
		try (TransformedMedia transformedMedia = transform(media)) {
			String mediaId = mediaMetaDataRepository.newId();
			Meta meta = media.getMeta();
			meta.setUploadDateTime(new DateTime());
			Urls urls = uploadTransformed(mediaId, transformedMedia, meta.isSecret());
			try {
				mediaMetaDataRepository.save(mediaId, meta, urls, media.getMediaClass());
			} catch (Exception e) {
				try {
					mediaFileRepository.delete(mediaId);
				} catch (NotFoundException e1) {
					// nop
				}
				throw e;
			}
			return get(mediaId).get();
		} catch (ApiException apie) {
			throw new RuntimeException("Communication with API failed when sending " + media +". Cause:" + apie.getMessage(), apie);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Creating media failed for " + media, e);
		}
	}

	/**
	 * Create the given media
	 *
	 * @param multiSource An object containing metadata, media type and one or more temporary file ids and their media types
	 * @return The created media
	 * @throws ApiException
	 */
	public Media create(NewMultiSourceMedia multiSource) throws ApiException {
		validate(multiSource.getMeta());
		try (TransformedMedia transformedMedia = transform(multiSource)) {
			String mediaId = mediaMetaDataRepository.newId();
			Meta meta = multiSource.getMeta();
			meta.setUploadDateTime(new DateTime());
			Urls urls = uploadTransformed(mediaId, transformedMedia, meta.isSecret());
			try {
				mediaMetaDataRepository.save(mediaId, meta, urls, multiSource.getMediaClass());
			} catch (Exception e) {
				try {
					mediaFileRepository.delete(mediaId);
				} catch (NotFoundException delexp) {}
				throw e;
			}
			return get(mediaId).get();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private TransformedMedia transform(NewMedia media) {
		try {
			File file = tempFileRepository.getHandle(media.getTempFileId()).get();
			Meta meta = media.getMeta();
			updateMetaData(meta, file);
			return transform(file, meta.getOriginalFilename(), media.getMediaClass());
		} catch (IOException e) {
			throw new RuntimeException("error while transforming images", e);
		}
	}

	private TransformedMedia transform(NewMultiSourceMedia multiSource) {
		try {
			TransformedMedia transformed = new TransformedMedia();
			for (NewMultiSourceMedia.MediaFile mediaFile : multiSource) {
				File file = tempFileRepository.getHandle(mediaFile.getTempFileId()).get();
				String fileName = mediaFile.getFilename() != null ? mediaFile.getFilename() : NameCleaner.clean(file.getName());
				Meta meta = multiSource.getMeta();
				updateMetaData(meta, file);
				addTransformations(transformed, mediaFile, file, fileName);
			}
			return transformed;
		} catch (IOException e) {
			throw new RuntimeException("error while transforming images", e);
		}
	}

	private TransformedMedia addTransformations(TransformedMedia transformed, NewMultiSourceMedia.MediaFile mediaFile, File file, String filename) {
		if (mediaFile.getType().getMediaClass() == MediaClass.IMAGE) {
			return transformed.addCombining(imageTransformer.transform(file, filename), mediaFile.getType());
		}
		if (mediaFile.getType().getMediaClass() == MediaClass.AUDIO) {
			return transformed.addCombining(audioTransformer.transform(file, filename), mediaFile.getType());
		}
		if (mediaFile.getType() == MediaFileType.VIDEO) {
			return transformed.addCombining(videoTransformer.transform(file, filename), mediaFile.getType());
		}
		if (mediaFile.getType() == MediaFileType.LOW_DETAIL_MODEL) {
			return transformed.addCombining(lowDetailModelTransformer.transform(file, filename), mediaFile.getType());
		}
		if (mediaFile.getType() == MediaFileType.HIGH_DETAIL_MODEL) {
			return transformed.addCombining(highDetailModelTransformer.transform(file, filename), mediaFile.getType());
		}
		if (mediaFile.getType() == MediaFileType.PDF) {
			return transformed.addCombining(pdfTransformer.transform(file, filename), mediaFile.getType());
		}
		throw new UnsupportedOperationException("Not yet implemented for " + mediaFile.getType());
	}

	private TransformedMedia transform(File file, String originalFilename, MediaClass mediaClass) {
		if (mediaClass == MediaClass.IMAGE) {
			return imageTransformer.transform(file, originalFilename);
		}
		if (mediaClass == MediaClass.AUDIO) {
			return audioTransformer.transform(file, originalFilename);
		}
		if (mediaClass == MediaClass.PDF) {
			return pdfTransformer.transform(file, originalFilename);
		}
		throw new UnsupportedOperationException("Not yet implemented for " + mediaClass);
	}

	private void updateMetaData(Meta meta, File file) {
		// if the metadata doesn't specify a new original filename lets use the filename of the temporary file upload
		if (meta.getOriginalFilename() == null || meta.getOriginalFilename().isEmpty()) {
			meta.setOriginalFilename(file.getName());
		}
	}

	/**
	 * Update the metadata for the given media id
	 *
	 * @param newMeta The new metadata
	 * @param id      The id of the media
	 * @throws ApiException An error occurred while communicating with an external API
	 */
	public void updateMeta(Meta newMeta, String id, MediaClass mediaClass) throws ApiException {
		newMeta.setModifiedDateTime(new DateTime());
		validate(newMeta);
		MediaMetaDataRepository.MetaAndUrls current = mediaMetaDataRepository.get(id).get();
		mediaMetaDataRepository.save(id, newMeta, current.urls, mediaClass);
		// update only if it has changed
		if (current.meta.isSecret() != newMeta.isSecret()) {
			mediaFileRepository.update(id, newMeta.isSecret());
		}
	}

	/**
	 * Get the media
	 *
	 * @param id The identifier of the media
	 * @return An optional containing the media if exists, absent otherwise
	 * @throws ApiException An error occurred while communicating with an external API
	 */
	public Optional<Media> get(String id) throws ApiException {
		Optional<MediaMetaDataRepository.MetaAndUrls> optional = mediaMetaDataRepository.get(id);
		if (!optional.isPresent()) {
			return Optional.absent();
		}
		return Optional.of(produce(optional.get(), id));
	}

	/**
	 * Search for medias with the given parameters
	 *
	 * @param searchKey The name of the field to be used for the search
	 * @param values    Query values (OR semantics)
	 * @param limit     The maximum amount of results
	 * @param offset    The offset of results
	 * @param mediaClass      Expected media type
	 * @return All matching medias
	 * @throws MediaMetaDataRepository.InvalidParameterException Thrown if the searchKey parameter is invalid
	 * @throws ApiException                                      An error occurred while communicating with an external API
	 */
	public List<Media> search(String searchKey, List<String> values, int limit, int offset, MediaClass mediaClass)
			throws MediaMetaDataRepository.InvalidParameterException, ApiException {
		List<Media> result = new ArrayList<>();
		List<MediaMetaDataRepository.WithId> search =
				this.mediaMetaDataRepository.search(searchKey, values, limit, offset, mediaClass);
		for (MediaMetaDataRepository.WithId entry : search) {
			Media produce = produce(entry.metaAndUrls, entry.id);
			result.add(produce);
		}

		return result;
	}

	private Media produce(MediaMetaDataRepository.MetaAndUrls metaAndUrls, String id) {
		Media result = new Media();
		result.setUrls(metaAndUrls.urls);
		result.setMeta(metaAndUrls.meta.doLegacyConversions());
		result.setId(id);
		if (result.getMeta().isSecret()) {
			result.setSecretKey(secret(id));
		}

		return result;
	}

	private Urls uploadTransformed(String id, TransformedMedia transformedImages, boolean secret) throws ApiException {
		Map<MediaFileType, URI> upload = mediaFileRepository.upload(id, transformedImages, secret);

		Urls urls = new Urls();
		urls.setOriginal(upload.get(MediaFileType.ORIGINAL_IMAGE));
		urls.setFull(upload.get(MediaFileType.FULL_IMAGE));
		urls.setLarge(upload.get(MediaFileType.LARGE_IMAGE));
		urls.setSquare(upload.get(MediaFileType.SQUARE_THUMBNAIL_IMAGE));
		urls.setThumbnail(upload.get(MediaFileType.THUMBNAIL_IMAGE));
		urls.setWav(upload.get(MediaFileType.WAV));
		urls.setMp3(upload.get(MediaFileType.MP3));
		urls.setFlac(upload.get(MediaFileType.FLAC));
		urls.setVideo(upload.get(MediaFileType.VIDEO));
		urls.setLowDetailModel(upload.get(MediaFileType.LOW_DETAIL_MODEL));
		urls.setHighDetailModel(upload.get(MediaFileType.HIGH_DETAIL_MODEL));
		urls.setPdf(upload.get(MediaFileType.PDF));
		return urls;
	}

	private String secret(String id) {
		String secret = preferences.get(MediaApiPreferences.IMAGE_REPOSITORY_SECRET).get();
		return Hasher.hash(id + ";" + secret);
	}


	private void validate(Meta meta) throws ApiException {
		try {
			validate(meta.getLicense(), enumsService.getLicenses(), "license");
			validate(meta.getLifeStage(), enumsService.getLifeStages(), "lifeStage");
			validate(meta.getPlantLifeStage(), enumsService.getPlantLifeStages(), "plantLifeStage");
			validate(meta.getSex(), enumsService.getSexes(), "sex");
			validate(meta.getSide(), enumsService.getSides(), "side");
			validate(meta.getType(), enumsService.getTypes(), "type");
		} catch (ExecutionException ex) {
			handle(ex);
		}
	}

	private void validate(String val, Set<String> allowed, String resourceName) {
		if (val == null) return;
		if (!allowed.contains(val)) throw new IllegalArgumentException("Unknown " + resourceName + ": " + val);
	}

	private void validate(List<String> vals, Set<String> allowed, String resourceName) {
		if (vals == null) return;
		for (String val : vals) {
			validate(val, allowed, resourceName);
		}
	}

	private void handle(ExecutionException ex) throws ApiException {
		Throwable cause = ex.getCause();
		while (cause != null) {
			if (cause instanceof ApiException) {
				throw (ApiException) cause; // Rethrow the original ApiException
			}
			cause = cause.getCause(); // Move to the next cause
		}
		throw ApiException.builder().cause(ex).build();
	}

}
