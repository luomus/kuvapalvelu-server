package fi.luomus.kuvapalvelu.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class HighDetailModelTransformer extends AbstractModelTransformer {

	private static final long MAX_FILE_SIZE = 500 * 1024 * 1024; // 500 MB

	@Inject
	public HighDetailModelTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected MediaFileType mediaFileType() {
		return MediaFileType.HIGH_DETAIL_MODEL;
	}

	@Override
	protected long maxSize() {
		return MAX_FILE_SIZE;
	}

}
