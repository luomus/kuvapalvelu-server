package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil.CommandExecutionResult;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class VideoTransformer extends AbstractTransformer {

	private static final long MAX_FILE_SIZE = 30 * 1024 * 1024; // 30MB

	private static final MediaType MP4 = new MediaType("video", "mp4");

	private static List<String> ALLOWED_EXTENSIONS;
	static {
		ALLOWED_EXTENSIONS = new ArrayList<>();
		ALLOWED_EXTENSIONS.add("mp4");
	}

	private static final Map<String, MediaType> ALLOWED_MEDIA_TYPES;
	static {
		ALLOWED_MEDIA_TYPES = new HashMap<>();
		ALLOWED_MEDIA_TYPES.put("video/mp4", MP4);
	}

	@Inject
	public VideoTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected CanTransform canTransformActual(File source) {
		try {
			if (source.length() > MAX_FILE_SIZE) {
				return CanTransform.no("Too large: " + source.length() + " > " + MAX_FILE_SIZE) ;
			}
			String ext = FilenameUtils.getExtension(source.getName()).toLowerCase();
			if (!ALLOWED_EXTENSIONS.contains(ext)) {
				return CanTransform.no("Not valid extension: " + ext + " <> " + ALLOWED_EXTENSIONS);
			}
			MediaType type = getMediaType(source);
			if (type == null) return CanTransform.no("Unknown media type");
			return CanTransform.yes();
		} catch (Exception e) {
			return CanTransform.no("Exception: " + e.getMessage());
		}
	}

	@Override
	protected TransformedMedia transformActual(File source, String fileName) {
		try {
			TransformedMedia transformed = new TransformedMedia();
			File copy = getNewFile(source.getName());
			FileUtils.copyFile(source, copy);
			MediaType mediaType = getMediaType(copy);
			if (mediaType == null) throw new IllegalStateException("Uknown media type (despite validation)");
			transformed.add(copy, MediaFileType.VIDEO, mediaType);
			return transformed;
		} catch (Exception e) {
			throw new RuntimeException("Video transform failed " + source.getName() + "; " + e.getMessage(), e);
		}
	}

	private MediaType getMediaType(File file) {
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/file", "-b", "--mime-type", getFilePath(file));
		if (!result.success()) return null;
		for (String line : result.getOutput().split(Pattern.quote("\n"))) {
			MediaType type = ALLOWED_MEDIA_TYPES.get(line);
			if (type != null) return type;
		}
		return null;
	}

	@Override
	protected long maxSize() {
		return MAX_FILE_SIZE;
	}

}
