package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil.CommandExecutionResult;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public abstract class AbstractModelTransformer extends AbstractTransformer {

	private static final MediaType STL = new MediaType("application", "octet-stream");
	private static final MediaType GLB = new MediaType("model", "gltf-binary");

	private static List<String> ALLOWED_EXTENSIONS;
	static {
		ALLOWED_EXTENSIONS = new ArrayList<>();
		ALLOWED_EXTENSIONS.add("stl");
		ALLOWED_EXTENSIONS.add("glb");
	}

	private static final Map<String, List<MediaType>> ALLOWED_MEDIA_TYPES;
	static {
		ALLOWED_MEDIA_TYPES = new HashMap<>();
		ALLOWED_MEDIA_TYPES.put("application/octet-stream",  Arrays.asList(STL, GLB));
		ALLOWED_MEDIA_TYPES.put("model/gltf-binary", Arrays.asList(GLB));
	}

	protected abstract MediaFileType mediaFileType();

	public AbstractModelTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected CanTransform canTransformActual(File source) {
		try {
			String ext = extension(source);
			if (!ALLOWED_EXTENSIONS.contains(ext)) {
				return CanTransform.no("Not valid extension: " + ext + " <> " + ALLOWED_EXTENSIONS);
			}
			MediaType type = getMediaType(source);
			if (type == null) return CanTransform.no("Unknown media type");
			return CanTransform.yes();
		} catch (Exception e) {
			return CanTransform.no("Exception: " + e.getMessage());
		}
	}

	private String extension(File source) {
		return FilenameUtils.getExtension(source.getName()).toLowerCase();
	}

	@Override
	protected TransformedMedia transformActual(File source, String fileName) {
		try {
			TransformedMedia transformed = new TransformedMedia();
			File copy = getNewFile(source.getName());
			FileUtils.copyFile(source, copy);
			MediaType mediaType = getMediaType(copy);
			if (mediaType == null) throw new IllegalStateException("Uknown media type (despite validation)");
			transformed.add(copy, mediaFileType(), mediaType);
			return transformed;
		} catch (Exception e) {
			throw new RuntimeException("Model transform failed " + source.getName() + "; " + e.getMessage(), e);
		}
	}

	private MediaType getMediaType(File file) {
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/file", "-b", "--mime-type", getFilePath(file));
		if (!result.success()) return null;
		for (String line : result.getOutput().split(Pattern.quote("\n"))) {
			List<MediaType> types = ALLOWED_MEDIA_TYPES.get(line);
			if (types == null) continue;
			if (types.size() == 1) return types.get(0);
			String ext = extension(file); // TODO should use some other program than file to determine type, for example gltf-viewer...?
			if ("stl".equals(ext)) return STL;
			if ("glb".equals(ext)) return GLB;
		}
		return null;
	}

}
