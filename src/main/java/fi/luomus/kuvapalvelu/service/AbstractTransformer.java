package fi.luomus.kuvapalvelu.service;

import static fi.luomus.kuvapalvelu.util.GenericUtils.notNull;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FilenameUtils;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.IMOps;
import org.im4java.core.Info;
import org.im4java.core.InfoException;

import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.preferences.LuomusPreferences;

public abstract class AbstractTransformer implements Transformer {

	protected static final MediaType JPEG_MEDIA_TYPE = new MediaType("image", "jpeg");
	protected static final MediaType PNG_MEDIA_TYPE = new MediaType("image", "png");
	protected static final String DEFAULT_FORMAT = "JPEG";
	protected static final int LARGE_DIMENSION = 1280;
	protected static final int THUMB_DIMENSION = 200;
	protected static final int SQUARE_DIMENSION = 400;

	protected abstract long maxSize();
	protected abstract CanTransform canTransformActual(File source);
	protected abstract TransformedMedia transformActual(File source, String fileName);

	private final LuomusPreferences preferences;
	private final TempFileRepository tempFileRepository;
	private Double imageQuality = null;

	public AbstractTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		this.preferences = preferences;
		this.tempFileRepository = tempFileRepository;
	}

	private double getImageQuality() {
		if (imageQuality == null) {
			imageQuality = (double) preferences.getFloat(MediaApiPreferences.IMAGE_QUALITY).get();
		}
		return imageQuality;
	}

	@Override
	public CanTransform check(File source) {
		notNull(source);
		isExistingFile(source);
		if (source.length() > maxSize()) {
			return CanTransform.no("Too large: " + source.length() + " > " + maxSize()) ;
		}
		return canTransformActual(source);
	}

	@Override
	public TransformedMedia transform(File source, String fileName) {
		notNull(source, fileName);
		isExistingFile(source);
		fileName = NameCleaner.clean(fileName);
		return transformActual(source, fileName);
	}

	private void isExistingFile(File source) {
		if (!source.exists() || !source.isFile()) {
			try {
				throw new IllegalArgumentException("invalid File object " + source.getCanonicalPath());
			} catch (IOException e) {
				throw new IllegalArgumentException("invalid File object " + source);
			}
		}
	}

	protected File thumbnail(File source) throws IOException, InfoException {
		return scaleToMaxDimension(source, THUMB_DIMENSION, "thumb");
	}

	protected File scaleToMaxDimension(File source, int maxDimension, String suffix) throws IOException, InfoException {
		IMOps operations = new IMOperation();
		operations.addImage();
		Info info = new Info(source.getCanonicalPath(), true);
		int biggestDimension = Math.max(info.getImageWidth(), info.getImageHeight());

		boolean scaled = false;
		if (biggestDimension > maxDimension) {
			// needs scaling so let's just do that
			operations = scaleToDimension(operations, info, maxDimension);
			scaled = true;
		}

		if (scaled || !info.getImageFormat().equals(DEFAULT_FORMAT)) {
			return transformAsJpg(source, operations, suffix);
		}
		// image wasn't scaled and its in desired format, no need to transform
		return source;

	}

	private IMOps scaleToDimension(IMOps operations, Info info, int maxDimension) throws InfoException {
		int width = info.getImageWidth();
		int height = info.getImageHeight();
		return scaleToDimension(operations, width, height, maxDimension);
	}

	protected IMOps scaleToDimension(IMOps operations, int width, int height, int maxDimension) {
		boolean isWider = width > height;
		double ratio = new Double(width) / new Double(height);
		if (isWider) {
			return operations.scale(maxDimension, new Double(maxDimension / ratio).intValue());
		}
		return operations.scale(new Double(maxDimension * ratio).intValue(), maxDimension);
	}

	protected File transformAsJpg(File source, IMOps operations, String suffix) throws IOException {
		operationsCustomizationHook(operations);
		operations.quality(getImageQuality());
		operations.format(DEFAULT_FORMAT);
		operations.strip();
		String filename = FilenameUtils.getName(source.getName());
		filename = FilenameUtils.removeExtension(filename) + "_" + suffix + ".jpg";
		return transform(source, operations, filename);
	}

	protected void operationsCustomizationHook(@SuppressWarnings("unused") IMOps operations) {
		// override in extending implementations as needed
	}

	protected File transform(File source, IMOps operations, String newFileName) throws IOException {
		return transform(source, operations, getNewFile(newFileName));
	}

	protected File getNewFile(String newFileName) throws IOException {
		newFileName = NameCleaner.clean(newFileName);
		return tempFileRepository.getTempFile(newFileName);
	}

	private File transform(File file, IMOps operations, File newFile) throws IOException {
		operations.addImage();
		ConvertCmd convertCmd = new ConvertCmd();
		try {
			convertCmd.run(operations, fileCommandFileNameCustomizationHook(file), newFile.getCanonicalPath());
		} catch (InterruptedException e) {
			throw new RuntimeException("error during saving converted image", e);
		} catch (IM4JavaException e) {
			throw new RuntimeException("error during saving converted image", e);
		}
		if (!newFile.exists()) throw new IOException("Tried to transform " + fileCommandFileNameCustomizationHook(file) + " to " + newFile.getAbsolutePath() + " but after transformation target file does not exist.");
		return newFile;
	}

	protected String fileCommandFileNameCustomizationHook(File file) throws IOException {
		return file.getCanonicalPath();
	}

	protected String getFilePath(File original) {
		return FilenameUtils.normalize(original.getAbsolutePath());
	}

}
