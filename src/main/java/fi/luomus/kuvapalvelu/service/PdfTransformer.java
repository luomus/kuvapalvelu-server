package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;
import org.im4java.core.IMOps;
import org.im4java.core.Info;
import org.im4java.core.InfoException;
import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class PdfTransformer extends ImageTransformer {

	private static final MediaType PDF_MEDIA_TYPE = new MediaType("application", "pdf");

	private static final long MAX_FILE_SIZE = 50 * 1024 * 1024; // 50MB

	@Inject
	public PdfTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected CanTransform canTransformActual(File source) {
		try {
			if (isPdf(source)) {
				return CanTransform.yes();
			}
			return CanTransform.no("Not a PDF");
		} catch (Exception e) {
			return CanTransform.no("Invalid pdf: " + e.getMessage());
		}
	}

	private boolean isPdf(File file) throws Exception {
		try {
			String format = new Info(file.getCanonicalPath(), true).getImageFormat();
			return format.toLowerCase().equals("pdf");
		} catch (InfoException | IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	protected TransformedMedia transformActual(File source, String fileName) {
		TransformedMedia transformedImages = new TransformedMedia();
		try {
			File copy = getNewFile(source.getName());
			FileUtils.copyFile(source, copy);
			transformedImages.add(copy, MediaFileType.ORIGINAL_IMAGE, PDF_MEDIA_TYPE);
			transformedImages.add(copy, MediaFileType.PDF, PDF_MEDIA_TYPE);
			File full = full(source);
			transformedImages.add(full, MediaFileType.FULL_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(large(full), MediaFileType.LARGE_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(thumbnail(full), MediaFileType.THUMBNAIL_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(square(full), MediaFileType.SQUARE_THUMBNAIL_IMAGE, JPEG_MEDIA_TYPE);
		} catch (Exception e) {
			throw new RuntimeException("PDF thumbnail image transform failed " + source.getName() + "; " + e.getMessage(), e);
		}
		return transformedImages;
	}

	@Override
	protected void operationsCustomizationHook(IMOps operations) {
		operations.density(150);
	}

	@Override
	protected String fileCommandFileNameCustomizationHook(File file) throws IOException {
		return super.fileCommandFileNameCustomizationHook(file)+"[0]";
	}

	@Override
	public long maxSize() {
		return MAX_FILE_SIZE;
	}

}
