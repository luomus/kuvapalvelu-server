package fi.luomus.kuvapalvelu.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.im4java.core.IMOperation;
import org.im4java.core.IMOps;
import org.im4java.core.Info;
import org.im4java.core.InfoException;
import org.springframework.stereotype.Service;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.repository.TempFileRepository;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil;
import fi.luomus.kuvapalvelu.util.CommandExecutorUtil.CommandExecutionResult;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class ImageTransformer extends AbstractTransformer {

	private static final long MAX_FILE_SIZE = 500 * 1024 * 1024; // 500 MB
	private static final String ICC_PROFILE = "ICC_Profile";

	@Inject
	public ImageTransformer(LuomusPreferences preferences, TempFileRepository tempFileRepository) {
		super(preferences, tempFileRepository);
	}

	@Override
	protected CanTransform canTransformActual(File source) {
		try {
			getImageType(source);
			return CanTransform.yes();
		} catch (Exception e) {
			return CanTransform.no("Invalid image: " + e.getMessage());
		}
	}

	private MediaType getImageType(File file) {
		try {
			String imageFormat = new Info(file.getCanonicalPath(), true).getImageFormat();
			return new MediaType("image", imageFormat.toLowerCase());
		} catch (InfoException | IOException e) {
			e.printStackTrace();
			throw new RuntimeException("unable to retrieve image type", e);
		}
	}

	@Override
	protected TransformedMedia transformActual(File source, String fileName) {
		TransformedMedia transformedImages = new TransformedMedia();
		try {
			File original = original(source, fileName);
			transformedImages.add(original, MediaFileType.ORIGINAL_IMAGE, getImageType(original));
			transformedImages.add(full(original), MediaFileType.FULL_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(large(original), MediaFileType.LARGE_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(thumbnail(original), MediaFileType.THUMBNAIL_IMAGE, JPEG_MEDIA_TYPE);
			transformedImages.add(square(original), MediaFileType.SQUARE_THUMBNAIL_IMAGE, JPEG_MEDIA_TYPE);
		} catch (Exception e) {
			throw new RuntimeException("Image transform failed " + source.getName() + "; " + e.getMessage(), e);
		}
		return transformedImages;
	}

	private File original(File source, String fileName) throws IOException {
		IMOps operations = new IMOperation();
		operations.addImage();
		operations.flatten();
		operations.strip();
		operations.autoOrient();
		File original = transform(source, operations, fileName);
		setLimitedExif(source, original);
		return original;
	}

	private void setLimitedExif(File source, File flattenedOriginal) {
		setExifData(flattenedOriginal, getLimitedExif(source));
	}

	private void setExifData(File file, List<String> exifData) {
		CommandExecutionResult result = setExifActual(file, exifData, true);
		if (!result.success()) {
			result = setExifActual(file, exifData, false);
			if (!result.success()) {
				throw new RuntimeException("exiftool failed for writing " + file.getName() + " " + result.getOutput());
			}
		}
	}

	private CommandExecutionResult setExifActual(File file, List<String> exifData, boolean withIccProfile) {
		List<String> commands = new ArrayList<>();
		commands.add("/usr/bin/exiftool");
		commands.add("-overwrite_original");
		if (!withIccProfile) {
			exifData.remove(ICC_PROFILE);
			commands.add("-icc_profile=");
		}
		commands.addAll(exifData);
		commands.add(getFilePath(file));
		return CommandExecutorUtil.execute(commands);
	}

	private List<String> getLimitedExif(File source) {
		CommandExecutionResult result = CommandExecutorUtil.execute("/usr/bin/exiftool", "-args", getFilePath(source));
		if (!result.success()) throw new RuntimeException("exiftool failed for reading " + source.getName() + " " + result.getOutput());
		List<String> data = new ArrayList<>();
		for (String line : result.getOutputLines()) {
			String arg = getArg(line);
			if (allowed(arg)) {
				data.add(line);
			}
		}
		return data;
	}

	private boolean allowed(String arg) {
		return ALLOWED_EXIF_TAGS.contains(arg);
	}

	private String getArg(String line) {
		if (!line.contains("=")) return "";
		return line.split(Pattern.quote("="))[0].replace("-", "").trim();
	}

	protected File large(File source) throws IOException, InfoException {
		return scaleToMaxDimension(source, LARGE_DIMENSION, "large");
	}

	protected File square(File source) throws IOException, InfoException {
		IMOps operations = new IMOperation();
		operations.addImage();
		Info info = new Info(source.getCanonicalPath(), true);

		// square the image
		boolean isWider = info.getImageWidth() > info.getImageHeight();
		int offset;
		int x, y;
		int newDimension;
		if (isWider) {
			offset = info.getImageWidth() - info.getImageHeight();

			newDimension = info.getImageHeight();
			x = offset / 2;
			y = 0;
		} else {
			offset = info.getImageHeight() - info.getImageWidth();
			newDimension = info.getImageWidth();
			x = 0;
			y = offset / 2;
		}
		operations = operations.crop(newDimension, newDimension, x, y);
		if (newDimension > SQUARE_DIMENSION) {
			operations = scaleToDimension(operations, newDimension, newDimension, SQUARE_DIMENSION);
		}
		return transformAsJpg(source, operations, "square");
	}

	protected File full(File source) throws IOException, InfoException {
		Info info = new Info(source.getCanonicalPath(), true);
		if (info.getImageFormat().equals(DEFAULT_FORMAT)) {
			return source;
		}
		IMOperation operation = new IMOperation();
		operation.addImage();
		return transformAsJpg(source, operation, "full");
	}

	private static final Set<String> ALLOWED_EXIF_TAGS;
	static {
		ALLOWED_EXIF_TAGS = new HashSet<>();
		ALLOWED_EXIF_TAGS.add("ApertureValue");
		ALLOWED_EXIF_TAGS.add("BrightnessValue");
		ALLOWED_EXIF_TAGS.add("CFAPattern");
		ALLOWED_EXIF_TAGS.add("ColorSpace");
		ALLOWED_EXIF_TAGS.add("Contrast");
		ALLOWED_EXIF_TAGS.add("CustomRender");
		ALLOWED_EXIF_TAGS.add("DigitalZoomRatio");
		ALLOWED_EXIF_TAGS.add("ExifImageHeight");
		ALLOWED_EXIF_TAGS.add("ExifImageWidth");
		ALLOWED_EXIF_TAGS.add("ExifOffset");
		ALLOWED_EXIF_TAGS.add("ExifVersion");
		ALLOWED_EXIF_TAGS.add("ExposureBiasValue");
		ALLOWED_EXIF_TAGS.add("ExposureCompensation");
		ALLOWED_EXIF_TAGS.add("ExposureMode");
		ALLOWED_EXIF_TAGS.add("ExposureProgram");
		ALLOWED_EXIF_TAGS.add("ExposureTime");
		ALLOWED_EXIF_TAGS.add("FileSource");
		ALLOWED_EXIF_TAGS.add("Flash");
		ALLOWED_EXIF_TAGS.add("FlashPixVersion");
		ALLOWED_EXIF_TAGS.add("FNumber");
		ALLOWED_EXIF_TAGS.add("FNumber");
		ALLOWED_EXIF_TAGS.add("FocalLength");
		ALLOWED_EXIF_TAGS.add("FocalLengthIn35mmFormat");
		ALLOWED_EXIF_TAGS.add("FocalPlaneResolutionUnit");
		ALLOWED_EXIF_TAGS.add("FocalPlaneXResolution");
		ALLOWED_EXIF_TAGS.add("FocalPlaneYResolution");
		ALLOWED_EXIF_TAGS.add("GainControl");
		ALLOWED_EXIF_TAGS.add("GlobalAltitude");
		ALLOWED_EXIF_TAGS.add("GlobalAngle");
		ALLOWED_EXIF_TAGS.add(ICC_PROFILE);
		ALLOWED_EXIF_TAGS.add("ISO");
		ALLOWED_EXIF_TAGS.add("ISOSpeed");
		ALLOWED_EXIF_TAGS.add("ISOSpeedRatings");
		ALLOWED_EXIF_TAGS.add("LensInfo");
		ALLOWED_EXIF_TAGS.add("LensModel");
		ALLOWED_EXIF_TAGS.add("LightSource");
		ALLOWED_EXIF_TAGS.add("Make");
		ALLOWED_EXIF_TAGS.add("MakeModel");
		ALLOWED_EXIF_TAGS.add("MaxApertureValue");
		ALLOWED_EXIF_TAGS.add("MeteringMode");
		ALLOWED_EXIF_TAGS.add("Model");
		ALLOWED_EXIF_TAGS.add("ResolutionUnit");
		ALLOWED_EXIF_TAGS.add("Saturation");
		ALLOWED_EXIF_TAGS.add("SceneCaptureType");
		ALLOWED_EXIF_TAGS.add("SceneType");
		ALLOWED_EXIF_TAGS.add("SensingMethod");
		ALLOWED_EXIF_TAGS.add("SensitivityType");
		ALLOWED_EXIF_TAGS.add("Sharpness");
		ALLOWED_EXIF_TAGS.add("ShutterSpeedValue");
		ALLOWED_EXIF_TAGS.add("SubjectDistance");
		ALLOWED_EXIF_TAGS.add("SubjectDistanceRange");
		ALLOWED_EXIF_TAGS.add("SubSecTime");
		ALLOWED_EXIF_TAGS.add("SubSecTimeDigitized");
		ALLOWED_EXIF_TAGS.add("SubSecTimeOriginal");
		ALLOWED_EXIF_TAGS.add("WhiteBalance");
		ALLOWED_EXIF_TAGS.add("XResolution");
		ALLOWED_EXIF_TAGS.add("YCbCrPositioning");
		ALLOWED_EXIF_TAGS.add("YResolution");
	}

	@Override
	protected long maxSize() {
		return MAX_FILE_SIZE;
	}

}
