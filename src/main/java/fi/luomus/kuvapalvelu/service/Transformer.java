package fi.luomus.kuvapalvelu.service;

import java.io.File;

import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.service.MediaService.CanTransform;

public interface Transformer {

	/**
	 * Is the file in question processable?
	 * @param source The source file
	 * @return can be transformed and if not, why not
	 */
	CanTransform check(File source);

	/**
	 * Transform the file into other versions / generate additional files from the original / strip metadatas / etc
	 * @param source The source file
	 * @param fileName Desired file name prefix
	 * @return A map linking the newly transformed versions of the original file to file handlers
	 */
	TransformedMedia transform(File source, String fileName);

}
