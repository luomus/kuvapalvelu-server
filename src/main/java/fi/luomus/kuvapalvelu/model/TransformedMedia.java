package fi.luomus.kuvapalvelu.model;

import static fi.luomus.kuvapalvelu.util.GenericUtils.notNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import fi.luomus.kuvapalvelu.util.FileUtils;

/**
 * Container object for the handles for the original file and its
 * transformed versions, deletes transformed images upon closing
 */
public class TransformedMedia implements AutoCloseable {

	public static class UploadFile {
		private final File file;
		private final MediaFileType fileType;
		private final MediaType mediaType;
		public UploadFile(File file, MediaFileType fileType, MediaType mediaType) {
			this.file = file;
			this.fileType = fileType;
			this.mediaType = mediaType;
		}
		public File getFile() {
			return file;
		}
		public MediaFileType getFileType() {
			return fileType;
		}
		public MediaType getMediaType() {
			return mediaType;
		}
	}

	private final Map<MediaFileType, File> transformed = new LinkedHashMap<>();
	private final Map<File, MediaFileType> fileTypes = new HashMap<>();
	private final Map<File, MediaType> mediaTypes = new HashMap<>();
	private boolean closed = false;

	public TransformedMedia() {}

	public TransformedMedia add(File file, MediaFileType type, MediaType mediaType) {
		notNull(file, type, mediaType);
		checkNotClosed();
		checkForUniqueImageType(type);
		transformed.put(type, file);
		if (!fileTypes.containsKey(file)) {
			fileTypes.put(file, type);
		}
		if (!mediaTypes.containsKey(file)) {
			mediaTypes.put(file, mediaType);
		}
		return this;
	}

	private TransformedMedia addCombining(File file, MediaFileType type, MediaType mediaType, MediaFileType preferring) {
		notNull(file, type, mediaType);
		checkNotClosed();
		if (transformed.containsKey(type)) {
			if (type == preferring) {
				transformed.put(type, file);
			}
		} else {
			transformed.put(type, file);
		}
		if (!fileTypes.containsKey(file)) {
			fileTypes.put(file, type);
		}
		if (!mediaTypes.containsKey(file)) {
			mediaTypes.put(file, mediaType);
		}
		return this;
	}

	public TransformedMedia addCombining(TransformedMedia transformed, MediaFileType preferring) {
		for (Entry<MediaFileType, File> e : transformed.transformed.entrySet()) {
			this.addCombining(e.getValue(), e.getKey(), transformed.mediaTypes.get(e.getValue()), preferring);
		}
		return this;
	}

	private void checkForUniqueImageType(MediaFileType type) {
		if (transformed.containsKey(type)) {
			throw new IllegalArgumentException("Can not contain media file type " + type + " more than once");
		}
	}

	public Collection<UploadFile> getTransformed() {
		checkNotClosed();
		List<UploadFile> images = new ArrayList<>();
		for (File file : getUniqueFiles()) {
			MediaFileType preferred = fileTypes.get(file);
			MediaType type = mediaTypes.get(file);
			images.add(new UploadFile(file, preferred, type));
		}
		return images;
	}

	private Collection<File> getUniqueFiles() {
		Set<File> files = new HashSet<>();
		for (File file : transformed.values()) {
			files.add(file);
		}
		return Collections.unmodifiableSet(files);
	}

	public Map<MediaFileType, File> getFilesByType() {
		checkNotClosed();
		return Collections.unmodifiableMap(transformed);
	}

	/**
	 * Deletes temp files (that have been uploaded to image storage) and the original source
	 */
	@Override
	public void close() throws Exception {
		checkNotClosed();
		closed = true;
		for (File file : getUniqueFiles()) {
			delete(file);
		}
	}

	private void delete(File f) throws IOException {
		FileUtils.delete(f);
	}

	private void checkNotClosed() {
		if (closed) {
			throw new IllegalStateException("already closed");
		}
	}

}
