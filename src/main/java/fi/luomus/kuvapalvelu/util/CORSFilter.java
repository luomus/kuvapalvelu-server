package fi.luomus.kuvapalvelu.util;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.luomus.utils.preferences.LuomusPreferences;

@Provider
public class CORSFilter implements ContainerResponseFilter {

	@Inject
	private LuomusPreferences luomusPreferences;

	private final Logger logger = LoggerFactory.getLogger(CORSFilter.class);
	private boolean notified = false;

	@Override
	public void filter(ContainerRequestContext containerRequestContext,
			ContainerResponseContext containerResponseContext) throws IOException {
		if (luomusPreferences.getBoolean(MediaApiPreferences.DEV_MODE).or(Boolean.TRUE).booleanValue()) {
			if (!this.notified) {
				logger.warn("allowing all cross-origin requests in dev mode, YOU SHOULD NOT SEE THIS IN PRODUCTION");
				this.notified = true;
			}
			MultivaluedMap<String, Object> headers = containerResponseContext.getHeaders();
			headers.add("Access-Control-Allow-Origin", "*");
			headers.add("Access-Control-Allow-Methods", "GET, PUT, DELETE, POST, OPTIONS");
			String headersRequest = containerRequestContext.getHeaders().getFirst("Access-Control-Request-Headers");
			if (headersRequest != null) {
				headers.putSingle("Access-Control-Allow-Headers", headersRequest);
			}
		}
	}

}
