package fi.luomus.kuvapalvelu.util;

import fi.luomus.utils.preferences.Preference;

final public class MediaApiPreferences {

	private MediaApiPreferences() {}

	public final static Preference DEV_MODE = new Preference("DEV_MODE");
	public final static Preference MAIL_SERVER = new Preference("MAIL_SERVER");
	public final static Preference MAIL_RECIPIENTS = new Preference("MAIL_RECIPIENTS");

	public final static Preference IMAGE_QUALITY = new Preference("IMAGE_QUALITY");
	public final static Preference TEMPORARY_FILE_LOCATION = new Preference("TEMPORARY_FILE_LOCATION");

	public final static Preference TRIPLESTORE_URL = new Preference("TRIPLESTORE_URL");
	public final static Preference TRIPLESTORE_USERNAME = new Preference("TRIPLESTORE_USERNAME");
	public final static Preference TRIPLESTORE_PASSWORD = new Preference("TRIPLESTORE_PASSWORD");

	public final static Preference MEDIA_REPOSITORY_URL = new Preference("IMAGE_REPOSITORY_URL");
	public final static Preference MEDIA_REPOSITORY_USERNAME = new Preference("IMAGE_REPOSITORY_USERNAME");
	public final static Preference MEDIA_REPOSITORY_PASSWORD = new Preference("IMAGE_REPOSITORY_PASSWORD");
	public final static Preference IMAGE_REPOSITORY_SECRET = new Preference("IMAGE_REPOSITORY_SECRET");

}
