package fi.luomus.kuvapalvelu.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class GenericUtils {

	private GenericUtils() {}

	public static void notNull(Object... args) {
		for (Object arg : args) {
			if (arg == null) {
				throw new IllegalStateException("null argument not allowed");
			}
		}
	}

	public static Logger logger(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz);
	}

}
