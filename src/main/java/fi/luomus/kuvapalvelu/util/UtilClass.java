package fi.luomus.kuvapalvelu.util;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.utils.exceptions.ApiException;

final public class UtilClass {

	private UtilClass() {}

	public static WebApplicationException generateError(Response.Status status, BaseErrorEntity entity) {
		return new WebApplicationException(Response.status(status).entity(entity).build());
	}

	public static WebApplicationException generateError(int status, BaseErrorEntity entity) {
		return new WebApplicationException(Response.status(status).entity(entity).build());
	}

	public static WebApplicationException generateError(ApiException ax) {
		final String source = ax.getSource();
		final String message = ax.getMessage();
		return generateError(Response.Status.BAD_GATEWAY, new BaseErrorEntity("API_ERROR") {
			@SuppressWarnings("unused")
			public String getSource() {
				return source;
			}

			@SuppressWarnings("unused")
			public String getMessage() {
				return message;
			}
		});
	}

	public static WebApplicationException generateError(Response.Status status, String code) {
		return generateError(status, new BaseErrorEntity(code));
	}
	public static WebApplicationException generateError(int status, String code) {
		return generateError(status, new BaseErrorEntity(code));
	}

	public static WebApplicationException notFound() {
		return generateError(Response.Status.NOT_FOUND, "NOT_FOUND");
	}

}
