package fi.luomus.kuvapalvelu.util;

import java.io.File;
import java.io.IOException;

public class FileUtils {

	private FileUtils() {}

	public static void delete(File f) throws IOException {
		if (f.exists()) {
			f.delete();
		}
		File dir = f.getParentFile();
		if (!dir.isDirectory()) {
			throw new RuntimeException("parent file not dir");
		}
		org.apache.commons.io.FileUtils.deleteDirectory(dir);
	}

}
