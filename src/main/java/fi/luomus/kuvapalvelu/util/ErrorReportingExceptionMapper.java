package fi.luomus.kuvapalvelu.util;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.spi.ExtendedExceptionMapper;
import org.slf4j.Logger;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.utils.preferences.LuomusPreferences;
import fi.luomus.utils.service.ErrorReporterService;

/**
 * In case of an exception that cannot be handled the exception should cause an e-mail report
 * in production environments and no stacktrace
 */
@Provider
public class ErrorReportingExceptionMapper implements ExtendedExceptionMapper<Exception> {

	@Inject
	private HttpServletRequest httpServletRequest;
	@Inject
	private ErrorReporterService errorReporterService;
	@Inject
	private LuomusPreferences preferences;

	private final Logger logger = GenericUtils.logger(ErrorReportingExceptionMapper.class);

	@Override
	public Response toResponse(final Exception exception) {
		try {
			errorReporterService.report(exception, httpServletRequest);
		} catch (Exception e) {
			try {
				logger.error("unable to report exception", exception);
				logger.error("Caused by", e);
			} catch (Exception e1) {
				throw new RuntimeException(e);
			}
			throw new RuntimeException(e);
		}
		logger.warn("Reported exception", exception);

		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.type("application/json")
				.entity(new BaseErrorEntity("INTERNAL_ERROR") {
				})
				.build();
	}

	@Override
	public boolean isMappable(Exception exception) {
		Boolean devMode = preferences.getBoolean(MediaApiPreferences.DEV_MODE).or(Boolean.TRUE);

		// skip error reporting if webapplicationexception (e.g. regular 404 error) or in dev mode
		return !(exception instanceof WebApplicationException || devMode);
	}

}
