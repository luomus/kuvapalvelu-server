package fi.luomus.kuvapalvelu.util;

public class StacktraceUtil {

	public static String buildStackTrace(StringBuilder stackTrace, Throwable condition, int maxTraceLevels) {
		if (condition == null) {
			stackTrace.append("Null condition given for error reporting!").append("\n");
			return stackTrace.toString();
		}
		stackTrace.append(condition.toString()).append("\n");
		int i = 0;
		if (condition.getStackTrace() != null) {
			for (StackTraceElement se : condition.getStackTrace()) {
				if (se == null) break;
				if (i++ > maxTraceLevels) break;
				stackTrace.append(se.toString()).append("\n");
			}
		}
		if (condition.getCause() != null) {
			buildStackTrace(stackTrace, condition.getCause(), maxTraceLevels);
		}
		return stackTrace.toString();
	}

	public static String shorten(String message, int maxLength) {
		if (maxLength < 1) {
			throw new IllegalArgumentException("" +maxLength);
		}

		if (message == null) return null;

		if (message.length() <= maxLength) {
			return message;
		}

		StringBuilder shortenedMessage = new StringBuilder();
		shortenedMessage.append(message.substring(0, (maxLength/2)+1));
		shortenedMessage.append("\n[--- LONG MESSAGE SHORTENED ---]\n");
		shortenedMessage.append(message.substring(message.length() - (maxLength/2)-1, message.length()));
		return shortenedMessage.toString();
	}

}
