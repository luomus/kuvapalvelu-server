package fi.luomus.kuvapalvelu.util;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;

public class CommandExecutorUtil {

	public static class CommandExecutionResult {
		private final StringBuilder output = new StringBuilder();
		private boolean success = false;
		public List<String> getOutputLines() {
			List<String> lines = new ArrayList<>();
			for (String line : getOutput().split(Pattern.quote("\n"))) {
				lines.add(line);
			}
			return lines;
		}
		public String getOutput() {
			return output.toString().replace("\r", "");
		}
		public boolean success() {
			return success;
		}
	}

	public static CommandExecutionResult execute(List<String> commands) {
		CommandExecutionResult result = new CommandExecutionResult();
		Process p = null;
		try {
			ProcessBuilder pb = new ProcessBuilder(commands);
			pb.redirectErrorStream(true);
			p = pb.start();
			result.output.append(IOUtils.toString(p.getInputStream(), Charset.defaultCharset()));
			int exitCode = p.waitFor();
			if (exitCode == 0) {
				result.success = true;
			}
		} catch (Exception e) {
			if (result.output.length() > 0) {
				result.output.append("\n");
			}
			result.output.append("Stacktrace:\n");
			StacktraceUtil.buildStackTrace(result.output, e, 10);
		} finally {
			try {
				if (p != null) p.destroy();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static CommandExecutionResult execute(String ... commands) {
		return execute(Arrays.asList(commands));
	}

}
