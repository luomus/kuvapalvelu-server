package fi.luomus.kuvapalvelu.util;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.utils.exceptions.NotFoundException;

/**
 * Prevent the not found exceptions from bubbling up and handle them by responding with an appropriate not
 * found response
 */
@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

	@Override
	public Response toResponse(final NotFoundException exception) {
		return Response.status(Response.Status.NOT_FOUND)
				.type("application/json")
				.entity(new BaseErrorEntity("NOT_FOUND"))
				.build();
	}

}
