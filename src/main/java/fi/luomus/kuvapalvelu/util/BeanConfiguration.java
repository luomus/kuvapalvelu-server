package fi.luomus.kuvapalvelu.util;

import java.net.URI;
import java.net.URISyntaxException;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import fi.luomus.triplestore.client.HTTPTriplestoreClientImpl;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.TriplestorePreferences;
import fi.luomus.utils.preferences.LuomusPreferences;
import fi.luomus.utils.service.ErrorReporterService;
import fi.luomus.utils.service.ErrorReporterServiceFactory;
import fi.luomus.utils.service.JavaMailErrorReportMailer;

@Configuration
public class BeanConfiguration {

	@Inject
	private LuomusPreferences preferences;

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
	}

	@Bean
	public TriplestoreClient triplestoreClient() {
		TriplestoreClient concreteClient = new HTTPTriplestoreClientImpl(new TriplestorePreferences() {
			@Override
			public URI getBaseUri() {
				try {
					return new URI(preferences.get(MediaApiPreferences.TRIPLESTORE_URL).get());
				} catch (URISyntaxException e) {
					throw new RuntimeException("malformed triplestore address", e);
				}
			}

			@Override
			public String getUsername() {
				return preferences.get(MediaApiPreferences.TRIPLESTORE_USERNAME).get();
			}

			@Override
			public String getPassword() {
				return preferences.get(MediaApiPreferences.TRIPLESTORE_PASSWORD).get();
			}
		});

		return concreteClient;
	}

	@Bean
	public ErrorReporterService errorReporterService() {
		return ErrorReporterServiceFactory.reporter(
				new JavaMailErrorReportMailer.JavaMailErrorReportMailerConfiguration() {
					@Override
					public String getHost() {
						return preferences.get(MediaApiPreferences.MAIL_SERVER).get();
					}

					@Override
					public String getSubject() {
						if (devMode()) return "!!! Media API (STAGING) Exception !!!";
						return "!!! Media API Exception !!!";
					}

					private Boolean devMode() {
						return preferences.getBoolean(MediaApiPreferences.DEV_MODE).or(false);
					}

					@Override
					public String[] getRecipients() {
						return preferences.getMulti(MediaApiPreferences.MAIL_RECIPIENTS).toArray(new String[]{});
					}
				});
	}

}
