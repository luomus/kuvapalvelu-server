package fi.luomus.kuvapalvelu.util;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Static utility class for hashing in order to avoid cluttering
 * code with scrypt parameters and to allow easy swapping of algorithms
 */
final public class Hasher {

	private Hasher() {}

	public static String hash(String seed) {
		return BCrypt.hashpw(seed, BCrypt.gensalt());
	}

	public static boolean check(String expected, String hash) {
		return BCrypt.checkpw(expected, hash);
	}

}
