package fi.luomus.kuvapalvelu.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Component
public class ConcurrentTaskSchedulerWithShutDownHook extends org.springframework.scheduling.concurrent.ConcurrentTaskScheduler {

	private final ExecutorService executorService;

	public ConcurrentTaskSchedulerWithShutDownHook() {
		super();
		ThreadFactory threadFactory = new ThreadFactoryBuilder().setDaemon(true).build();
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(threadFactory);
		this.setScheduledExecutor(executorService);
		this.executorService = executorService;
	}

	@PreDestroy
	public void destroy() {
		executorService.shutdownNow();
	}

}
