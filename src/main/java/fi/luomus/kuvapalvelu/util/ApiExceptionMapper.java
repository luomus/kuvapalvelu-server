package fi.luomus.kuvapalvelu.util;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.LoggerFactory;

import fi.luomus.kuvapalvelu.model.BaseErrorEntity;
import fi.luomus.utils.exceptions.ApiException;

/**
 * Handle api exceptions so that they won't cause unhandled exceptions and return an appropriate error response
 */
@Provider
public class ApiExceptionMapper implements ExceptionMapper<ApiException> {

	@Override
	public Response toResponse(final ApiException exception) {
		LoggerFactory.getLogger(ApiExceptionMapper.class).error("Received API error from source " + exception.getSource());
		return Response.status(Response.Status.BAD_GATEWAY)
				.type("application/json")
				.entity(new BaseErrorEntity("API_ERROR") {
					@SuppressWarnings("unused")
					public String getSource() {
						return exception.getSource();
					}

					@SuppressWarnings("unused")
					public String getMessage() {
						return exception.getMessage();
					}

				})
				.build();
	}

}
