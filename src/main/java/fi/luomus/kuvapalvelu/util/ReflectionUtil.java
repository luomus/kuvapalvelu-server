package fi.luomus.kuvapalvelu.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class ReflectionUtil {

	private static final Map<Class<?>, Map<String, Method>> SETTER_STORAGE = new HashMap<>();

	private static final Map<Class<?>, Map<String, Method>> GETTER_STORAGE = new HashMap<>();

	public static String cleanGetterFieldName(Method method) {
		String fieldName =  method.getName().startsWith("get") ? method.getName().replaceFirst("get", "") : method.getName().replaceFirst("is", "");
		return caseName(fieldName);
	}

	public static String cleanSetterFieldName(Method method) {
		String fieldName = method.getName().replaceFirst("set", "");
		return caseName(fieldName);
	}

	public static String caseName(String fieldName) {
		return Character.toLowerCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	public static String reCaseName(String fieldName) {
		return Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
	}

	public static Method getSetter(String fieldName, Class<?> fromClass) throws NoSuchMethodException {
		Map<String, Method> methods = getSettersMap(fromClass);
		Method m = methods.get(fieldName);
		if (m == null) {
			throw new NoSuchMethodException(fieldName + " in " + fromClass);
		}
		return m;
	}

	public static Method getGetter(String fieldName, Class<?> fromClass) throws NoSuchMethodException {
		Map<String, Method> methods = getGettersMap(fromClass);
		Method m = methods.get(fieldName);
		if (m == null) {
			throw new NoSuchMethodException(fieldName + " in " + fromClass);
		}
		return m;
	}

	public static Map<String, Method> getGettersMap(Class<?> fromClass) {
		if (!GETTER_STORAGE.containsKey(fromClass)) {
			Map<String, Method> getters = new TreeMap<>();
			for (Method m : fromClass.getMethods()) {
				if (!m.getName().startsWith("get") && !m.getName().startsWith("is")) continue;
				if (m.getName().equals("getClass")) continue;
				if (Modifier.isStatic(m.getModifiers())) continue;
				if (m.getParameterTypes().length != 0) continue;
				getters.put(cleanGetterFieldName(m), m);
			}
			GETTER_STORAGE.put(fromClass, Collections.unmodifiableMap(getters));
		}
		return GETTER_STORAGE.get(fromClass);
	}

	public static Map<String, Method> getSettersMap(Class<?> fromClass) {
		if (!SETTER_STORAGE.containsKey(fromClass)) {
			Map<String, Method> setters = new TreeMap<>();
			for (Method m : fromClass.getMethods()) {
				if (!m.getName().startsWith("set")) continue;
				if (Modifier.isStatic(m.getModifiers())) continue;
				if (m.getParameterTypes().length != 1) continue;
				setters.put(cleanSetterFieldName(m), m);
			}
			SETTER_STORAGE.put(fromClass, Collections.unmodifiableMap(setters));
		}
		return SETTER_STORAGE.get(fromClass);
	}

	public static Collection<Method> getSetters(Class<?> fromClass) {
		return getSettersMap(fromClass).values();
	}

	public static Collection<Method> getGetters(Class<?> fromClass) {
		return getGettersMap(fromClass).values();
	}

}
