package fi.luomus.kuvapalvelu;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;

public class Application extends ResourceConfig {

	public Application() {
		packages(true, "fi.luomus.kuvapalvelu.resource");
		registerClasses(FreemarkerMvcFeature.class, JacksonFeature.class, MultiPartFeature.class, RolesAllowedDynamicFeature.class);
		this.property(FreemarkerMvcFeature.TEMPLATE_BASE_PATH, "WEB-INF/templates");
		this.property(FreemarkerMvcFeature.ENCODING, "UTF-8");
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
	}

}
