package fi.luomus.kuvapalvelu.repository;

import static fi.luomus.kuvapalvelu.util.GenericUtils.notNull;

import java.io.File;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.springframework.stereotype.Repository;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.kuvapalvelu.model.TransformedMedia.UploadFile;
import fi.luomus.kuvapalvelu.util.Constants;
import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Repository
public class MediaFileRepositoryImpl implements MediaFileRepository {

	private final LuomusPreferences preferences;

	@Inject
	public MediaFileRepositoryImpl(LuomusPreferences preferences) {
		this.preferences = preferences;
	}

	@Override
	public Map<MediaFileType, URI> upload(String id, TransformedMedia transformedMedia, boolean secret) throws ApiException {
		notNull(id, transformedMedia);
		Map<File, URI> uploadedFiles = upload(id, transformedMedia.getTransformed(), secret);
		return getURIs(transformedMedia, uploadedFiles);
	}

	private Map<MediaFileType, URI> getURIs(TransformedMedia transformedMedia, Map<File, URI> uploadedFiles) {
		Map<MediaFileType, URI> mediaURIs = new HashMap<>();
		for (Map.Entry<MediaFileType, File> e : transformedMedia.getFilesByType().entrySet()) {
			MediaFileType uriType = e.getKey();
			File file = e.getValue();
			URI uri = uploadedFiles.get(file);
			mediaURIs.put(uriType, uri);
		}
		return mediaURIs;
	}

	private Map<File, URI> upload(String id, Collection<UploadFile> files, boolean secret) throws ApiException {
		FormDataMultiPart multiPart = new FormDataMultiPart();
		multiPart.field("protected", secret ? "true" : "false");

		for (UploadFile file : files) {
			String bin = bin(file);
			multiPart.bodyPart(bodyPart(bin, file));
		}

		Map<String, String> responseData = upload(id, multiPart);

		String baseUrl = preferences.get(MediaApiPreferences.MEDIA_REPOSITORY_URL).get();
		Map<File, URI> fileURIs = new HashMap<>();
		for (UploadFile file : files) {
			String bin = bin(file);
			String uploadedFileName = responseData.get(bin);
			URI uri = UriBuilder.fromUri(baseUrl).path(id).path(uploadedFileName).build();
			fileURIs.put(file.getFile(), uri);
		}
		return fileURIs;
	}

	private String bin(UploadFile file) {
		return file.getFileType().toString().toLowerCase();
	}

	private FileDataBodyPart bodyPart(String key, UploadFile file) {
		FileDataBodyPart bp = new FileDataBodyPart(key, file.getFile());
		bp.setMediaType(file.getMediaType());
		return bp;
	}

	private Map<String, String> upload(String id, FormDataMultiPart multiPart) throws ApiException {
		WebTarget target = target();
		Response post = target.path(id).request().post(Entity.entity(multiPart, multiPart.getMediaType()));
		raiseForNonSuccess(post);
		return post.readEntity(new GenericType<Map<String, String>>() {});
	}

	@Override
	public void delete(String id) throws NotFoundException, ApiException {
		Response delete = target().path(id).request().delete();
		if (delete.getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
			throw new NotFoundException("resource " + id + " not found");
		}
		raiseForNonSuccess(delete);
	}

	@Override
	public void update(String id, final boolean isProtected) throws ApiException {
		Response response = target().path(id).request().put(Entity.json(new Object() {
			@SuppressWarnings("unused")
			public Boolean isProtected() {
				return isProtected;
			}
		}));
		raiseForNonSuccess(response);
	}

	private void raiseForNonSuccess(Response response) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			String details = null;
			try {
				details = response.readEntity(String.class);
			} catch (ProcessingException e) {
				//nop
			}
			throw ApiException.builder().source(Constants.KUVAVARASTO)
			.code("" + response.getStatus())
			.details(details)
			.build();
		}
	}

	private WebTarget target() {
		String username = preferences.get(MediaApiPreferences.MEDIA_REPOSITORY_USERNAME).get();
		String password = preferences.get(MediaApiPreferences.MEDIA_REPOSITORY_PASSWORD).get();
		Client client = ClientBuilder.newBuilder()
				.register(HttpAuthenticationFeature.basic(username, password))
				.register(new LoggingFeature()).register(MultiPartFeature.class)
				.build();
		return client.target(preferences.get(MediaApiPreferences.MEDIA_REPOSITORY_URL).get());
	}

}
