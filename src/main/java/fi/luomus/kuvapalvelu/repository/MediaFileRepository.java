package fi.luomus.kuvapalvelu.repository;

import java.net.URI;
import java.util.Map;

import fi.luomus.kuvapalvelu.model.MediaFileType;
import fi.luomus.kuvapalvelu.model.TransformedMedia;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

/**
 * Media file repository is where the media files and their transformed versions are stored and retrieved from after
 * the media file and it's accompanying metadata have been successfully uploaded
 */
public interface MediaFileRepository {

	/**
	 * Upload 1..n files to the file repository
	 *
	 * @param id Media resource id
	 * @param transformedImages Files to upload
	 * @param secret Should the files be public or private
	 * @return The final file locations
	 */
	Map<MediaFileType, URI> upload(String id, TransformedMedia transformedImages, boolean secret) throws ApiException;

	/**
	 * Update image resource protected status
	 *
	 * @param isProtected Should the access to image files be protected
	 */
	void update(String id, boolean isProtected) throws ApiException;

	/**
	 * Deletes all the files associated with the provided id
	 *
	 * @param Media resource id
	 */
	void delete(String id) throws NotFoundException, ApiException;

}
