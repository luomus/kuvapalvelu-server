package fi.luomus.kuvapalvelu.repository;

import static fi.luomus.kuvapalvelu.util.GenericUtils.notNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.NamedResource;
import fi.luomus.kuvapalvelu.model.Predicate;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Side;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.kuvapalvelu.model.Urls;
import fi.luomus.kuvapalvelu.util.ReflectionUtil;
import fi.luomus.triplestore.client.TriplestoreClient;
import fi.luomus.triplestore.client.TriplestoreClient.TooManyResultsException;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.model.LocalizedString;
import fi.luomus.utils.model.LocalizedString.LocalizedStringFactory;

/**
 * Implements media meta data repository using LuOnto ontology database (Triplestore) as the
 * backing store, contains a mapper that maps Java object values to appropriate predicates
 */
@Repository
public class MediaMetaDataRepositoryImpl implements MediaMetaDataRepository {

	private static final String RDFS_NS_URI = "http://www.w3.org/2000/01/rdf-schema#";
	private static final String PUBLIC = "MZ.publicityRestrictionsPublic";
	private static final String PRIVATE = "MZ.publicityRestrictionsPrivate";
	private final TriplestoreClient triplestoreClient;

	private static final Set<String> MEDIA_CLASSES = Arrays.asList(MediaClass.values()).stream().map(c->c.getClassName()).collect(Collectors.toSet());

	private static boolean isMediaClass(String className) {
		return MEDIA_CLASSES.contains(className);
	}

	@Inject
	public MediaMetaDataRepositoryImpl(TriplestoreClient triplestoreClient) {
		this.triplestoreClient = triplestoreClient;
	}

	@Override
	public void save(String id, Meta meta, Urls urls, MediaClass mediaClass) throws ApiException {
		notNull(id, meta, urls);

		Model model = ModelFactory.createDefaultModel();
		Resource imageResource = model.createResource(Predicate.DEFAULT_NS + id);
		imageResource.addProperty(RDF.type, model.createResource(Predicate.DEFAULT_NS + mediaClass.getClassName()));
		Mapper mapper = new Mapper(imageResource);
		writeResource(mapper, meta);
		writeResource(mapper, meta.getIdentifications());
		writeResource(mapper, urls);
		String value = meta.isSecret() ? PRIVATE : PUBLIC;
		Property property = ModelFactory.createDefaultModel().createProperty(Predicate.DEFAULT_NS, "MZ.publicityRestrictions");
		mapper.setResource(property, Predicate.DEFAULT_NS, value);
		triplestoreClient.upsertResource(id, model);
	}

	@Override
	public Optional<MetaAndUrls> get(String id) throws ApiException {
		notNull(id);
		Optional<Model> optionalResource = triplestoreClient.getResource(id);
		if (!optionalResource.isPresent()) {
			return Optional.absent();
		}
		Model model = optionalResource.get();
		Resource resource = model.getResource(Predicate.DEFAULT_NS + id);
		Statement typeStatement = resource.getProperty(RDF.type);
		if (typeStatement == null) {
			throw new NotMediaMetadataException("resource class not specified");
		}
		String className = typeStatement.getObject().asResource().getLocalName();
		if (!isMediaClass(className)) {
			throw new NotMediaMetadataException("retrieved resource class is " + className);
		}
		return Optional.of(readResource(resource));
	}

	private MetaAndUrls readResource(Resource resource) {
		Meta meta = new Meta();
		Mapper mapper = new Mapper(resource);
		readResource(mapper, meta);
		Meta.Identifications identifications = new Meta.Identifications();
		readResource(mapper, identifications);
		meta.setIdentifications(identifications);
		Urls urls = new Urls();
		readResource(mapper, urls);

		String publicityRestrictions = mapper.getSingle("MZ.publicityRestrictions");
		if (publicityRestrictions != null) {
			switch (publicityRestrictions) {
			case PUBLIC:
				meta.setSecret(false);
				break;
			case PRIVATE:
				meta.setSecret(true);
				break;
			default:
				throw new RuntimeException("Invalid publicity value " + publicityRestrictions);
			}
		}

		return new MetaAndUrls(meta, urls);
	}

	@Override
	public void delete(String id) throws NotFoundException, ApiException {
		notNull(id);
		this.triplestoreClient.delete(id);
	}

	@Override
	public String newId() throws ApiException {
		return this.triplestoreClient.getSequence("MM");
	}

	@Override
	public List<WithId> search(String field, List<String> values, int limit, int offset, MediaClass expectedClass) throws ApiException,
	InvalidParameterException {
		String expectedClassQname = expectedClass.getClassName();
		List<String> invalidFields = new ArrayList<>();
		List<WithId> withIds = new ArrayList<>();
		try {
			Predicate predicate = getPredicate(field, Meta.class);
			withIds = search(predicate.value(), values, limit, offset, predicate.isLiteral(), expectedClassQname);
		} catch (NoSuchFieldException e) {
			invalidFields.add(field);
			// dont throw exception yet so well get all of the fields
		}

		Collections.sort(withIds, new Comparator<WithId>() {
			@Override
			public int compare(WithId o1, WithId o2) {
				return o1.metaAndUrls.meta.compareTo(o2.metaAndUrls.meta);
			}
		});

		if (invalidFields.size() > 0) {
			throw new InvalidParameterException(invalidFields);
		}
		return withIds;
	}

	// split to maximum of 1000 character value chains to prevent url errors
	private List<WithId> search(String predicate, List<String> values, int limit, int offset, boolean isLiteral, String expectedClassQname)
			throws ApiException {
		int length = 0;
		final int MAX_PARAM_LEN = 1000;
		for (int i = 0; i < values.size(); i++) {
			if (length > MAX_PARAM_LEN) {
				List<WithId> result = new ArrayList<>();
				result.addAll(searchChunk(predicate, values.subList(0, i), limit, offset, isLiteral, expectedClassQname));
				result.addAll(searchChunk(predicate, values.subList(i, values.size()), limit, offset, isLiteral, expectedClassQname));
				return result;
			}
			if (values.get(i).length() > MAX_PARAM_LEN) {
				throw new IllegalStateException("value is not allowed to be over 1000 characters");
			}
			length += values.get(i).length();
		}

		return searchChunk(predicate, values, limit, offset, isLiteral, expectedClassQname);
	}

	private List<WithId> searchChunk(String predicate, List<String> values, int limit, int offset, boolean isLiteral, String expectedClassQname)
			throws ApiException {
		TriplestoreClient.SearchQuery searchQuery;
		if (isLiteral) {
			searchQuery = this.triplestoreClient.searchLiteral();
		} else {
			searchQuery = this.triplestoreClient.searchResource();
		}
		Model search = searchQuery.subjectClass(expectedClassQname).predicate(predicate).object(values).limit(limit).offset(offset).execute();

		return search(search);
	}

	private List<WithId> search(Model searchResult) {
		List<WithId> result = new ArrayList<>();
		ResIterator resIterator = searchResult.listSubjects();
		while (resIterator.hasNext()) {
			Resource next = resIterator.next();
			MetaAndUrls metaAndUrls = readResource(next);
			WithId withId = new WithId(next.getLocalName(), metaAndUrls);
			result.add(withId);
		}

		return result;
	}

	@Override
	public List<License> getLicenses() throws ApiException {
		return getEnumeration("MZ.intellectualRightsEnum", License.class);
	}

	@Override
	public List<Sex> getSexes() throws ApiException {
		return getEnumeration("MY.sexes", Sex.class);
	}

	@Override
	public List<LifeStage> getLifeStages() throws ApiException {
		return getEnumeration("MY.lifeStages", LifeStage.class);
	}

	@Override
	public List<LifeStage> getPlantLifeStages() throws ApiException {
		return getEnumeration("MY.plantLifeStageEnum", LifeStage.class);
	}

	@Override
	public List<Side> getSides() throws ApiException {
		return getEnumeration("MM.sideEnum", Side.class);
	}

	@Override
	public List<Type> getTypes() throws ApiException {
		return getEnumeration("MM.typeEnum", Type.class);
	}

	private <T extends NamedResource> List<T> getEnumeration(String enumerationId, Class<T> targetClass) throws ApiException {
		Optional<Model> optionalModel;
		try {
			optionalModel = this.triplestoreClient.getResource(enumerationId, Collections.singletonMap("format", "RDFXML"));
		} catch (TooManyResultsException e) {
			// should not happen
			throw new RuntimeException(e);
		}
		if (!optionalModel.isPresent()) {
			throw new IllegalArgumentException(enumerationId + " not found in triplestore");
		}
		StmtIterator stmtIterator = optionalModel.get().listStatements();
		Map<Integer, T> result = new TreeMap<>();
		while (stmtIterator.hasNext()) {
			Statement stmt = stmtIterator.next();
			String name = stmt.getPredicate().getLocalName();
			if (name.startsWith("_")) {
				int order = parseOrder(name);
				result.put(order, getNamedResource(stmt.getResource().getLocalName(), targetClass));
			}
		}
		return new ArrayList<>(result.values());
	}

	private int parseOrder(String name) {
		try {
			return Integer.valueOf(name.replace("_", ""));
		} catch (Exception e) {
			return name.hashCode() * 10000;
		}
	}

	private <T extends NamedResource> T getNamedResource(String resourceId, Class<T> resoureClass) throws ApiException {
		final Property LABEL = ModelFactory.createDefaultModel().createProperty(RDFS_NS_URI, "label");
		Optional<Model> optional = this.triplestoreClient.getResource(resourceId);
		if (!optional.isPresent()) {
			throw new IllegalStateException(String.format("Resource %s in enumeration but not found in triplestore", resourceId));
		}
		T resource;
		try {
			resource = resoureClass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new UnsupportedOperationException("Not implemented for " + resoureClass);
		}
		resource.setId(resourceId);
		resource.setLabel(new LocalizedString());
		StmtIterator stmtIterator = optional.get().getResource(Predicate.DEFAULT_NS + resourceId).listProperties(LABEL);
		while (stmtIterator.hasNext()) {
			Statement next = stmtIterator.next();
			resource.getLabel().put(next.getLanguage(), next.getLiteral().getString());
		}
		return resource;
	}

	@Override
	public List<String> getSubTaxa(String taxonId) throws ApiException {
		List<String> result = new ArrayList<>();
		Optional<Model> optionalResource;
		try {
			optionalResource = this.triplestoreClient.getResource(taxonId, ImmutableMap.<String, String>builder()
					.put("resulttype", "children").build());
		} catch (TriplestoreClient.TooManyResultsException e) {
			return result;
		}
		if (!optionalResource.isPresent()) {
			return result;
		}
		Model parentResource = optionalResource.get();
		ResIterator resIterator = parentResource.listSubjects();

		while (resIterator.hasNext()) {
			Resource resource = resIterator.nextResource();

			// skip the taxon itself
			if (!resource.getLocalName().equals(taxonId)) {
				result.add(resource.getLocalName());
			}
		}

		return result;
	}

	private Predicate getPredicate(String key, Class<?> currentClass) throws NoSuchFieldException {
		List<String> split = Splitter.on('.').splitToList(key);
		int splitSize = split.size();

		if (splitSize == 0) {
			// should not happen
			throw new RuntimeException();
		}
		Field declaredField = currentClass.getDeclaredField(split.get(0));
		if (splitSize == 1) {
			Predicate annotation = declaredField.getAnnotation(Predicate.class);
			if (annotation == null) {
				throw new NoSuchFieldException("field not mapped");
			}
			return annotation;
		}
		Class<?> fieldType = declaredField.getType();
		return getPredicate(Joiner.on(".").join(split.subList(1, splitSize)), fieldType);
	}

	private void writeResource(Mapper mapper, Object object) {
		try {
			for (Field field : object.getClass().getDeclaredFields()) {
				writeField(field, mapper, object);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void writeField(Field field, Mapper mapper, Object object) throws Exception {
		Predicate predicate = field.getAnnotation(Predicate.class);
		if (predicate == null) return;

		String fieldName = field.getName();
		String value = BeanUtils.getProperty(object, fieldName);

		if (value == null) return;

		Property property = ModelFactory.createDefaultModel().createProperty(predicate.namespace(), predicate.value());

		if (List.class.isAssignableFrom(field.getType())) {
			for (Object listValue : BeanUtils.getArrayProperty(object, fieldName)) {
				if (listValue == null) continue;
				writeString(mapper, predicate, property, listValue.toString());
			}
		} else if (LocalizedString.class.isAssignableFrom(field.getType())) {
			LocalizedString localizedString = (LocalizedString) PropertyUtils.getProperty(object, fieldName);
			mapper.setLocalizedString(property, localizedString);
		} else {
			writeString(mapper, predicate, property, value);
		}
	}

	private void writeString(Mapper mapper, Predicate predicate, Property property, String stringValue) {
		if (predicate.isLiteral()) {
			mapper.setLiteral(property, stringValue);
		} else {
			mapper.setResource(property, Predicate.DEFAULT_NS, stringValue);
		}
	}

	private <T> T readResource(Mapper mapper, T object) {
		for (Field field : object.getClass().getDeclaredFields()) {
			Predicate predicate = field.getAnnotation(Predicate.class);
			if (predicate == null) {
				continue;
			}
			String fieldName = field.getName();
			Property prop = ModelFactory.createDefaultModel().createProperty(predicate.namespace() + predicate.value());
			Object value;
			try {
				value = getValue(field, prop, mapper);
			} catch (Exception e) {
				throw new RuntimeException("error while trying to convert resource value to object value", e);
			}
			try {
				ReflectionUtil.getSetter(fieldName, object.getClass()).invoke(object, value);
			} catch (Exception e) {
				throw new RuntimeException("error while trying to set object value", e);
			}
		}

		return object;
	}

	private Object getValue(Field field, Property prop, Mapper mapper) throws Exception {
		Class<?> type = field.getType();
		if (String.class.isAssignableFrom(type)) {
			return mapper.getSingle(prop);
		}
		if (URI.class.isAssignableFrom(type)) {
			return mapper.uri(prop.getLocalName());
		}
		if (List.class.isAssignableFrom(type)) {
			Class<?> innerType = getInnerTypeOfList(field);
			if (String.class.isAssignableFrom(innerType)) {
				return mapper.getMulti(prop);
			}
			throw new RuntimeException("unknown inner type " + innerType);
		}
		if (DateTime.class.isAssignableFrom(type)) {
			String raw = mapper.getSingle(prop);
			if (raw == null) return null;
			return DateTime.parse(raw);
		}
		if (LocalizedString.class.isAssignableFrom(type)) {
			return mapper.getLocalizedString(prop);
		}

		// try to construct a new instance with a single string constructor if such exists
		Constructor<?> constructor = type.getConstructor(String.class);
		if (constructor == null) {
			throw new RuntimeException("Unknown type " + type);
		}
		String raw = mapper.getSingle(prop);
		if (raw == null) return null;
		return constructor.newInstance(raw);
	}

	private Class<?> getInnerTypeOfList(Field field) throws NoSuchMethodException {
		Class<?> innerType = TypeToken.of(field.getGenericType()).resolveType(List.class.getMethod("get", int.class).getGenericReturnType()).getRawType();
		return innerType;
	}

	private static class Mapper {
		private final Resource resource;

		public Mapper(Resource resource) {
			this.resource = resource;
		}

		public LocalizedString getLocalizedString(Property prop) {
			StmtIterator stmtIterator = resource.listProperties(prop);
			if (!stmtIterator.hasNext()) return null;
			LocalizedStringFactory factory = new LocalizedStringFactory();
			while (stmtIterator.hasNext()) {
				Statement statement = stmtIterator.next();
				RDFNode object = statement.getObject();
				if (object.isLiteral()) {
					Literal literal = statement.getLiteral();
					factory.with(literal.getLanguage(), literal.getString());
				} else {
					throw new RuntimeException("cant transform statement " + statement);
				}
			}
			return factory.get();
		}

		private URI uri(String property) {
			String raw = getSingle(property);
			if (raw != null) {
				return UriBuilder.fromUri(raw).build();
			}
			return null;
		}

		private String getSingle(String propName) {
			return getSingle(ModelFactory.createDefaultModel().createProperty(Predicate.DEFAULT_NS, propName));
		}

		private String getSingle(Property prop) {
			List<String> multi = getMulti(prop);
			if (multi.size() > 1) {
				throw new RuntimeException("got multiple for property " + prop);
			}
			return multi.size() == 1 ? multi.get(0) : null;
		}

		private List<String> getMulti(Property prop) {
			List<String> values = new ArrayList<>();
			StmtIterator stmtIterator = resource.listProperties(prop);
			while (stmtIterator.hasNext()) {
				Statement statement = stmtIterator.next();
				RDFNode object = statement.getObject();
				if (object.isLiteral()) {
					values.add(statement.getLiteral().getString());
				} else if (object.isResource()) {
					values.add(object.asResource().getLocalName());
				} else {
					throw new RuntimeException("cant transform statement " + statement);
				}
			}


			return values;
		}

		private void setLiteral(Property prop, String value) {
			resource.addProperty(prop, value);
		}

		private void setResource(Property prop, String namespace, String localName) {
			Resource newResource = ResourceFactory.createResource(namespace + localName);
			resource.addProperty(prop, newResource);
		}

		public void setLocalizedString(Property prop, LocalizedString localizedString) {
			for (String language : localizedString.keySet()) {
				String value = localizedString.get(language);
				if (value != null) {
					resource.addProperty(prop, value, language);
				}
			}
		}
	}

	public static class NotMediaMetadataException extends RuntimeException {
		private static final long serialVersionUID = -2133097395947010825L;

		public NotMediaMetadataException(String reason) {
			super(reason);
		}
	}

}
