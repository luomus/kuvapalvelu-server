package fi.luomus.kuvapalvelu.repository;

import java.util.List;

import com.google.common.base.Optional;

import fi.luomus.kuvapalvelu.model.License;
import fi.luomus.kuvapalvelu.model.LifeStage;
import fi.luomus.kuvapalvelu.model.MediaClass;
import fi.luomus.kuvapalvelu.model.Meta;
import fi.luomus.kuvapalvelu.model.Sex;
import fi.luomus.kuvapalvelu.model.Side;
import fi.luomus.kuvapalvelu.model.Type;
import fi.luomus.kuvapalvelu.model.Urls;
import fi.luomus.utils.exceptions.ApiException;
import fi.luomus.utils.exceptions.NotFoundException;

/**
 * Media meta data repository contains the metadata for the uploaded media files, it does not contain
 * the media files themselves but references to them
 */
public interface MediaMetaDataRepository {

	/**
	 * Create or update the metadata for the image resource corresponding to the given id
	 *
	 * @param id   Media resource id for an existing or non-existing image resource
	 * @param meta New meta values
	 * @param urls New urls
	 * @param mediaClass Media type
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	void save(String id, Meta meta, Urls urls, MediaClass mediaClass) throws ApiException;

	/**
	 * Get the metadata for the media resource with the given id
	 *
	 * @param id Media resource id for an existing or non-existing image resource
	 * @return Optional of the metadata, absent if there is no media resource with the given id
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	Optional<MetaAndUrls> get(String id) throws ApiException;

	/**
	 * Delete the metadata of the image resource with the given id
	 *
	 * @param id Media resource id
	 * @throws ApiException      Thrown if a communication error occurred during the operation
	 * @throws NotFoundException Thrown if no media resource with the given id exists
	 */
	void delete(String id) throws ApiException, NotFoundException;

	/**
	 * Get a list of metada that matches with the search query in a deterministic order
	 *
	 * @param field  The name of the field (e.g. "identification.documentId")
	 * @param values All possible values (OR semantics)
	 * @param limit  The maximum amount of search results
	 * @param offset The offset of the search results
	 * @param mediaClass Expected media type
	 * @return A list of metadata that matches the search query
	 * @throws ApiException              Thrown if a communication error occurred during the operation
	 * @throws InvalidParameterException The given search parameters is not valid (typically the field name)
	 */
	List<WithId> search(String field, List<String> values, int limit, int offset, MediaClass mediaClass) throws ApiException, InvalidParameterException;

	/**
	 * Retrieve the next free media resource id
	 *
	 * @return Next free media resource id
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	String newId() throws ApiException;

	/**
	 * Return a list of available licenses
	 *
	 * @return List of licenses
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	List<License> getLicenses() throws ApiException;

	/**
	 * Return a list of available sexes
	 *
	 * @return List of sexes
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */

	List<Sex> getSexes() throws ApiException;

	/**
	 * Return a list of available life stages
	 *
	 * @return List of life stages
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */

	List<LifeStage> getLifeStages() throws ApiException;

	/**
	 * Return a list of available plant life stages
	 *
	 * @return List of plant life stages
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	List<LifeStage> getPlantLifeStages() throws ApiException;

	/**
	 * Return a list of available taxon image sides
	 *
	 * @return List of sides
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	List<Side> getSides() throws ApiException;

	/**
	 * Return a list of available taxon image types
	 *
	 * @return List of types
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	List<Type> getTypes() throws ApiException;

	/**
	 * Get a list of taxon ids that are sub-taxons of the given taxon id
	 *
	 * @param taxonId The parent taxon id
	 * @return List of sub-taxon ids
	 * @throws ApiException Thrown if a communication error occurred during the operation
	 */
	List<String> getSubTaxa(String taxonId) throws ApiException;

	class WithId {
		public final String id;
		public final MetaAndUrls metaAndUrls;

		public WithId(String id, MetaAndUrls metaAndUrls) {
			this.id = id;
			this.metaAndUrls = metaAndUrls;
		}
	}

	class MetaAndUrls {
		public final Meta meta;
		public final Urls urls;

		public MetaAndUrls(Meta meta, Urls urls) {
			this.meta = meta;
			this.urls = urls;
		}
	}

	class InvalidParameterException extends Exception {
		private static final long serialVersionUID = -7855223810036526411L;
		private final List<String> invalidParams;

		public InvalidParameterException(List<String> invalidParams) {
			this.invalidParams = invalidParams;
		}

		public List<String> getInvalidParams() {
			return invalidParams;
		}
	}

}
