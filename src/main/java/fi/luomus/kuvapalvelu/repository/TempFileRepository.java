package fi.luomus.kuvapalvelu.repository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.joda.time.DateTime;

import com.google.common.base.Optional;

import fi.luomus.utils.exceptions.NotFoundException;

/**
 * Temporary file repository is where media files are stored during the media resource creation process where
 * it can be accessed for a limited time for file transformations and to be finally uploaded to the actual
 * media file repository
 */
public interface TempFileRepository {

	/**
	 * Reads the input stream and saves the file using the given name
	 *
	 * @param stream   File byte stream
	 * @param filename The name that should be used for the created file
	 * @return An object containing the temporary file repository handle identifier and the time after which the file is not available
	 * @throws IOException Thrown if an error occurs during I/O operation
	 */
	TempFileInfo storeTempFile(InputStream stream, String filename) throws IOException;

	/**
	 * Delete the temporary file with the given temporary file id handle
	 *
	 * @param id The handle id for the file that should be deleted
	 * @throws IOException       Thrown if an error occurs during the I/O operation
	 * @throws NotFoundException Thrown if there is no temp file with the given id
	 */
	void delete(String id) throws IOException, NotFoundException;

	/**
	 * Get a File object handle for the given temporary file repository handle id
	 *
	 * @param id The handle id for the file that should be returned
	 * @return An optional of the file handle for the given id, absent if there is no temp file for the given id
	 * @throws IOException Thrown if an error occurs during the I/O operation
	 */
	Optional<File> getHandle(String id) throws IOException;

	/**
	 * Represents a temporary file repository file upload handle with expiration time
	 */
	interface TempFileInfo {
		String getId();

		DateTime getExpires();
	}

	/**
	 * Get a random temporary folder. These files will not be auto deleted, unlike with storeTempFile.
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	File getTempFile(String filename) throws IOException;

}
