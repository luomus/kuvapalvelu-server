package fi.luomus.kuvapalvelu.repository;

import static fi.luomus.kuvapalvelu.util.GenericUtils.logger;
import static fi.luomus.kuvapalvelu.util.GenericUtils.notNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.UUID;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;

import fi.luomus.kuvapalvelu.util.MediaApiPreferences;
import fi.luomus.utils.exceptions.NotFoundException;
import fi.luomus.utils.preferences.LuomusPreferences;

@Service
public class TempFileRepositoryImpl implements TempFileRepository {

	private final static int TEMPORARY_FILE_RETENTION_MINUTES = 15;
	private final TaskScheduler taskScheduler;

	private File tempDir;
	private String randomDirName;
	private final Logger logger = logger(TempFileRepositoryImpl.class);
	private final LuomusPreferences preferences;

	@Inject
	public TempFileRepositoryImpl(TaskScheduler taskScheduler, LuomusPreferences preferences) {
		this.taskScheduler = taskScheduler;
		this.preferences = preferences;
	}

	@Override
	public TempFileInfo storeTempFile(InputStream stream, String filename) throws IOException {
		notNull(stream, filename);
		final String random = UUID.randomUUID().toString();
		FileOutputStream outputStream = new FileOutputStream(getTempFile(random, FilenameUtils.normalize(filename)));
		IOUtils.copy(stream, outputStream);

		final DateTime expires = DateTime.now().plusMinutes(TEMPORARY_FILE_RETENTION_MINUTES);
		scheduleDeletion(random, expires);

		return new TempFileInfo() {
			@Override
			public String getId() {
				return random;
			}

			@Override
			public DateTime getExpires() {
				return expires;
			}
		};
	}

	@Override
	public void delete(String id) throws IOException, NotFoundException {
		notNull(id);
		File tempDir = getSubTempDir(id);
		if (!tempDir.exists()) {
			throw new NotFoundException("Temporary file location has already been deleted");
		}

		FileUtils.deleteDirectory(tempDir);
	}

	@Override
	public Optional<File> getHandle(String id) throws IOException {
		notNull(id);
		File tempDir = getSubTempDir(id);
		if (!tempDir.exists()) {
			return Optional.absent();
		}

		File[] files = tempDir.listFiles();
		if (files == null) {
			throw new RuntimeException("listfiles returned null, not a directory?");
		}
		if (files.length != 1) {
			throw new RuntimeException("Temp file dir has " + files.length + " files");
		}

		return Optional.of(files[0]);
	}

	private void scheduleDeletion(final String id, DateTime when) {
		final TempFileRepositoryImpl thiz = this;
		taskScheduler.schedule(new Runnable() {
			@Override
			public void run() {
				try {
					thiz.delete(id);
				} catch (IOException e) {
					throw new RuntimeException(e);
				} catch (NotFoundException e) {
					// nop
				}
			}
		}, when.toDate());
	}

	@PreDestroy
	public void destroy() {
		try {
			File tempDir = getTempDir();
			logger.info("deleting temporary directory " + tempDir.getCanonicalPath());
			FileUtils.deleteDirectory(tempDir);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public File getTempFile(String filename) throws IOException {
		String random = UUID.randomUUID().toString();
		return getTempFile(random, filename);
	}

	private File getTempFile(String random, String filename) throws IOException {
		File dir = getSubTempDir(random);
		if (!dir.mkdir()) {
			throw new RuntimeException("Unable to create directory");
		}
		return new File(dir.getCanonicalPath() + File.separator + filename);
	}

	private File getSubTempDir(String random) throws IOException {
		return new File(getTempDir().getCanonicalPath() + File.separator + random + File.separator);
	}

	private File getTempDir() throws IOException {
		Optional<String> tempFileLocation = preferences.get(MediaApiPreferences.TEMPORARY_FILE_LOCATION);
		if (!tempFileLocation.isPresent()) {
			return getDefaultTempDir();
		}
		String dirString = tempFileLocation.get();
		File baseDir = new File(dirString);
		if (!baseDir.isDirectory()) {
			throw new IllegalStateException(dirString + " is not a valid directory for temp files");
		}
		File dir = new File(baseDir, getRandomDirName());
		if (!dir.exists()) {
			if (!dir.mkdir()) {
				throw new RuntimeException("unable to create temp dir " + dir.getAbsolutePath());
			}
			logger.info("created temporary subdir " + dir.getAbsolutePath());
		}
		return dir;
	}

	private synchronized File getDefaultTempDir() throws IOException {
		if (this.tempDir == null) {
			this.tempDir = Files.createTempDirectory("image-api").toFile();
		}

		return this.tempDir;
	}

	private synchronized String getRandomDirName() {
		if (this.randomDirName == null) {
			this.randomDirName = UUID.randomUUID().toString();
		}

		return this.randomDirName;
	}

}
