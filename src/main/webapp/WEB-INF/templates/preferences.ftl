<!doctype html>
<html>
<head>
    <title>Asetukset</title>
</head>
<body>
<h1>Asetukset</h1>
<#if saved>
<h3>Asetukset tallennettiin</h3>
</#if>
<form method="post">
    <table>
        <tr>
            <th>Avain</th>
            <th>Arvo</th>
        </tr>
    <#list preferences?keys as preference>
        <tr>
            <td><label>${preference}</label></td>
            <td>
                <input type="text" name="${preference}" value="${preferences[preference]}"
                       style="width: 50%; min-width: 500px"/>
            </td>
        </tr>
    </#list>
    </table>
    <input type="submit" value="Tallenna"/>
</form>
</body>
</html>